/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "agge/test/googletest.hpp"
#include "agge/test/output_test.hpp"

#include "agge/math/vec3f.hpp"
#include "agge/core/stdexceptions.hpp"

using agge::math::vec3f;

TEST(vec3f, default_construct)
{
    vec3f v1;
}

TEST(vec3f, construct_initialize_members)
{
    vec3f v(agge::core::initialize_members);
    EXPECT_EQ(0.0f, v.x);
    EXPECT_EQ(0.0f, v.y);
    EXPECT_EQ(0.0f, v.z);
}

TEST(vec3f, construct_from_values)
{
    vec3f v(1.0f, 2.0f, 3.0f);
    EXPECT_EQ(1.0f, v.x);
    EXPECT_EQ(2.0f, v.y);
    EXPECT_EQ(3.0f, v.z);
}

TEST(vec3f, construct_from_value)
{
    vec3f v(2.0f);
    EXPECT_EQ(2.0f, v.x);
    EXPECT_EQ(2.0f, v.y);
    EXPECT_EQ(2.0f, v.z);
}

TEST(vec3f, construct_from_initializer)
{
    vec3f v = { 1.0f, 2.0f, 3.0f };
    EXPECT_EQ(1.0f, v.x);
    EXPECT_EQ(2.0f, v.y);
    EXPECT_EQ(3.0f, v.z);
}

TEST(vec3f, construct_from_too_small_initializer)
{
    try {
        vec3f v = { 2.0f };
        FAIL()<< "No exception raised, got " << v;
    } catch(agge::core::illegal_argument&) {
    } catch(...) {
        FAIL() << "Unexpected exception raised";
    }
}

TEST(vec3f, construct_from_too_large_initializer)
{
    try {
        vec3f v = { 2.0f, 2.0f, 2.0f, 17.0f };
        FAIL()<< "No exception raised, got " << v;
    } catch(agge::core::illegal_argument&) {
    } catch(...) {
        FAIL() << "Unexpected exception raised";
    }
}

TEST(vec3f, construct_from_float)
{
    vec3f v(1.0f);
    EXPECT_EQ(1.0f, v.x);
    EXPECT_EQ(1.0f, v.y);
    EXPECT_EQ(1.0f, v.z);
}

TEST(vec3f, construct_from_float_pointer)
{
    float x[] = { 1.0f, 2.0f, 3.0f };

    vec3f v(&x[0]);
    EXPECT_EQ(1.0f, v.x);
    EXPECT_EQ(2.0f, v.y);
    EXPECT_EQ(3.0f, v.z);
}

TEST(vec3f, constants)
{
    vec3f v1 = vec3f::ZERO;
    vec3f v2 = vec3f::X;
    vec3f v3 = vec3f::Y;
    vec3f v4 = vec3f::Z;
    vec3f v5 = vec3f::XYZ;

    EXPECT_EQ(v1, vec3f::ZERO);
    EXPECT_NE(v2, vec3f::ZERO);
    EXPECT_NE(v3, vec3f::ZERO);
    EXPECT_NE(v4, vec3f::ZERO);
    EXPECT_NE(v5, vec3f::ZERO);
}

TEST(vec3f, assign)
{
    vec3f v1(1.0f, 2.0f, 3.0f);
    vec3f v2;

    v2 = v1;

    EXPECT_EQ(1.0f, v1.x);
    EXPECT_EQ(2.0f, v1.y);
    EXPECT_EQ(3.0f, v1.z);
    EXPECT_EQ(1.0f, v2.x);
    EXPECT_EQ(2.0f, v2.y);
    EXPECT_EQ(3.0f, v2.z);
}

TEST(vec3f, const_access)
{
    const vec3f v(1.0f, 2.0f, 3.0f);
    EXPECT_EQ(1.0f, v[0]);
    EXPECT_EQ(2.0f, v[1]);
    EXPECT_EQ(3.0f, v[2]);
}

TEST(vec3f, const_access_with_too_large_index)
{
    const vec3f v(1.0f, 2.0f, 3.0f);
    try {
        float f;
        f = v[17];
        FAIL()<< "Expected exception to be thrown, got " << f;
    } catch(const agge::core::index_out_of_bounds&) {
    } catch(...) {
        FAIL() << "Unknown exception was thrown";
    }
}

TEST(vec3f, access)
{
    vec3f v(1.0f, 2.0f, 3.0f);
    EXPECT_EQ(1.0f, v[0]);
    EXPECT_EQ(2.0f, v[1]);
    EXPECT_EQ(3.0f, v[2]);
    v[0] = 10.0f;
    v[1] = 11.0f;
    v[2] = 17.0f;
    EXPECT_EQ(10.0f, v[0]);
    EXPECT_EQ(11.0f, v[1]);
    EXPECT_EQ(17.0f, v[2]);
}

TEST(vec3f, access_with_too_large_index)
{
    vec3f v(1.0f, 2.0f, 3.0f);
    try {
        v[17] = 23.f;
        FAIL()<< "Expected exception to be thrown";
    } catch(const agge::core::index_out_of_bounds&) {
    } catch(...) {
        FAIL() << "Unknown exception was thrown";
    }
}

TEST(vec3f, add_assign)
{
    vec3f v1(1.0f, 2.0f, 3.0f);
    vec3f v2(1.0f, 2.0f, 3.0f);

    v2 += v1;
    EXPECT_EQ(2.0f, v2.x);
    EXPECT_EQ(4.0f, v2.y);
    EXPECT_EQ(6.0f, v2.z);

}

TEST(vec3f, subtract_assign)
{
    vec3f v1(1.0f, 2.0f, 3.0f);
    vec3f v2(1.0f, 2.0f, 3.0f);

    v2 -= v1;
    EXPECT_EQ(0.0f, v2.x);
    EXPECT_EQ(0.0f, v2.y);
    EXPECT_EQ(0.0f, v2.z);
}

TEST(vec3f, add)
{
    vec3f v1(1.0f, 2.0f, 3.0f);
    vec3f v2(1.0f, 2.0f, 3.0f);

    vec3f s = v1 + v2;
    EXPECT_EQ(2.0f, s.x);
    EXPECT_EQ(4.0f, s.y);
    EXPECT_EQ(6.0f, s.z);
}

TEST(vec3f, subtract)
{
    vec3f v1(1.0f, 2.0f, 3.0f);
    vec3f v2(1.0f, 2.0f, 3.0f);

    vec3f s = v1 - v2;
    EXPECT_EQ(0.0f, s.x);
    EXPECT_EQ(0.0f, s.y);
    EXPECT_EQ(0.0f, s.z);
}

TEST(vec3f, multiply_assign)
{
    vec3f v(1.0f, 2.0f, 3.0f);
    v *= 10.0f;
    EXPECT_EQ(10.0f, v.x);
    EXPECT_EQ(20.0f, v.y);
    EXPECT_EQ(30.0f, v.z);
}

TEST(vec3f, divide_assign)
{
    vec3f v(10.0f, 20.0f, 30.0f);
    v /= 2.0f;
    EXPECT_EQ(5.0f, v.x);
    EXPECT_EQ(10.0f, v.y);
    EXPECT_EQ(15.0f, v.z);
}

TEST(vec3f, multiply)
{
    vec3f v(1.0f, 2.0f, 3.0f);
    vec3f r = v * 10.0f;
    EXPECT_EQ(10.0f, r.x);
    EXPECT_EQ(20.0f, r.y);
    EXPECT_EQ(30.0f, r.z);
}

TEST(vec3f, minus)
{
    vec3f v(1.0f, 2.0f, 3.0f);
    vec3f m = -v;
    EXPECT_EQ(-1.0f, m.x);
    EXPECT_EQ(-2.0f, m.y);
    EXPECT_EQ(-3.0f, m.z);
}

TEST(vec3f, equals)
{
    vec3f v1(1.0f, 2.0f, 3.0f);
    vec3f v2(1.0f, 2.0f, 3.0f);
    vec3f v3(3.0f, 4.0f, 5.0f);

    EXPECT_TRUE(v1 == v2);
    EXPECT_FALSE(v1 == v3);
}

TEST(vec3f, not_equals)
{
    vec3f v1(1.0f, 2.0f, 3.0f);
    vec3f v2(1.0f, 2.0f, 3.0f);
    vec3f v3(3.0f, 4.0f, 5.0f);

    EXPECT_FALSE(v1 != v2);
    EXPECT_TRUE(v1 != v3);
}

class vec3f_output : public agge::test::output_test<vec3f>
{
};

TEST_P(vec3f_output, test_output )
{
    testOutput(GetParam().input, GetParam().expectation);
}

agge::test::output_expectation<vec3f> vec3f_output_fixture[] = {
{ vec3f::ZERO, "(0, 0, 0)" }, { vec3f::XYZ, "(1, 1, 1)" } };

INSTANTIATE_TEST_CASE_P(vec3f_output, vec3f_output,
                        ::testing::ValuesIn(vec3f_output_fixture));
