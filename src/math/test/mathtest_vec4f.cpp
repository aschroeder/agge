/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "agge/test/googletest.hpp"
#include "agge/test/output_test.hpp"

#include "agge/math/vec4f.hpp"
#include "agge/core/stdexceptions.hpp"

using agge::math::vec4f;

TEST(vec4f, default_construct)
{
    vec4f v1;
}

TEST(vec4f, construct_initialize_members)
{
    vec4f v(agge::core::initialize_members);
    EXPECT_EQ(0.0f, v.x);
    EXPECT_EQ(0.0f, v.y);
    EXPECT_EQ(0.0f, v.z);
    EXPECT_EQ(0.0f, v.w);
}

TEST(vec4f, construct_from_values)
{
    vec4f v(1.0f, 2.0f, 3.0f, 4.0f);
    EXPECT_EQ(1.0f, v.x);
    EXPECT_EQ(2.0f, v.y);
    EXPECT_EQ(3.0f, v.z);
    EXPECT_EQ(4.0f, v.w);
}

TEST(vec4f, construct_from_value)
{
    vec4f v(2.0f);
    EXPECT_EQ(2.0f, v.x);
    EXPECT_EQ(2.0f, v.y);
    EXPECT_EQ(2.0f, v.z);
    EXPECT_EQ(2.0f, v.w);
}

TEST(vec4f, construct_from_initializer)
{
    vec4f v = { 1.0f, 2.0f, 3.0f, 4.0f };
    EXPECT_EQ(1.0f, v.x);
    EXPECT_EQ(2.0f, v.y);
    EXPECT_EQ(3.0f, v.z);
    EXPECT_EQ(4.0f, v.w);
}

TEST(vec4f, construct_from_too_small_initializer)
{
    try {
        vec4f v = { 2.0f };
        FAIL()<< "No exception raised but got " << v;
    } catch(agge::core::illegal_argument&) {
    } catch(...) {
        FAIL() << "Unexpected exception raised";
    }
}

TEST(vec4f, construct_from_too_large_initializer)
{
    try {
        vec4f v = { 2.0f, 2.0f, 2.0f, 17.0f, 29.0f };
        FAIL()<< "No exception raised but got " << v;
    } catch(agge::core::illegal_argument&) {
    } catch(...) {
        FAIL() << "Unexpected exception raised";
    }
}

TEST(vec4f, construct_from_float)
{
    vec4f v(1.0f);
    EXPECT_EQ(1.0f, v.x);
    EXPECT_EQ(1.0f, v.y);
    EXPECT_EQ(1.0f, v.z);
    EXPECT_EQ(1.0f, v.w);
}

TEST(vec4f, construct_from_float_pointer)
{
    float x[] = { 1.0f, 2.0f, 3.0f, 4.0f };

    vec4f v(&x[0]);
    EXPECT_EQ(1.0f, v.x);
    EXPECT_EQ(2.0f, v.y);
    EXPECT_EQ(3.0f, v.z);
    EXPECT_EQ(4.0f, v.w);
}

TEST(vec4f, constants)
{
    vec4f v1 = vec4f::ZERO;
    vec4f v2 = vec4f::X;
    vec4f v3 = vec4f::Y;
    vec4f v4 = vec4f::Z;
    vec4f v5 = vec4f::XYZW;
    EXPECT_EQ(v1, vec4f::ZERO);
    EXPECT_NE(v2, vec4f::ZERO);
    EXPECT_NE(v3, vec4f::ZERO);
    EXPECT_NE(v4, vec4f::ZERO);
    EXPECT_NE(v5, vec4f::ZERO);
}

TEST(vec4f, assign)
{
    vec4f v1(1.0f, 2.0f, 3.0f, 4.0f);
    vec4f v2;

    v2 = v1;

    EXPECT_EQ(1.0f, v1.x);
    EXPECT_EQ(2.0f, v1.y);
    EXPECT_EQ(3.0f, v1.z);
    EXPECT_EQ(4.0f, v1.w);
    EXPECT_EQ(1.0f, v2.x);
    EXPECT_EQ(2.0f, v2.y);
    EXPECT_EQ(3.0f, v2.z);
    EXPECT_EQ(4.0f, v2.w);
}

TEST(vec4f, const_access)
{
    const vec4f v(1.0f, 2.0f, 3.0f, 4.0f);
    EXPECT_EQ(1.0f, v[0]);
    EXPECT_EQ(2.0f, v[1]);
    EXPECT_EQ(3.0f, v[2]);
    EXPECT_EQ(4.0f, v[3]);
}

TEST(vec4f, const_access_with_too_large_index)
{
    const vec4f v(1.0f, 2.0f, 3.0f, 4.0f);
    try {
        float f;
        f = v[17];
        FAIL()<< "Expected exception to be thrown but got " << f;
    } catch(const agge::core::index_out_of_bounds&) {
    } catch(...) {
        FAIL() << "Unknown exception was thrown";
    }
}

TEST(vec4f, access)
{
    vec4f v(1.0f, 2.0f, 3.0f, 4.0f);
    EXPECT_EQ(1.0f, v[0]);
    EXPECT_EQ(2.0f, v[1]);
    EXPECT_EQ(3.0f, v[2]);
    EXPECT_EQ(4.0f, v[3]);
    v[0] = 10.0f;
    v[1] = 11.0f;
    v[2] = 17.0f;
    v[3] = 29.0f;
    EXPECT_EQ(10.0f, v[0]);
    EXPECT_EQ(11.0f, v[1]);
    EXPECT_EQ(17.0f, v[2]);
    EXPECT_EQ(29.0f, v[3]);
}

TEST(vec4f, access_with_too_large_index)
{
    vec4f v(1.0f, 2.0f, 3.0f, 4.0f);
    try {
        v[17] = 23.f;
        FAIL()<< "Expected exception to be thrown";
    } catch(const agge::core::index_out_of_bounds&) {
    } catch(...) {
        FAIL() << "Unknown exception was thrown";
    }
}

TEST(vec4f, add_assign)
{
    vec4f v1(1.0f, 2.0f, 3.0f, 4.0f);
    vec4f v2(1.0f, 2.0f, 3.0f, 4.0f);

    v2 += v1;
    EXPECT_EQ(2.0f, v2.x);
    EXPECT_EQ(4.0f, v2.y);
    EXPECT_EQ(6.0f, v2.z);
    EXPECT_EQ(8.0f, v2.w);

}

TEST(vec4f, subtract_assign)
{
    vec4f v1(1.0f, 2.0f, 3.0f, 4.0f);
    vec4f v2(1.0f, 2.0f, 3.0f, 4.0f);

    v2 -= v1;
    EXPECT_EQ(0.0f, v2.x);
    EXPECT_EQ(0.0f, v2.y);
    EXPECT_EQ(0.0f, v2.z);
    EXPECT_EQ(0.0f, v2.w);
}

TEST(vec4f, add)
{
    vec4f v1(1.0f, 2.0f, 3.0f, 4.0f);
    vec4f v2(1.0f, 2.0f, 3.0f, 4.0f);

    vec4f s = v1 + v2;
    EXPECT_EQ(2.0f, s.x);
    EXPECT_EQ(4.0f, s.y);
    EXPECT_EQ(6.0f, s.z);
    EXPECT_EQ(8.0f, s.w);
}

TEST(vec4f, subtract)
{
    vec4f v1(1.0f, 2.0f, 3.0f, 4.0f);
    vec4f v2(1.0f, 2.0f, 3.0f, 4.0f);

    vec4f s = v1 - v2;
    EXPECT_EQ(0.0f, s.x);
    EXPECT_EQ(0.0f, s.y);
    EXPECT_EQ(0.0f, s.z);
    EXPECT_EQ(0.0f, s.w);
}

TEST(vec4f, multiply_assign)
{
    vec4f v(1.0f, 2.0f, 3.0f, 4.0f);
    v *= 10.0f;
    EXPECT_EQ(10.0f, v.x);
    EXPECT_EQ(20.0f, v.y);
    EXPECT_EQ(30.0f, v.z);
    EXPECT_EQ(40.0f, v.w);
}

TEST(vec4f, divide_assign)
{
    vec4f v(10.0f, 20.0f, 30.0f, 40.0f);
    v /= 2.0f;
    EXPECT_EQ(5.0f, v.x);
    EXPECT_EQ(10.0f, v.y);
    EXPECT_EQ(15.0f, v.z);
    EXPECT_EQ(20.0f, v.w);
}

TEST(vec4f, multiply)
{
    vec4f v(1.0f, 2.0f, 3.0f, 4.0f);
    vec4f r = v * 10.0f;
    EXPECT_EQ(10.0f, r.x);
    EXPECT_EQ(20.0f, r.y);
    EXPECT_EQ(30.0f, r.z);
    EXPECT_EQ(40.0f, r.w);
}

TEST(vec4f, minus)
{
    vec4f v(1.0f, 2.0f, 3.0f, 4.0f);
    vec4f m = -v;
    EXPECT_EQ(-1.0f, m.x);
    EXPECT_EQ(-2.0f, m.y);
    EXPECT_EQ(-3.0f, m.z);
    EXPECT_EQ(-4.0f, m.w);
}

TEST(vec4f, equals)
{
    vec4f v1(1.0f, 2.0f, 3.0f, 4.0f);
    vec4f v2(1.0f, 2.0f, 3.0f, 4.0f);
    vec4f v3(3.0f, 4.0f, 5.0f, 6.0f);

    EXPECT_TRUE(v1 == v2);
    EXPECT_FALSE(v1 == v3);
}

TEST(vec4f, not_equals)
{
    vec4f v1(1.0f, 2.0f, 3.0f, 4.0f);
    vec4f v2(1.0f, 2.0f, 3.0f, 4.0f);
    vec4f v3(3.0f, 4.0f, 5.0f, 6.0f);

    EXPECT_FALSE(v1 != v2);
    EXPECT_TRUE(v1 != v3);
}

class vec4f_output : public agge::test::output_test<vec4f>
{
};

TEST_P(vec4f_output, test_output )
{
    testOutput(GetParam().input, GetParam().expectation);
}

agge::test::output_expectation<vec4f> vec4f_output_fixture[] = {
{ vec4f::ZERO, "(0, 0, 0, 0)" }, { vec4f::XYZW, "(1, 1, 1, 1)" } };

INSTANTIATE_TEST_CASE_P(vec4f_output, vec4f_output,
                        ::testing::ValuesIn(vec4f_output_fixture));

