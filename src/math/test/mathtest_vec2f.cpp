/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "agge/test/googletest.hpp"
#include "agge/test/output_test.hpp"

#include "agge/math/vec2f.hpp"
#include "agge/core/stdexceptions.hpp"

using agge::math::vec2f;

TEST(vec2f, default_construct)
{
    vec2f v1;
}

TEST(vec2f, construct_initialize_members)
{
    vec2f v(agge::core::initialize_members);
    EXPECT_EQ(0.0f, v.x);
    EXPECT_EQ(0.0f, v.y);
}

TEST(vec2f, construct_from_values)
{
    vec2f v(1.0f, 2.0f);
    EXPECT_EQ(1.0f, v.x);
    EXPECT_EQ(2.0f, v.y);
}

TEST(vec2f, construct_from_value)
{
    vec2f v(2.0f);
    EXPECT_EQ(2.0f, v.x);
    EXPECT_EQ(2.0f, v.y);
}

TEST(vec2f, construct_from_initializer)
{
    vec2f v = { 1.0f, 2.0f };
    EXPECT_EQ(1.0f, v.x);
    EXPECT_EQ(2.0f, v.y);
}

TEST(vec2f, construct_from_too_small_initializer)
{
    try {
        vec2f v = { 2.0f };
        FAIL()<< "No exception raised, got " << v;
    } catch(agge::core::illegal_argument&) {
    } catch(...) {
        FAIL() << "Unexpected exception raised";
    }
}

TEST(vec2f, construct_from_too_large_initializer)
{
    try {
        vec2f v = { 2.0f, 2.0f, 2.0f };
        FAIL()<< "No exception raised, got " << v;
    } catch(agge::core::illegal_argument&) {
    } catch(...) {
        FAIL() << "Unexpected exception raised";
    }
}

TEST(vec2f, construct_from_float)
{
    vec2f v(1.0f);
    EXPECT_EQ(1.0f, v.x);
    EXPECT_EQ(1.0f, v.y);
}

TEST(vec2f, construct_from_float_pointer)
{
    float x[] = { 1.0f, 2.0f };

    vec2f v(&x[0]);
    EXPECT_EQ(1.0f, v.x);
    EXPECT_EQ(2.0f, v.y);
}

TEST(vec2f, constants)
{
    vec2f v1 = vec2f::ZERO;
    vec2f v2 = vec2f::X;
    vec2f v3 = vec2f::Y;
    vec2f v4 = vec2f::XY;

    EXPECT_EQ(v1, vec2f::ZERO);
    EXPECT_NE(v2, vec2f::ZERO);
    EXPECT_NE(v3, vec2f::ZERO);
    EXPECT_NE(v4, vec2f::ZERO);
}

TEST(vec2f, assign)
{
    vec2f v1(1.0f, 2.0f);
    vec2f v2;

    v2 = v1;

    EXPECT_EQ(1.0f, v1.x);
    EXPECT_EQ(2.0f, v1.y);
    EXPECT_EQ(1.0f, v2.x);
    EXPECT_EQ(2.0f, v2.y);
}

TEST(vec2f, const_access)
{
    const vec2f v(1.0f, 2.0f);
    EXPECT_EQ(1.0f, v[0]);
    EXPECT_EQ(2.0f, v[1]);
}

TEST(vec2f, const_access_with_too_large_index)
{
    const vec2f v(1.0f, 2.0f);
    try {
        float f;
        f = v[17];
        FAIL()<< "Expected exception to be thrown, got " << f;
    } catch(const agge::core::index_out_of_bounds&) {
    } catch(...) {
        FAIL() << "Unknown exception was thrown";
    }
}

TEST(vec2f, access)
{
    vec2f v(1.0f, 2.0f);
    EXPECT_EQ(1.0f, v[0]);
    EXPECT_EQ(2.0f, v[1]);
    v[0] = 10.0f;
    v[1] = 11.0f;
    EXPECT_EQ(10.0f, v[0]);
    EXPECT_EQ(11.0f, v[1]);
}

TEST(vec2f, access_with_too_large_index)
{
    vec2f v(1.0f, 2.0f);
    try {
        v[17] = 23.f;
        FAIL()<< "Expected exception to be thrown";
    } catch(const agge::core::index_out_of_bounds&) {
    } catch(...) {
        FAIL() << "Unknown exception was thrown";
    }
}

TEST(vec2f, add_assign)
{
    vec2f v1(1.0f, 2.0f);
    vec2f v2(1.0f, 2.0f);

    v2 += v1;
    EXPECT_EQ(2.0f, v2.x);
    EXPECT_EQ(4.0f, v2.y);

}

TEST(vec2f, subtract_assign)
{
    vec2f v1(1.0f, 2.0f);
    vec2f v2(1.0f, 2.0f);

    v2 -= v1;
    EXPECT_EQ(0.0f, v2.x);
    EXPECT_EQ(0.0f, v2.y);
}

TEST(vec2f, add)
{
    vec2f v1(1.0f, 2.0f);
    vec2f v2(1.0f, 2.0f);

    vec2f s = v1 + v2;
    EXPECT_EQ(2.0f, s.x);
    EXPECT_EQ(4.0f, s.y);
}

TEST(vec2f, subtract)
{
    vec2f v1(1.0f, 2.0f);
    vec2f v2(1.0f, 2.0f);

    vec2f s = v1 - v2;
    EXPECT_EQ(0.0f, s.x);
    EXPECT_EQ(0.0f, s.y);
}

TEST(vec2f, multiply_assign)
{
    vec2f v(1.0f, 2.0f);
    v *= 10.0f;
    EXPECT_EQ(10.0f, v.x);
    EXPECT_EQ(20.0f, v.y);
}

TEST(vec2f, divide_assign)
{
    vec2f v(10.0f, 20.0f);
    v /= 2.0f;
    EXPECT_EQ(5.0f, v.x);
    EXPECT_EQ(10.0f, v.y);
}

TEST(vec2f, multiply)
{
    vec2f v(1.0f, 2.0f);
    vec2f r = v * 10.0f;
    EXPECT_EQ(10.0f, r.x);
    EXPECT_EQ(20.0f, r.y);
}

TEST(vec2f, minus)
{
    vec2f v(1.0f, 2.0f);
    vec2f m = -v;
    EXPECT_EQ(-1.0f, m.x);
    EXPECT_EQ(-2.0f, m.y);
}

TEST(vec2f, equals)
{
    vec2f v1(1.0f, 2.0f);
    vec2f v2(1.0f, 2.0f);
    vec2f v3(3.0f, 4.0f);

    EXPECT_TRUE(v1 == v2);
    EXPECT_FALSE(v1 == v3);
}

TEST(vec2f, not_equals)
{
    vec2f v1(1.0f, 2.0f);
    vec2f v2(1.0f, 2.0f);
    vec2f v3(3.0f, 4.0f);

    EXPECT_FALSE(v1 != v2);
    EXPECT_TRUE(v1 != v3);
}


class vec2f_output : public agge::test::output_test<vec2f>
{
};

TEST_P(vec2f_output, test_output )
{
    testOutput(GetParam().input, GetParam().expectation);
}

agge::test::output_expectation<vec2f> vec2f_output_fixture[] = {
{ vec2f::ZERO, "(0, 0)" }, { vec2f::XY, "(1, 1)" } };

INSTANTIATE_TEST_CASE_P(vec2f_output, vec2f_output,
                        ::testing::ValuesIn(vec2f_output_fixture));
