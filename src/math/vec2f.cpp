/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/math/vec2f.hpp"
#include "agge/core/stdexceptions.hpp"

namespace agge {
    namespace math {

        const vec2f vec2f::ZERO(0.0f, 0.0f);
        const vec2f vec2f::X(1.0f, 0.0f);
        const vec2f vec2f::Y(0.0f, 1.0f);
        const vec2f vec2f::XY(1.0f, 1.0f);

        vec2f::vec2f()
        {
        }

        vec2f::vec2f(agge::core::initialize_members_tag)
                        : x(0), y(0)
        {
        }

        vec2f::vec2f(const vec2f& v)
                        : x(v.x), y(v.y)
        {
        }

        vec2f::vec2f(vec2f&& v)
                        : x(v.x), y(v.y)
        {

        }
        vec2f::vec2f(float f)
                        : x(f), y(f)
        {
        }

        vec2f::vec2f(float x_, float y_)
                        : x(x_), y(y_)
        {
        }

        vec2f::vec2f(const std::initializer_list<float>& l)
        {
            if (l.size() != 2) {
                throw AGGE_EXCEPTION(agge::core::illegal_argument)<< "Wrong number of arguments in initializer list, expected 2, found " << l.size();
            }
            auto i = l.begin();
            x = *i;
            ++i;
            y = *i;
        }

        vec2f::vec2f(const float *f)
                        : x(f[0]), y(f[1])
        {
        }

        vec2f&
        vec2f::operator =(const vec2f& v)
        {
            x = v.x;
            y = v.y;
            return *this;
        }

        vec2f&
        vec2f::operator =(vec2f&& v)
        {
            x = v.x;
            y = v.y;
            return *this;
        }

        float&
        vec2f::operator[](vec2f::size_type i)
        {
            if (i >= 2) {
                throw AGGE_EXCEPTION(agge::core::index_out_of_bounds)<< "Index out of bounds, expected to be in range 0..1, found "
                << i;
            }
            return *(&x + i);
        }

        const float&
        vec2f::operator[](vec2f::size_type i) const
        {
            if (i >= 2) {
                throw AGGE_EXCEPTION(agge::core::index_out_of_bounds)<< "Index out of bounds, expected to be in range 0..1, found "
                << i;
            }
            return *(&x + i);
        }

        vec2f&
        vec2f::operator +=(const vec2f& v)
        {
            x += v.x;
            y += v.y;
            return *this;
        }

        vec2f&
        vec2f::operator -=(const vec2f& v)
        {
            x -= v.x;
            y -= v.y;
            return *this;
        }

        vec2f vec2f::operator -() const
        {
            return vec2f(-x, -y);
        }

        vec2f operator +(const vec2f& a, const vec2f& b)
        {
            return vec2f(a.x + b.x, a.y + b.y);
        }

        vec2f operator -(const vec2f& a, const vec2f& b)
        {
            return vec2f(a.x - b.x, a.y - b.y);
        }

        bool operator ==(const vec2f& a, const vec2f& b)
        {
            return a.x == b.x && a.y == b.y;
        }

        bool operator !=(const vec2f& a, const vec2f& b)
        {
            return a.x != b.x || a.y != b.y;
        }

        std::ostream& operator <<(std::ostream& os, const vec2f& v)
        {
            os << "(" << v.x << ", " << v.y << ")";
            return os;
        }
    }
}
