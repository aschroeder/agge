/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/math/vec4f.hpp"
#include "agge/core/stdexceptions.hpp"

using agge::core::illegal_argument;
using agge::core::index_out_of_bounds;

namespace agge {
    namespace math {

        const vec4f vec4f::ZERO(0.0f, 0.0f, 0.0f, 0.0f);
        const vec4f vec4f::X(1.0f, 0.0f, 0.0f, 0.0f);
        const vec4f vec4f::Y(0.0f, 1.0f, 0.0f, 0.0f);
        const vec4f vec4f::Z(0.0f, 0.0f, 1.0f, 0.0f);
        const vec4f vec4f::W(0.0f, 0.0f, 0.0f, 1.0f);
        const vec4f vec4f::XYZW(1.0f, 1.0f, 1.0f, 1.0f);

        vec4f::vec4f()
        {
        }

        vec4f::vec4f(agge::core::initialize_members_tag)
                        : x(0), y(0), z(0), w(0)
        {
        }

        vec4f::vec4f(const vec4f& v)
                        : x(v.x), y(v.y), z(v.z), w(v.w)
        {
        }

        vec4f::vec4f(vec4f&& v)
                        : x(v.x), y(v.y), z(v.z), w(v.w)
        {
        }

        vec4f::vec4f(float f)
                        : x(f), y(f), z(f), w(f)
        {
        }

        vec4f::vec4f(float x_, float y_, float z_, float w_)
                        : x(x_), y(y_), z(z_), w(w_)
        {
        }

        vec4f::vec4f(const std::initializer_list<float>& l)
        {
            if (l.size() != 4) {
                throw AGGE_EXCEPTION(illegal_argument) << "Wrong number of arguments in initializer list, expected 4, found "
                      << l.size();
            }
            auto i = l.begin();
            x = *i;
            ++i;
            y = *i;
            ++i;
            z = *i;
            ++i;
            w = *i;
        }

        vec4f::vec4f(const float *f)
                        : x(f[0]), y(f[1]), z(f[2]), w(f[3])
        {
        }

        vec4f&
        vec4f::operator =(const vec4f& v)
        {
            x = v.x;
            y = v.y;
            z = v.z;
            w = v.w;
            return *this;
        }

        vec4f&
        vec4f::operator =(vec4f&& v)
        {
            x = v.x;
            y = v.y;
            z = v.z;
            w = v.w;
            return *this;
        }

        float&
        vec4f::operator[](vec4f::size_type i)
        {
            if (i >= 4) {
                throw AGGE_EXCEPTION(agge::core::index_out_of_bounds)<< "Index out of bounds, expected to be in range 0..3, found "
                << i;
            }
            return *(&x + i);
        }

        const float&
        vec4f::operator[](vec4f::size_type i) const
        {
            if (i >= 4) {
                throw AGGE_EXCEPTION(index_out_of_bounds)<< "Index out of bounds, expected to be in range 0..3, found "
                << i;
            }
            return *(&x + i);
        }

        vec4f&
        vec4f::operator +=(const vec4f& v)
        {
            x += v.x;
            y += v.y;
            z += v.z;
            w += v.w;
            return *this;
        }

        vec4f&
        vec4f::operator -=(const vec4f& v)
        {
            x -= v.x;
            y -= v.y;
            z -= v.z;
            w -= v.w;
            return *this;
        }

        vec4f vec4f::operator -() const
        {
            return vec4f(-x, -y, -z, -w);
        }

        vec4f operator +(const vec4f& a, const vec4f& b)
        {
            return vec4f(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
        }

        vec4f operator -(const vec4f& a, const vec4f& b)
        {
            return vec4f(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
        }

        bool operator ==(const vec4f& a, const vec4f& b)
        {
            return a.x == b.x && a.y == b.y && a.z == b.z && a.w == b.w;
        }

        bool operator !=(const vec4f& a, const vec4f& b)
        {
            return a.x != b.x || a.y != b.y || a.z != b.z || a.w != b.w;
        }

        std::ostream& operator <<(std::ostream& os, const vec4f& v)
        {
            os << "(" << v.x << ", " << v.y << ", " << v.z << ", " << v.w
               << ")";
            return os;
        }
    }
}
