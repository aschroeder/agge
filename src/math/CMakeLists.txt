# AGGE - Another Graphics/Game Engine
# Copyright (C) 2006-2015 by Alexander Schroeder
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License version 2 as published by the Free Software Foundation.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
ADD_LIBRARY(aggemath
            SHARED 
            vec2f.cpp
            vec3f.cpp
            vec4f.cpp) 
            
TARGET_LINK_LIBRARIES(aggemath aggecore)
TARGET_ADD_PROJECT_INCLUDES(aggemath)
TARGET_ADD_DEFINE(aggemath BUILD_AGGE_MATH)
ADD_SUBDIRECTORY(test)