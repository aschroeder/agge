/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/math/vec3f.hpp"
#include "agge/core/stdexceptions.hpp"

namespace agge {
    namespace math {

        const vec3f vec3f::ZERO(0.0f, 0.0f, 0.0f);
        const vec3f vec3f::X(1.0f, 0.0f, 0.0f);
        const vec3f vec3f::Y(0.0f, 1.0f, 0.0f);
        const vec3f vec3f::Z(0.0f, 0.0f, 1.0f);
        const vec3f vec3f::XYZ(1.0f, 1.0f, 1.0f);

        vec3f::vec3f()
        {
        }

        vec3f::vec3f(agge::core::initialize_members_tag)
                        : x(0), y(0), z(0)
        {
        }

        vec3f::vec3f(const vec3f& v)
                        : x(v.x), y(v.y), z(v.z)
        {
        }

        vec3f::vec3f(vec3f&& v)
                        : x(v.x), y(v.y), z(v.z)
        {
        }

        vec3f::vec3f(float f)
                        : x(f), y(f), z(f)
        {
        }

        vec3f::vec3f(float x_, float y_, float z_)
                        : x(x_), y(y_), z(z_)
        {
        }

        vec3f::vec3f(const std::initializer_list<float>& l)
        {
            if (l.size() != 3) {
                throw AGGE_EXCEPTION(agge::core::illegal_argument)<< "Wrong number of arguments in initializer list, expected 3, found " << l.size();
            }
            auto i = l.begin();
            x = *i;
            ++i;
            y = *i;
            ++i;
            z = *i;
        }

        vec3f::vec3f(const float *f)
                        : x(f[0]), y(f[1]), z(f[2])
        {
        }

        vec3f&
        vec3f::operator =(const vec3f& v)
        {
            x = v.x;
            y = v.y;
            z = v.z;
            return *this;
        }

        vec3f&
        vec3f::operator =(vec3f&& v)
        {
            x = v.x;
            y = v.y;
            z = v.z;
            return *this;
        }

        float&
        vec3f::operator[](vec3f::size_type i)
        {
            if (i >= 3) {
                throw AGGE_EXCEPTION(agge::core::index_out_of_bounds)<< "Index out of bounds, expected to be in range 0..2, found "
                << i;
            }
            return *(&x + i);
        }

        const float&
        vec3f::operator[](vec3f::size_type i) const
        {
            if (i >= 3) {
                throw AGGE_EXCEPTION(agge::core::index_out_of_bounds)<< "Index out of bounds, expected to be in range 0..2, found "
                << i;
            }
            return *(&x + i);
        }

        vec3f&
        vec3f::operator +=(const vec3f& v)
        {
            x += v.x;
            y += v.y;
            z += v.z;
            return *this;
        }

        vec3f&
        vec3f::operator -=(const vec3f& v)
        {
            x -= v.x;
            y -= v.y;
            z -= v.z;
            return *this;
        }

        vec3f vec3f::operator -() const
        {
            return vec3f(-x, -y, -z);
        }

        vec3f operator +(const vec3f& a, const vec3f& b)
        {
            return vec3f(a.x + b.x, a.y + b.y, a.z + b.z);
        }

        vec3f operator -(const vec3f& a, const vec3f& b)
        {
            return vec3f(a.x - b.x, a.y - b.y, a.z - b.z);
        }

        bool operator ==(const vec3f& a, const vec3f& b)
        {
            return a.x == b.x && a.y == b.y && a.z == b.z;
        }

        bool operator !=(const vec3f& a, const vec3f& b)
        {
            return a.x != b.x || a.y != b.y || a.z != b.z;
        }

        std::ostream& operator <<(std::ostream& os, const vec3f& v)
        {
            os << "(" << v.x << ", " << v.y << ", " << v.z << ")";
            return os;
        }

    }
}
