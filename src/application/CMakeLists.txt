# AGGE - Another Graphics/Game Engine
# Copyright (C) 2006-2015 by Alexander Schroeder
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License version 2 as published by the Free Software Foundation.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


ADD_LIBRARY(aggeapplication
            SHARED 
            application.cpp) 
            
TARGET_LINK_LIBRARIES(aggeapplication aggecore aggeevent)
TARGET_ADD_PROJECT_INCLUDES(aggeapplication)
TARGET_ADD_DEFINE(aggeapplication BUILD_AGGE_APPLICATION)
ADD_SUBDIRECTORY(test)