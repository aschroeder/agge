/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/application/application.hpp"

#include "agge/core/module_loader.hpp"
#include "agge/core/log.hpp"
#include "agge/core/exception.hpp"

#include "agge/event/event_queue.hpp"

using agge::core::log;
using agge::core::null_pointer;

namespace agge {
    namespace application {

        application* application::s_instance = 0;

        application::application()
                        : m_argv(0),
                          m_quit(false),
                          m_update_rate(25),
                          m_max_frame_skip(5),
                          m_last_time(-1),
                          m_elapsed_time(0)
        {
            s_instance = this;
        }

        void application::set_arguments(int argc, const char **argv)
        {
            log_application_info(argc, argv);
            process_arguments(argc, argv);
            core::module_loader::load_modules();
        }

        application::~application()
        {
            if (m_argv) {
                delete[] m_argv;
            }
            s_instance = 0;
        }

        void application::set_quit(bool quit)
        {
            if (m_quit && quit == false) {
                AGGE_ERROR_LOG(APPLICATION)
                                << "Try to reset application quit flag."
                                << log::flush;

                AGGE_THROW(agge::core::illegal_state)<< "Cannot reset application quit flag";
            }
            AGGE_DEBUG_LOG(APPLICATION) << "Set application quit flag." << log::flush;
            m_quit = quit;
        }

        bool application::is_quit() const
        {
            return m_quit;
        }

        void application::process_arguments(int argc, const char **argv)
        {
            m_program_name = argv[0];
            for (int i = 1; i < argc; ++i) {
                m_arguments.push_back(argv[i]);
            }

            filter_configuration_arguments();
            int old_argc = argc;
            // configuration arguments are now removed,
            // just use remaining arguments
            argc = get_argc();

            if (old_argc != argc) {
                AGGE_INFO_LOG(APPLICATION) << "Filtered " << (old_argc - argc)
                                             << " configuration arguments"
                                             << log::flush;
            }
            m_argv = new char *[(size_t) argc];
            for (size_t i = 1; i < (size_t) argc; ++i) {
                m_argv[i] = const_cast<char *>(m_arguments[i - 1].c_str());
            }
            m_argv[0] = const_cast<char *>(m_program_name.c_str());
        }

        void application::log_application_info(int argc, const char **argv)
        {
            AGGE_INFO_LOG(APPLICATION) << "Application " << argv[0]
                                         << " started." << log::flush;
            if (argc > 1) {
                AGGE_INFO_LOG(APPLICATION) << "Arguments:" << log::flush;
                for (int i = 1; i < argc; ++i) {
                    AGGE_INFO_LOG(APPLICATION) << "Argument[" << i << "] = '"
                                                 << argv[i] << "'."
                                                 << log::flush;
                }
            }
        }

        int application::get_argc() const
        {
            return static_cast<int>(m_arguments.size() + 1);
        }

        const char **
        application::get_argv() const
        {
            return const_cast<const char **>(m_argv);
        }

        const std::string&
        application::get_program_name() const
        {
            return m_program_name;
        }

        void application::set_update_rate(int update_rate)
        {
            if (update_rate <= 0) {
                AGGE_THROW(agge::core::illegal_argument);
            }
            m_update_rate = update_rate;
        }

        int application::get_update_rate() const
        {
            return m_update_rate;
        }

        int application::get_max_frame_skip() const
        {
            return m_max_frame_skip;
        }

        int application::run()
        {
            return 0;
        }

        void application::run_main_loop()
        {
            AGGE_DEBUG_LOG(APPLICATION) << "Main loop started" << log::flush;

            int loops = 0;
            std::int64_t next_game_tick =
                            agge::core::system::get_current_nanos();
            int update_rate = get_update_rate();
            AGGE_DEBUG_LOG(APPLICATION) << "Update rate: " << update_rate
                                         << " Hz" << log::flush;
            int skip_ticks = 1000000000 / update_rate;
            AGGE_DEBUG_LOG(APPLICATION) << "Skip ticks: " << skip_ticks
                                         << " ns" << log::flush;
            int max_frame_skip = get_max_frame_skip();
            AGGE_DEBUG_LOG(APPLICATION) << "Maximum skipped frames: "
                                         << max_frame_skip << log::flush;
            float interpolation = 0.0f;

            while (!is_quit()) {
                update_rate = get_update_rate();
                skip_ticks = 1000000000 / update_rate;
                max_frame_skip = get_max_frame_skip();

                loops = 0;

                for (;;) {
                    std::int64_t update_ts =
                                    agge::core::system::get_current_nanos();
                    if (!(update_ts > next_game_tick && loops < max_frame_skip)) {
                        break;
                    }
                    update_application(update_ts);
                    next_game_tick += skip_ticks;
                    ++loops;
                }
                std::int64_t display_ts =
                                agge::core::system::get_current_nanos();
                if (display_ts < next_game_tick) {
                    interpolation = (float) (next_game_tick - display_ts) / (float) skip_ticks;
                } else {
                    interpolation = (float) (next_game_tick + skip_ticks
                                    - display_ts)
                                    / (float) skip_ticks;
                }
                display_application(interpolation);
            }

            AGGE_DEBUG_LOG(APPLICATION) << "Main loop has finished"
                                         << log::flush;
        }

        void application::update_application(std::int64_t nanoseconds)
        {
            // update local timestamps
            if (m_last_time == -1) {
                m_elapsed_time = 0;
                m_last_time = nanoseconds;
            } else {
                m_elapsed_time = nanoseconds - m_last_time;
                m_last_time = nanoseconds;
            }

            // process the input
            agge::event::event_queue& q = agge::event::event_queue::get();
            if (q.is_live()) {
                try {
                    q.collect();
                    q.dispatch();
                } catch (const agge::core::exception& ce) {
                    AGGE_ERROR_LOG(APPLICATION)
                                    << "Exception caught in event dispatch: "
                                    << ce.what() << log::flush;
                    if (ce.get_backtrace()) {
                        AGGE_ERROR_LOG(APPLICATION) << "Backtrace: "
                                                     << *ce.get_backtrace()
                                                     << log::flush;
                    }
                } catch (const std::exception& e) {
                    AGGE_ERROR_LOG(APPLICATION)
                                    << "Exception caught in event dispatch: "
                                    << e.what() << log::flush;
                }
            }

            for (const auto& u : m_update_listeners) {
                try {
                    u.second(nanoseconds);
                } catch (const agge::core::exception& ce) {
                    AGGE_ERROR_LOG(APPLICATION)
                                    << "Exception caught in update step: "
                                    << ce.what() << log::flush;
                    if (ce.get_backtrace()) {
                        AGGE_ERROR_LOG(APPLICATION) << "Backtrace: "
                                                     << *ce.get_backtrace()
                                                     << log::flush;
                    }
                } catch (const std::exception& e) {
                    AGGE_ERROR_LOG(APPLICATION)
                                    << "Exception caught in update step: "
                                    << e.what() << log::flush;
                }
            }

            return;
        }

        unsigned int application::add_update_listener(const update_listener& l)
        {
            if (!l) {
                throw AGGE_EXCEPTION(null_pointer)
                                << "Update listener must not be null function";
            }
            return m_update_listeners.insert(l);
        }

        unsigned int application::add_display_listener(const display_listener& l)
        {
            if (!l) {
                throw AGGE_EXCEPTION(null_pointer)
                                << "Display listener must not be null function";
            }

            return m_display_listeners.insert(l);
        }

        bool application::remove_display_listener(unsigned int id)
        {
            auto it = m_display_listeners.find(id);
            if(it == m_display_listeners.end()) {
                return false;
            } else {
                m_display_listeners.erase(it);
                return true;
            }
        }

        bool application::remove_update_listener(unsigned int id)
        {
            auto it = m_update_listeners.find(id);
            if(it == m_update_listeners.end()) {
                return false;
            } else {
                m_update_listeners.erase(it);
                return true;
            }
        }

        void application::display_application(float interpolation)
        {
            for (const auto& f : m_display_listeners) {
                try {
                    f.second(interpolation);
                } catch (const agge::core::exception& ce) {
                    AGGE_ERROR_LOG(APPLICATION)
                                    << "Exception caught in display step: "
                                    << ce.what() << log::flush;
                    if (ce.get_backtrace()) {
                        AGGE_ERROR_LOG(APPLICATION) << "Backtrace: "
                                                     << *ce.get_backtrace()
                                                     << log::flush;
                    }
                } catch (const std::exception& e) {
                    AGGE_ERROR_LOG(APPLICATION)
                                    << "Exception caught in display step: "
                                    << e.what() << log::flush;
                }
            }
            return;
        }

        application * application::get_instance()
        {
            return s_instance;
        }

        AGGE_REGISTER_COMPONENT(application);

        int application::run_application(int argc, const char **argv)
        {
            std::vector<std::string> implementations;

            AGGE_GET_IMPLEMENTATIONS(application, implementations);

            if (implementations.empty()) {
                std::cerr << "No application implementation found!";
                ::exit(1);
            }

            if (implementations.size() != 1) {
                std::cerr << "Multiple application implementations found!";
                ::exit(1);
            }

            AGGE_INFO_LOG(APPLICATION) << "Running application: "
                                         << implementations[0] << "."
                                         << log::flush;
            application_ref app;
            int rc = 0;

            try {
                app = AGGE_CREATE_INSTANCE(application, implementations[0]);
                app->set_arguments(argc, argv);
            } catch (const core::exception& e) {
                AGGE_ERROR_LOG(APPLICATION)
                                << "Exception in application initialization: "
                                << e << log::flush;
                rc = 1;
            } catch (const std::exception& e) {
                AGGE_ERROR_LOG(APPLICATION)
                                << "Exception in application initialization: "
                                << e.what() << log::flush;
                rc = 1;
            }

            if (rc == 0) {
                try {
                    rc = app->run();
                    AGGE_INFO_LOG(APPLICATION)
                                    << "Application run method returned " << rc
                                    << "." << log::flush;
                } catch (const core::exception& e) {
                    AGGE_ERROR_LOG(APPLICATION) << "Exception in application: "
                                                 << e << log::flush;
                    rc = 1;
                } catch (const std::exception& e) {
                    AGGE_ERROR_LOG(APPLICATION) << "Exception in application: "
                                                 << e.what() << log::flush;
                    rc = 1;
                } catch (...) {
                    AGGE_ERROR_LOG(APPLICATION)
                                    << "Unknown exception in application."
                                    << log::flush;
                }
            }
            AGGE_INFO_LOG(APPLICATION) << "Application finished, rc = " << rc
                                         << log::flush;
            return rc;
        }

        void application::filter_configuration_arguments()
        {
            auto i = m_arguments.begin();
            while (i != m_arguments.end()) {
                if (*i == "--config") {
                    i = m_arguments.erase(i);
                    if (i != m_arguments.end()) {
                        std::string s = *i;
                        auto eqpos = s.find('=');
                        if (eqpos != std::string::npos) {
                            std::string key(s.begin(),
                                            s.begin() + (ptrdiff_t) eqpos);
                            std::string val(s.begin() + (ptrdiff_t) eqpos + 1,
                                            s.end());
                            AGGE_INFO_LOG(APPLICATION)
                                            << "Override config entry '" << key
                                            << "' with value '" << val << "'."
                                            << agge::core::log::flush;
                            core::configuration::set_override(key.c_str(),
                                                              val.c_str());
                            i = m_arguments.erase(i);
                        }
                    }
                } else {
                    ++i;
                }
            }
            return;
        }

    }
}
