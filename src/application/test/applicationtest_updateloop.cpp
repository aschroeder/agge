/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"
#include "agge/application/application.hpp"
#include "agge/core/stdexceptions.hpp"
#include "agge/core/system.hpp"

using agge::application::application;

class my_application;

class my_application : public application
{
public:
    my_application()
    {
        s_instance = this;
        update_called = 0;
        display_called = 0;
        bad_interpolation = 0.0f;
    }

    ~my_application()
    {
    }

    int run()
    {
        add_update_listener([&] (std::int64_t){
            update_called++;
            if(update_called == 75) {
                set_quit(true);
            }
        });
        add_display_listener([&] (float interpolation) {
            if(interpolation < 0.0f || interpolation >1.0f) {
                bad_interpolation = interpolation;
            }
            ++display_called;
        });


        std::int64_t start_ts = agge::core::system::get_current_nanos();
        application::run_main_loop();
        std::int64_t end_ts = agge::core::system::get_current_nanos();

        elapsed_time = (double) (end_ts - start_ts) / 1000000000.0;
        elapsed_time = std::round(elapsed_time);

        int argc = get_argc();
        char **argv = (char **) get_argv();
        ::testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
    }

    static my_application* s_instance;

    int update_called;
    int display_called;
    float bad_interpolation;
    double elapsed_time;

};

TEST(applicationupdateloop, correct_number_of_update_calls)
{
    EXPECT_EQ(75, my_application::s_instance->update_called);
}

TEST(applicationupdateloop, correct_elapsed_time)
{
    EXPECT_EQ(3.0, my_application::s_instance->elapsed_time);
}

TEST(applicationupdateloop, display_called_more_frequently)
{
    EXPECT_GT(my_application::s_instance->display_called,
              my_application::s_instance->update_called);
}

TEST(applicationupdateloop, interpolation_in_bounds)
{
    EXPECT_EQ(0.0, my_application::s_instance->bad_interpolation);
}

my_application* my_application::s_instance = 0;

AGGE_REGISTER_IMPLEMENTATION_ALIAS(my_application,
                                   agge::application::application, my_app);
AGGE_MAINFUNCTION
;
