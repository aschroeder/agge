/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"
#include "agge/application/application.hpp"
#include "agge/core/stdexceptions.hpp"

using agge::application::application;

class my_application : public application
{
public:
    my_application()
    {
        s_instance = this;
    }

    ~my_application()
    {
    }

    int run()
    {
        int argc = get_argc();
        char **argv = (char **) get_argv();
        ::testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
    }

    static my_application* s_instance;
};

TEST(application, instance)
{
    EXPECT_EQ(my_application::s_instance, application::get_instance());
}

TEST(application, update_rate)
{
    EXPECT_EQ(25, application::get_instance()->get_update_rate());
    application::get_instance()->set_update_rate(30);
    EXPECT_EQ(30, application::get_instance()->get_update_rate());
}

TEST(application, set_bad_update_rate)
{
    EXPECT_THROW(application::get_instance()->set_update_rate(-30),
                 agge::core::illegal_argument);
}

TEST(application, set_quit)
{
    EXPECT_FALSE(application::get_instance()->is_quit());
    application::get_instance()->set_quit(true);
    EXPECT_TRUE(application::get_instance()->is_quit());
    EXPECT_THROW(application::get_instance()->set_quit(false),
                 agge::core::illegal_state);
}

TEST(application, add_empty_display_listener)
{
    application::display_listener l;
    EXPECT_THROW(application::get_instance()->add_display_listener(l),
                 agge::core::null_pointer);
}

TEST(application, add_and_remove_display_listener)
{
    application::display_listener l = [](float) {
        std::cout << "foo!" << std::endl;
    };
    unsigned int l_id = application::get_instance()->add_display_listener(l);
    EXPECT_TRUE(application::get_instance()->remove_display_listener(l_id));
}

TEST(application, add_and_remove_wrong_display_listener)
{
    application::display_listener foo = [](float) {
        std::cout << "foo!" << std::endl;
    };
    unsigned int foo_id = application::get_instance()->add_display_listener(foo);
    EXPECT_FALSE(application::get_instance()->remove_display_listener(foo_id + 10));
    EXPECT_TRUE(application::get_instance()->remove_display_listener(foo_id));
}

AGGE_REGISTER_IMPLEMENTATION_ALIAS(my_application,
                                   agge::application::application, my_app);

my_application* my_application::s_instance = 0;

AGGE_MAINFUNCTION
