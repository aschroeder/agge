/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/graphics/display_system.hpp"
#include "agge/core/configuration.hpp"
#include "agge/core/component.hpp"
#include "agge/core/log.hpp"
#include "agge/core/stdexceptions.hpp"

namespace agge {
    namespace graphics {

        display_system::display_system()
        {
        }

        display_system::~display_system()
        {
        }

        static void store_default_config(core::configuration& config)
        {
            config.set("system", "opengl");
            config.store();
        }

        display_system_ref display_system::create(const char *implementation)
        {
            display_system_ref result;
            result = AGGE_CREATE_INSTANCE(display_system, implementation);
            return result;
        }

        display_system_ref display_system::create()
        {
            core::configuration display_config("display");
            if (!display_config.contains_key("system")) {
                store_default_config(display_config);
            }
            std::string name = display_config.get("system");
            return create(name.c_str());
        }

        const display_system_info&
        display_system::get_system_info() const
        {
            static display_system_info nil_info("", "", "", "");
            AGGE_UNSUPPORTED_OPERATION(get_system_info);
            return nil_info;
        }

        monitor_ref display_system::get_primary_monitor() const
        {
            AGGE_UNSUPPORTED_OPERATION(get_primary_monitor);
            return monitor_ref();
        }

        render_device_ref display_system::create_render_device(const dimension&)
        {
            render_device_ref result;
            AGGE_UNSUPPORTED_OPERATION(create_render_device);
            return result;
        }

        window_ref display_system::create_window(const dimension&,
                                                 const std::string&,
                                                 bool)
        {
            window_ref result;
            AGGE_UNSUPPORTED_OPERATION(create_window);
            return result;
        }

        AGGE_REGISTER_COMPONENT(display_system);
    }
}
