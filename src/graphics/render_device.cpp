/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/graphics/render_device.hpp"

namespace agge {
    namespace graphics {

        render_device*
        render_device::s_current_device = nullptr;

        render_device::render_device(const dimension& d)
                        : m_dimension(d)
        {
        }

        render_device::~render_device()
        {
            if(s_current_device == this) {
                s_current_device = nullptr;
            }
        }

        const dimension&
        render_device::get_dimension() const
        {
            return m_dimension;
        }

        bool
        render_device::is_current() const
        {
            return s_current_device == this;
        }

        void
        render_device::make_current()
        {
            if(!is_current()) {
                on_make_current();
            }
            s_current_device = this;
        }

        void
        render_device::on_make_current()
        {
            AGGE_UNSUPPORTED_OPERATION(render_device::on_make_current);
        }

        void
        render_device::flush()
        {
            on_flush();
        }

        void
        render_device::clear(const rgb_color& c)
        {
            on_clear(rgba_color(c, 1.0));
        }

        void
        render_device::clear(const rgba_color& c)
        {
            on_clear(c);
        }

        void
        render_device::on_flush()
        {
            AGGE_UNSUPPORTED_OPERATION(render_device::on_flush);
        }

        void
        render_device::on_clear(const rgba_color&)
        {
            AGGE_UNSUPPORTED_OPERATION(render_device::on_clear);
        }

    }
}
