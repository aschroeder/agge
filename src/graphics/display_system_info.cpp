/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/graphics/display_system_info.hpp"

namespace agge {
    namespace graphics {

        display_system_info::display_system_info(const std::string &api,
                                                 const std::string &version,
                                                 const std::string &gpu_vendor,
                                                 const std::string &gpu_product)

        :m_api(api),
         m_version(version),
         m_gpu_vendor(gpu_vendor),
         m_gpu_product(gpu_product)
        {}

        display_system_info::~display_system_info()
        {}

        const std::string&
        display_system_info::get_api() const
        {
            return m_api;
        }

        const std::string&
        display_system_info::get_version() const
        {
            return m_version;
        }

        const std::string&
        display_system_info::get_gpu_vendor() const
        {
            return m_gpu_vendor;
        }

        const std::string&
        display_system_info::get_gpu_product() const
        {
            return m_gpu_product;
        }
    }
}
