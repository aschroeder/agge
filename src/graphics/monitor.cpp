/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/graphics/monitor.hpp"

namespace agge {
    namespace graphics {

        monitor::monitor()
        {
        }

        monitor::~monitor()
        {
        }

        const dimension&
        monitor::get_dimension() const
        {
            static dimension d(0,0,0,0);
            return d;
        }

        bool
        monitor::is_primary() const
        {
            return true;
        }

        const std::string&
        monitor::get_name() const
        {
            static std::string n;
            return n;
        }

        monitor::video_mode_list
        monitor::get_video_modes() const
        {
            monitor::video_mode_list l;
            return l;
        }


    }
}
