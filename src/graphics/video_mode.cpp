/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/graphics/video_mode.hpp"

namespace agge {
    namespace graphics {
        video_mode::video_mode()
                        : m_width(0),
                          m_height(0),
                          m_red_bits(0),
                          m_green_bits(0),
                          m_blue_bits(0),
                          m_refresh_rate(0)
        {
        }

        video_mode::video_mode(unsigned int width, unsigned int height,
                               unsigned int red_bits, unsigned int green_bits,
                               unsigned int blue_bits,
                               unsigned int refresh_rate)
                        : m_width(width),
                          m_height(height),
                          m_red_bits(red_bits),
                          m_green_bits(green_bits),
                          m_blue_bits(blue_bits),
                          m_refresh_rate(refresh_rate)
        {
        }

        video_mode::~video_mode()
        {
        }
    }
}
