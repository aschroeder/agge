/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/graphics/rgba_color.hpp"
#include "agge/graphics/rgb_color.hpp"
#include "agge/core/stdexceptions.hpp"
#include "color_names.hpp"

using agge::core::illegal_argument;

namespace agge {
    namespace graphics {

        rgba_color::rgba_color(unsigned int rgbavalue)
        {
            r = ((float) ((rgbavalue >> 24) & 0xFF)) / 255.0f;
            g = ((float) ((rgbavalue >> 16) & 0xFF)) / 255.0f;
            b = ((float) ((rgbavalue >> 8) & 0xFF)) / 255.0f;
            a = ((float) (rgbavalue & 0xFF)) / 255.0f;
        }

        rgba_color::rgba_color(const rgb_color& c, float alpha)
        {
            r = c.r;
            g = c.g;
            b = c.b;
            a = alpha;
        }

        rgba_color::rgba_color(const char *name, float alpha)
                        : a(alpha)
        {
            const internal::color_name *c = internal::find_color(name);
            if (c == 0) {
                AGGE_THROW(illegal_argument) << "Unknown color name \""
                                                  << name << "\"";
            }
            r = c->r;
            g = c->g;
            b = c->b;
        }

        bool rgba_color::operator ==(const rgba_color& c) const
        {
            return r == c.r && g == c.g && b == c.b && a == c.a;
        }

        bool rgba_color::operator !=(const rgba_color& c) const
        {
            return r != c.r || g != c.g || b != c.b || a != c.a;
        }

        AGGE_GRAPHICS_EXPORT std::ostream&
        operator <<(std::ostream& o, const rgba_color& c)
        {
            return o << "rgba_color[r=" << c.r << ", g=" << c.g << ", b=" << c.b
                     << ", a=" << c.a << "]";
        }

    }
}
