/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/graphics/window.hpp"
#include "agge/graphics/rgb_color.hpp"

namespace agge {
    namespace graphics {

        window::window(const dimension& dimension, const std::string& title,
                       bool fullscreen)
                        : m_dimension(dimension),
                          m_title(title),
                          m_fullscreen(fullscreen),
                          m_visible(false),
                          m_display_listener_id(0)
        {
        }

        window::~window()
        {
        }

        const dimension& window::get_dimension() const
        {
            return m_dimension;
        }

        const std::string& window::get_title() const
        {
            return m_title;
        }

        bool window::is_fullscreen() const
        {
            return m_fullscreen;
        }

        void window::set_visible(bool visible)
        {
            if (visible != m_visible) {
                if (visible) {
                    on_show();
                    install_display_listener();
                } else {
                    uninstall_display_listener();
                    on_hide();
                }
                m_visible = visible;
            }
            return;
        }

        bool window::is_visible() const
        {
            return m_visible;
        }

        void window::on_show()
        {
            return;
        }

        void window::on_hide()
        {
            return;
        }

        void window::set_redraw_listener(const redraw_listener& l)
        {
            m_redraw_listener = l;
        }

        void window::set_close_listener(const close_listener& l)
        {
            m_close_listener = l;
        }

        void window::redraw(float interpolation)
        {
            if (m_redraw_listener) {
                m_redraw_listener(get_render_device(), interpolation);
            } else {
                default_redraw();
            }
            return;
        }

        void window::default_redraw()
        {
            const rgb_color black(0.0f, 0.0f, 0.0f);
            render_device &d = get_render_device();
            d.make_current();
            d.clear(black);
            d.flush();
            return;
        }

        void window::install_display_listener()
        {
            application::application *app =
                            application::application::get_instance();
            if (app) {
                m_display_listener_id = app->add_display_listener(
                                [&](float interpolation) {
                                    this->redraw(interpolation);
                                });
            }
            return;
        }

        void window::uninstall_display_listener()
        {
            if (m_display_listener_id) {
                application::application *app =
                                application::application::get_instance();
                if (app) {
                    app->remove_display_listener(m_display_listener_id);
                }
                m_display_listener_id = 0;
            }
        }

        bool window::on_closing()
        {
            if (m_closing_listener) {
                return m_closing_listener();
            } else {
                return true;
            }
        }

        void window::on_close()
        {
            if (m_close_listener) {
                m_close_listener();
            }
        }

    }
}
