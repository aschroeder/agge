/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"
#include "agge/core/stdexceptions.hpp"
#include "agge/graphics/rgb_color.hpp"
#include "agge/graphics/rgba_color.hpp"

using namespace agge::core;
using namespace agge::graphics;

TEST( rgb_color, construct_simple )
{
    rgb_color c;
    EXPECT_EQ(0.0f, c.r);
    EXPECT_EQ(0.0f, c.g);
    EXPECT_EQ(0.0f, c.b);
}

TEST( rgb_color, construct)
{
    rgb_color c(0.5f, 0.5f, 0.5f);
    EXPECT_EQ(0.5f, c.r);
    EXPECT_EQ(0.5f, c.g);
    EXPECT_EQ(0.5f, c.b);
}

TEST( rgb_color, construct_copy)
{
    rgb_color c0(0.5f, 0.5f, 0.5f);
    rgb_color c(c0);
    EXPECT_EQ(0.5f, c.r);
    EXPECT_EQ(0.5f, c.g);
    EXPECT_EQ(0.5f, c.b);
}

TEST( rgb_color, construct_bw)
{
    rgb_color c(0.5f);
    EXPECT_EQ(0.5f, c.r);
    EXPECT_EQ(0.5f, c.g);
    EXPECT_EQ(0.5f, c.b);
}

TEST( rgb_color, construct_array)
{
    float values[] = { 0.5f, 0.5f, 0.5f };
    rgb_color c(values);
    EXPECT_EQ(0.5f, c.r);
    EXPECT_EQ(0.5f, c.g);
    EXPECT_EQ(0.5f, c.b);
}

TEST ( rgb_color, construct_rgbvalue )
{
    unsigned int value = 0xFFAAEE;
    float b = (float) 0xEE / 255.0f;
    float g = (float) 0xAA / 255.0f;
    float r = 1.0f;

    rgb_color c(value);
    EXPECT_EQ(r, c.r);
    EXPECT_EQ(g, c.g);
    EXPECT_EQ(b, c.b);
}

TEST( rgb_color, construct_name )
{
    rgb_color black("black");
    rgb_color grey("grey");
    EXPECT_THROW(rgb_color c("unknowncolornot really there"), illegal_argument);
}

TEST( rgb_color, output )
{
    rgb_color c;
    std::stringstream ss;
    ss << c;
    EXPECT_EQ("rgb_color[r=0, g=0, b=0]", ss.str());
}

TEST( rgb_color, methods )
{
    rgb_color c;
    float f = 0.2345f;
    c.set_red(f);
    EXPECT_EQ(f, c.get_red());
    EXPECT_EQ(f, c.r);

    f = 0.3456f;
    c.set_green(f);
    EXPECT_EQ(f, c.get_green());
    EXPECT_EQ(f, c.g);

    f = 0.8563f;
    c.set_blue(f);
    EXPECT_EQ(f, c.get_blue());
    EXPECT_EQ(f, c.b);
}

TEST( rgb_color, eq )
{
    rgb_color c1;
    rgb_color c2;
    c1.set_red(0.456f);
    c2.set_red(0.456f);
    c1.set_green(0.345f);
    c2.set_green(0.345f);
    c1.set_blue(0.567f);
    c2.set_blue(0.567f);
    EXPECT_TRUE(c1 == c2);
}

TEST( rgb_color, ne )
{
    rgb_color c1;
    rgb_color c2;
    c1.set_red(0.456f);
    c2.set_red(0.457f);
    c1.set_green(0.345f);
    c2.set_green(0.345f);
    c1.set_blue(0.567f);
    c2.set_blue(0.567f);
    EXPECT_TRUE(c1 != c2);
}

TEST( rgba_color, construct_simple )
{
    rgba_color c;
    EXPECT_EQ(0.0f, c.r);
    EXPECT_EQ(0.0f, c.g);
    EXPECT_EQ(0.0f, c.b);
    EXPECT_EQ(0.0f, c.a);
}

TEST( rgba_color, construct_rgba )
{
    rgba_color c(0xFFAAEE77);
    EXPECT_EQ(((float )0xFF) / 255.0f, c.r);
    EXPECT_EQ(((float )0xAA) / 255.0f, c.g);
    EXPECT_EQ(((float )0xEE) / 255.0f, c.b);
    EXPECT_EQ(((float )0x77) / 255.0f, c.a);
}

TEST( rgba_color, construct_copy )
{
    rgba_color c0(0xFFAAEE77);
    rgba_color c(c0);
    EXPECT_EQ(((float )0xFF) / 255.0f, c.r);
    EXPECT_EQ(((float )0xAA) / 255.0f, c.g);
    EXPECT_EQ(((float )0xEE) / 255.0f, c.b);
    EXPECT_EQ(((float )0x77) / 255.0f, c.a);
}

TEST( rgba_color, construct_name )
{
    rgba_color black("black", 1.0f);
    rgba_color grey("grey");
    EXPECT_THROW(rgba_color c("unknowncolornot really there", 1.0f),
                 illegal_argument);
}

TEST( rgba_color, construct_bw )
{
    rgba_color c(0.5f, 1.0f);
    EXPECT_EQ(0.5f, c.r);
    EXPECT_EQ(0.5f, c.g);
    EXPECT_EQ(0.5f, c.b);
    EXPECT_EQ(1.0f, c.a);
}

TEST( rgba_color, construct_values )
{
    float values[] = { 0.5f, 0.4f, 0.3f, 0.2f };
    rgba_color c(values);
    EXPECT_EQ(values[0], c.r);
    EXPECT_EQ(values[1], c.g);
    EXPECT_EQ(values[2], c.b);
    EXPECT_EQ(values[3], c.a);
}

TEST( rgba_color, read_access )
{
    float values[] = { 0.5f, 0.4f, 0.3f, 0.2f };
    rgba_color c(values);
    EXPECT_EQ(values[0], c.get_red());
    EXPECT_EQ(values[1], c.get_green());
    EXPECT_EQ(values[2], c.get_blue());
    EXPECT_EQ(values[3], c.get_alpha());
}

TEST( rgba_color, write_access )
{
    float values[] = { 0.5f, 0.4f, 0.3f, 0.2f };
    rgba_color c;
    c.set_red(values[0]);
    c.set_green(values[1]);
    c.set_blue(values[2]);
    c.set_alpha(values[3]);
    EXPECT_EQ(values[0], c.get_red());
    EXPECT_EQ(values[1], c.get_green());
    EXPECT_EQ(values[2], c.get_blue());
    EXPECT_EQ(values[3], c.get_alpha());
}

TEST( rgba_color, output )
{
    rgba_color g(0.5f, 1.0f);
    std::stringstream ss;
    ss << g;
    EXPECT_EQ("rgba_color[r=0.5, g=0.5, b=0.5, a=1]", ss.str());
}

TEST( rgba_color, eq )
{
    rgba_color c1(0.5f, 1.0f);
    rgba_color c2(0.5f, 1.0f);
    rgba_color c3(0.5f, 0.5f);
    EXPECT_TRUE(c1 != c3);
    EXPECT_TRUE(c1 == c2);
}
