/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_GRAPHICS_GRAPHICSTEST_MOCKS_HPP
#define AGGE_GRAPHICS_GRAPHICSTEST_MOCKS_HPP

#include "agge/test/googlemock.hpp"
#include "agge/test/googletest.hpp"

#ifdef AGGE_GRAPHICS_RENDER_DEVICE_HPP
class MOCK_render_device : public agge::graphics::render_device
{
public:
    MOCK_render_device(): agge::graphics::render_device(agge::graphics::dimension(0,0,640,480))
    {}

    MOCK_METHOD0(on_make_current, void());

};
#endif

#ifdef AGGE_GRAPHICS_WINDOW_HPP
class MOCK_window : public agge::graphics::window
{
public:
    MOCK_window()
    : agge::graphics::window(agge::graphics::dimension(0,0,640,480), "MOCK_window", false)
    {}
    MOCK_window(const agge::graphics::dimension& dim,
                    const std::string& name,
                    bool fullscreen)
    : agge::graphics::window(dim, name, fullscreen)
    {}
    MOCK_METHOD0(on_show, void());
    MOCK_METHOD0(on_hide, void());

    agge::graphics::render_device& get_render_device()
    {
        return m_device;
    }

    MOCK_render_device m_device;
};
#endif

#endif
