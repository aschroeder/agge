/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"
#include "agge/graphics/render_device.hpp"
#include "agge/core/stdexceptions.hpp"
#include "graphicstest_mocks.hpp"

using namespace testing;
using agge::graphics::render_device;


TEST(render_device, make_current_callback_called)
{
    MOCK_render_device device;
    EXPECT_FALSE(device.is_current());
    EXPECT_CALL(device, on_make_current()).Times(1);
    device.make_current();
    EXPECT_TRUE(device.is_current());
}

TEST(render_device, current_device_changes)
{
    MOCK_render_device d1;
    EXPECT_FALSE(d1.is_current());
    EXPECT_CALL(d1, on_make_current()).Times(1);
    d1.make_current();
    EXPECT_TRUE(d1.is_current());

    {
        MOCK_render_device d2;
        EXPECT_FALSE(d2.is_current());
        EXPECT_CALL(d2, on_make_current()).Times(1);
        d2.make_current();
        EXPECT_TRUE(d2.is_current());
        EXPECT_FALSE(d1.is_current());
    }
}

TEST(render_device, device_not_current_on_exception)
{
    MOCK_render_device d1;
    EXPECT_FALSE(d1.is_current());
    EXPECT_CALL(d1, on_make_current())
            .Times(1)
            .WillRepeatedly(Throw(agge::core::illegal_state()));
    try {
        d1.make_current();
        FAIL() << "Expected exception thrown";
    } catch(const agge::core::illegal_state&) {

    }
    EXPECT_FALSE(d1.is_current());
}



