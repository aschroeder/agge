/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/graphics/window.hpp"
#include "graphicstest_mocks.hpp"

using agge::graphics::dimension;

TEST(window, title)
{
    MOCK_window w(dimension(0,0,640,480), "TITLE", false);
    EXPECT_EQ(w.get_title(), std::string("TITLE"));
}

TEST(window, fullscreen)
{
    MOCK_window w(dimension(0,0,640,480), "TITLE", true);
    EXPECT_TRUE(w.is_fullscreen());
}

TEST(window, initially_not_visible)
{
    MOCK_window w(dimension(0,0,640,480), "TITLE", true);
    EXPECT_FALSE(w.is_visible());
}

TEST(window, set_visible_true_calls_on_show)
{
    MOCK_window w(dimension(0,0,640,480), "TITLE", true);
    EXPECT_CALL(w, on_show()).Times(1);
    w.set_visible(true);
    EXPECT_TRUE(w.is_visible());
    w.set_visible(true);
}

TEST(window, set_visible_false_calls_on_hide)
{
    MOCK_window w(dimension(0,0,640,480), "TITLE", true);
    EXPECT_CALL(w, on_show()).Times(1);
    w.set_visible(true);
    EXPECT_TRUE(w.is_visible());
    EXPECT_CALL(w, on_hide()).Times(1);
    w.set_visible(false);
    EXPECT_FALSE(w.is_visible());
    w.set_visible(false);
}
