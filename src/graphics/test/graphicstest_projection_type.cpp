/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"
#include "output_test.hpp"
#include "agge/graphics/projection_type.hpp"

class projection_type_output_test : public agge::test::output_test<
                agge::graphics::projection_type>
{
};

TEST_P(projection_type_output_test, test_output)
{
    testOutput(GetParam().input, GetParam().expectation);
}

agge::test::output_expectation<agge::graphics::projection_type> projection_type_output_test_fixture[] =
                {
                { agge::graphics::projection_type::ORTHOGRAPHIC, "ORTHOGRAPHIC" },
                { agge::graphics::projection_type::PERSPECTIVE, "PERSPECTIVE" },
                { (agge::graphics::projection_type) 17, "PROJECTION_TYPE(17)" }, };

INSTANTIATE_TEST_CASE_P(
                projection_type_output_test, projection_type_output_test,
                ::testing::ValuesIn(projection_type_output_test_fixture));

