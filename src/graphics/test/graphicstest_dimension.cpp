/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"

#include "dimension.hpp"

using agge::graphics::dimension;

TEST(dimension, construct)
{
    dimension d;
    EXPECT_EQ(0, d.get_width());
    EXPECT_EQ(0, d.get_height());
}

TEST(dimension, construct_width_height)
{
    dimension d(640, 480);
    EXPECT_EQ(640, d.get_width());
    EXPECT_EQ(480, d.get_height());
}

TEST(dimension, construct_list_2)
{
    dimension d({640, 480});
    EXPECT_EQ(640, d.get_width());
    EXPECT_EQ(480, d.get_height());
    EXPECT_EQ(0, d.get_x());
    EXPECT_EQ(0, d.get_y());
}

TEST(dimension, construct_list_4)
{
    dimension d({37, 48, 640, 480});
    EXPECT_EQ(640, d.get_width());
    EXPECT_EQ(480, d.get_height());
    EXPECT_EQ(37, d.get_x());
    EXPECT_EQ(48, d.get_y());
}

TEST(dimension, construct_throws_with_bad_list)
{
	EXPECT_THROW(dimension d({1,2,3}), agge::core::illegal_argument);
}

TEST(dimension, get_right)
{
    dimension d({37, 48, 640, 480});
    EXPECT_EQ(640 + 37, d.get_right());
}

TEST(dimension, get_bottom)
{
    dimension d({37, 48, 640, 480});
    EXPECT_EQ(480 + 48, d.get_bottom());
}

TEST(dimension, equals)
{
    dimension d1({11, 12, 640, 480});
    dimension d2({11, 12, 640, 480});
    dimension d3({0, 0, 40, 30});

    EXPECT_TRUE(d1 == d2);
    EXPECT_FALSE(d1 == d3);
}

TEST(dimension, not_equals)
{
    dimension d1({11, 12, 640, 480});
    dimension d2({11, 12, 640, 480});
    dimension d3({0, 0, 40, 30});

    EXPECT_FALSE(d1 != d2);
    EXPECT_TRUE(d1 != d3);
}
