/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/graphics/rgb_color.hpp"
#include "color_names.hpp"
#include "agge/core/stdexceptions.hpp"

using agge::core::illegal_argument;

namespace agge {
    namespace graphics {

        rgb_color::rgb_color(unsigned int rgbvalue)
        {
            r = (float) ((rgbvalue >> 16) & 0xFF) / 255.0f;
            g = (float) ((rgbvalue >> 8) & 0xFF) / 255.0f;
            b = (float) (rgbvalue & 0xFF) / 255.0f;
        }

        rgb_color::rgb_color(const char *name)
        {
            const internal::color_name *c = internal::find_color(name);
            if (c == 0) {
                AGGE_THROW(illegal_argument) << "Unknown color name \""
                                                  << name << "\"";
            }
            r = c->r;
            g = c->g;
            b = c->b;
        }

        bool rgb_color::operator ==(const rgb_color& c) const
        {
            return r == c.r && g == c.g && b == c.b;
        }

        bool rgb_color::operator !=(const rgb_color& c) const
        {
            return r != c.r || g != c.g || b != c.b;
        }

        AGGE_GRAPHICS_EXPORT std::ostream&
        operator <<(std::ostream& o, const rgb_color& c)
        {
            return o << "rgb_color[r=" << c.r << ", g=" << c.g << ", b=" << c.b
                     << "]";
        }

    }
}
