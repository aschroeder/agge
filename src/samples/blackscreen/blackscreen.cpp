/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/application/application.hpp"
#include "agge/graphics/display_system.hpp"


using agge::application::application;
using agge::graphics::display_system;
using agge::graphics::display_system_ref;
using agge::graphics::window_ref;
using agge::graphics::dimension;

class blackscreen : public agge::application::application
{
public:
    blackscreen()
    {
    }

    ~blackscreen()
    {
    }

    int run()
    {
        m_display_system = agge::graphics::display_system::create();
        m_window = m_display_system->create_window(dimension(0, 0, 640, 480),
                                                   "blackscreen", false);
        m_window->set_close_listener([&] {
            set_quit(true);
        });
        m_window->set_visible(true);
        application::run_main_loop();
        return 0;
    }

    display_system_ref m_display_system;
    window_ref m_window;
};

AGGE_REGISTER_IMPLEMENTATION_ALIAS(blackscreen, agge::application::application,
                                   blackscreen);

int main(int argc, const char **argv)
{
    return agge::application::application::run_application(argc, argv);
}
