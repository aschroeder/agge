/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_MEMORY_LOG_HANDLER_HPP
#define AGGE_CORE_MEMORY_LOG_HANDLER_HPP

#include "agge/core/log_handler.hpp"
#include <vector>

namespace agge {
    namespace core {

        class memory_log_handler : public log_handler
        {
        private:
            typedef std::vector<log_record> log_record_collection;
        public:
            typedef log_record_collection::const_iterator const_iterator;

            memory_log_handler();
            virtual ~memory_log_handler();

            virtual void log(const log_record& record);
            virtual void configure(const configuration&);
            const_iterator begin() const;
            const_iterator end() const;

        private:
            log_record_collection m_log;
        };

    }
}

#endif
