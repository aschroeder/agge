/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/setup.hpp"
#include "agge/core/stdexceptions.hpp"
#include "agge/core/system.hpp"
#include "agge/core/log.hpp"
#include "agge/core/backtrace.hpp"

#ifdef AGGE_OS_LINUX
#  include <errno.h>
#  include <string.h>
#endif

#include <cstdio>
#include <iostream>

namespace agge {
    namespace core {

        namespace internal {

            void agge_unexpected_exception()
            {
                const exception *e = exception::get_last_exception();
                if (e) {
                    std::cerr << "Unexpected exception: " << *e << std::endl;
                    backtrace_ref r = e->get_backtrace();
                    if (r) {
                        std::cerr << *r;
                    } else {
                        backtrace::dump(std::cerr);
                    }
                    ::abort();
                } else {
                    std::cerr << "Unknown unexpected exception." << std::endl;
                    backtrace::dump(std::cerr);
                    ::abort();
                }
            }

            void agge_terminate()
            {
                const exception *e = exception::get_last_exception();
                if (e) {
                    std::cerr << "Program termination: " << *e << std::endl;
                    backtrace_ref r = e->get_backtrace();
                    if (r) {
                        std::cerr << *r;
                    } else {
                        backtrace::dump(std::cerr);
                    }
                    ::abort();
                } else {
                    std::cerr << "Program termination due to unknown error."
                              << std::endl;
                    backtrace::dump(std::cerr);
                    ::abort();
                }
            }

            struct set_exception_handlers
            {
                set_exception_handlers()
                {
                    std::set_unexpected(&agge_unexpected_exception);
                    std::set_terminate(&agge_terminate);
                }
            };
        }

        exception::backtrace_tag exception::backtrace;

        exception::exception()
                        : m_line(0), m_stream_modified(true)
        {
            install_handlers();
            s_last_exception = this;
        }

        exception::exception(const char *file, int line)
                        : m_line(line), m_file(file), m_stream_modified(true)
        {
            install_handlers();
            s_last_exception = this;
        }

        exception::~exception() throw ()
        {
            if (s_last_exception == this) {
                s_last_exception = 0;
            }
        }

        exception::exception(const exception& e)
                        : m_line(e.get_line()),
                          m_file(e.get_file()),
                          m_stream_modified(true),
                          m_backtrace(e.get_backtrace())
        {
            m_message_stream << e.get_message();
            s_last_exception = this;
        }

        int exception::get_line() const
        {
            return m_line;
        }

        const std::string&
        exception::get_file() const
        {
            return m_file;
        }

        backtrace_ref exception::get_backtrace() const
        {
            return m_backtrace;
        }

        const std::string exception::get_message() const
        {
            if (m_stream_modified) {
                materialize_message();
            }
            return m_message;
        }

        void exception::materialize_message() const
        {
            m_message = m_message_stream.str();
            m_stream_modified = false;
        }

        const char *
        exception::what() const throw ()
        {
            if (m_stream_modified) {
                materialize_message();
            }
            return m_message.c_str();
        }

        void exception::install_handlers()
        {
            static internal::set_exception_handlers set_handlers;
            return;
        }

        exception&
        exception::operator =(const exception& e)
        {
            if (&e != this) {
                m_line = e.get_line();
                m_file = e.get_file();
                m_backtrace = e.get_backtrace();
                m_message_stream.str(e.get_message());
                m_stream_modified = true;
            }
            return *this;
        }

        exception&
        exception::operator <<(const exception::backtrace_tag&)
        {
            m_backtrace = std::make_shared<agge::core::backtrace>();
            if (m_backtrace->empty()) {
                m_backtrace.reset();
            }
            return *this;
        }

        void exception::system_error(const char *file, const int line,
                                     bool check)
        {
            system_call_error(NULL, file, line, check);
        }

        void exception::system_call_error(const char *function,
                                          const char *file, const int line,
                                          bool check)
        {
#ifdef AGGE_OS_WINDOWS
            DWORD systemerror = GetLastError();
            if (systemerror == 0) {
                if (check) {
                    return;
                }
                agge::core::system_error result(file, line);
                if (function) {
                    result << "Unknown system error calling '" << function
                    << "' (no system error set).";
                } else {
                    result << "Unknown system error (no system error set).";
                }
                result << exception::backtrace;
                throw result;
            } else {
                char *msgbuf;
                char errorCodeBuf[20];
                FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                                NULL,
                                systemerror,
                                MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                                (LPTSTR) &msgbuf, 0,
                                NULL);
                std::sprintf(errorCodeBuf, "%X", (unsigned int) systemerror);
                agge::core::system_error result(file, line);
                if (function) {
                    result << "System error calling '" << function << "' ("
                    << errorCodeBuf << "):" << msgbuf;
                } else {
                    result << "System error (" << errorCodeBuf << "):"
                    << msgbuf;
                }
                LocalFree(msgbuf);
                result << exception::backtrace;
                throw result;
            }
#elif defined(AGGE_OS_LINUX) || defined(AGGE_OS_MACOSX)
            int systemerror = errno;
            if (systemerror == 0) {
                if (check) {
                    return;
                }
                agge::core::system_error result(file, line);
                if(function) {
                    result << "Unknown system error calling '" << function
                    << "' (no system error set).";
                } else {
                    result << "Unknown system error (no system error set).";
                }
                result << exception::backtrace;
                throw result;
            } else {
                char errorcodebuf[20];
                std::sprintf(errorcodebuf, "%d", systemerror);
                char msgbuf[512];
                char *message;
#ifdef AGGE_OS_MACOSX
                strerror_r(systemerror, msgbuf, sizeof(msgbuf));
                message = (char *)msgbuf;
#else
                message = strerror_r(systemerror, msgbuf, sizeof(msgbuf));
#endif
                agge::core::system_error result(file, line);
                if(function) {
                    result << "System error calling '" << function << "' (" << errorcodebuf << "):" << message;
                } else {
                    result << "System error (" << errorcodebuf << "):" << message;
                }
                result << exception::backtrace;
                throw result;
            }
#else
#  error Missing port.
#endif
        }

        std::ostream&
        operator <<(std::ostream& o, const exception& e)
        {
            o << e.get_file() << ":" << e.get_line() << ":" << e.get_message();
            backtrace_ref bt = e.get_backtrace();
            if (bt) {
                o << std::endl;
                o << *bt;
            }
            return o;
        }

        const exception *
        exception::get_last_exception()
        {
            return s_last_exception;
        }

        void exception::add_argument(const std::string& argument)
        {
            m_message_stream << argument;
            m_stream_modified = true;
            return;
        }

        const exception * exception::s_last_exception = 0;

    } // core
} // agge
