/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "file_log_handler.hpp"
#include "agge/core/exception.hpp"
#include <sstream>

namespace agge {
    namespace core {

        file_log_handler::file_log_handler()
                        : m_file(NULL)
        {
        }

        file_log_handler::~file_log_handler()
        {
            if (m_file) {
                fclose(m_file);
            }
        }

        void file_log_handler::log(const log_record& record)
        {
            if (m_file == 0) {
                return;
            }

            std::stringstream ss;
            m_formatter->format(ss, record);
            fputs(ss.str().c_str(), m_file);
            fflush(m_file);

            return;
        }

        void file_log_handler::configure(const configuration& config)
        {
            std::string formatter = config.get<std::string>("formatter",
                                                            "text");
            std::string file = config.get<std::string>("file", "log.txt");

            m_formatter = AGGE_CREATE_INSTANCE(log_formatter, formatter);
            if (!m_formatter) {
                AGGE_THROW(illegal_state)
                                << "Cannot initialize formatter: " << formatter;
            }

            m_file = fopen(file.c_str(), "w");
            return;
        }

    AGGE_REGISTER_IMPLEMENTATION_ALIAS(file_log_handler, log_handler, file);

}
}
