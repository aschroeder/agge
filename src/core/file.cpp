/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/file.hpp"
#include "agge/core/file_access_factory.hpp"
#include "agge/core/init_on_demand.hpp"
#include "agge/core/stdexceptions.hpp"

namespace agge {
    namespace core {

        class file_access_provider
        {
        public:
            file_access_provider()
            {
            }

            ~file_access_provider()
            {
            }

            file_access_ref get_file_access(const std::string& path)
            {
                if (!m_system_factory) {
                    m_system_factory = AGGE_CREATE_INSTANCE(file_access_factory,
                                                            "system");
                    if (!m_system_factory) {
                        throw AGGE_EXCEPTION(illegal_state)
                                        << "System file access factory not initialized";
                    }
                }

                file_access_ref result = m_system_factory->get_file_access(
                                path);
                return result;
            }

            file_access_ref get_file_access(const std::string& path,
                                            const std::string& name)
            {
                if (!m_system_factory) {
                    m_system_factory = AGGE_CREATE_INSTANCE(file_access_factory,
                                                            "system");
                    if (!m_system_factory) {
                        throw AGGE_EXCEPTION(illegal_state)
                                        << "System file access factory not initialized";
                    }
                }

                file_access_ref result = m_system_factory->get_file_access(
                                path, name);
                return result;
            }

            static file_access_provider& instance()
            {
                static init_on_demand<file_access_provider> provider;
                return *provider;
            }

            file_access_factory_ref m_system_factory;
        };

        file::file(const file_access_ref& access)
                        : m_access(access)
        {
        }

        file::file(const char *path)
        {
            m_access = file_access_provider::instance().get_file_access(
                            std::string(path));
        }

        file::file(const std::string& path)
        {
            m_access = file_access_provider::instance().get_file_access(path);
        }

        file::file(const path &path)
        {
            m_access = file_access_provider::instance().get_file_access(
                            path.get_generic_string());
        }

        file::file(const std::string& path, const std::string& name)
        {
            m_access = file_access_provider::instance().get_file_access(path,
                                                                        name);
        }

        file::~file()
        {
        }

        bool file::exists() const
        {
            return m_access->exists();
        }

        bool file::is_file() const
        {
            return m_access->is_file();
        }

        bool file::is_directory() const
        {
            return m_access->is_directory();
        }

        bool file::is_system_file() const
        {
            return m_access->is_system_file();
        }

        void file::mkdir()
        {
            m_access->mkdir();
        }

        void file::rmdir()
        {
            m_access->rmdir();
        }

        void file::list(std::vector<file>& files)
        {
            m_access->list(files);
        }

        input_stream_ref file::open_for_input()
        {
            return m_access->open_for_input();
        }

        std::string file::get_extension() const
        {
            return m_access->get_extension();
        }

        std::string file::get_path() const
        {
            return m_access->get_path();
        }

        std::string file::get_file_name() const
        {
            return m_access->get_file_name();
        }

        AGGE_CORE_EXPORT std::ostream&
        operator <<(std::ostream& os, const file& f)
        {
            return os << f.get_path();
        }

    }
}
