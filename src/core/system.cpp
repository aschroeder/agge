/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/setup.hpp"
#include "agge/core/system.hpp"
#include "agge/core/stdexceptions.hpp"

#include <cstdio>
#include <cstring>
#include <cstdarg>
#include <iostream>
#include <map>
#include <set>

#ifdef AGGE_OS_MACOSX
#  include <libkern/OSAtomic.h>
#endif

#ifdef AGGE_COMPILER_GNUC
extern "C" char *
__cxa_demangle(const char* __mangled_name, char* __output_buffer,
               size_t* __length, int* __status);
#endif

#if HAVE_MALLOC_H
#include <malloc.h>
#endif

#if defined(USE_POSIX_TIME_FUNCTIONS)
#  include <errno.h>
#  include <unistd.h>
#  include <time.h>
#  include <sys/time.h>
#endif

#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif

#if defined(AGGE_OS_LINUX) || defined(AGGE_OS_MACOSX)
extern const char *__progname;
#endif

namespace agge {
    namespace core {

        AGGE_CORE_EXPORT std::ostream&
        operator <<(std::ostream& o, const system::timestamp& ts)
        {
            char oldfill = o.fill();
            o.fill('0');
            o.width(4);
            o << ts.year << '-';
            o.width(2);
            o << (int) ts.month << '-';
            o.width(2);
            o << (int) ts.day << ' ';
            o.width(2);
            o << (int) ts.hour << ':';
            o.width(2);
            o << (int) ts.minute << ':';
            o.width(2);
            o << (int) ts.second << '.';
            o.width(3);
            o << ts.millisecond;
            o.fill(oldfill);
            return o;
        }

#ifdef AGGE_OS_WINDOWS
        static std::int64_t first_result = system::get_current_nanos();
#endif

        std::int64_t
        system::get_current_nanos()
        {
#ifdef AGGE_OS_WINDOWS

            union {
                FILETIME filetime;
                std::int64_t result;
            }tmp;
            GetSystemTimeAsFileTime(&tmp.filetime);
            return (tmp.result * 100) - first_result;
#elif defined(USE_POSIX_TIME_FUNCTIONS)
            struct timeval tv;
            if (gettimeofday(&tv, 0)) {
                return 0;
            } else {
                int64_t result = tv.tv_sec;
                result = result << 32;
                result |= tv.tv_usec;
                return result;
            }
#else
#  error "Not implemented."
#endif
        }


        void
        system::get_current_time(system::timestamp& ts)
        {
#ifdef AGGE_OS_WINDOWS
            SYSTEMTIME systime;
            GetLocalTime(&systime);
            ts.year = (unsigned short) systime.wYear;
            ts.month = (unsigned char) systime.wMonth;
            ts.day = (unsigned char) systime.wDay;
            ts.hour = (unsigned char) systime.wHour;
            ts.minute = (unsigned char) systime.wMinute;
            ts.second = (unsigned char) systime.wSecond;
            ts.millisecond = (unsigned short) systime.wMilliseconds;
            return;
#elif defined(USE_POSIX_TIME_FUNCTIONS)
            struct timeval tv;
            struct tm systime;
            gettimeofday(&tv, 0);
            localtime_r(&(tv.tv_sec), &systime);
            ts.year = (unsigned short) systime.tm_year + 1900;
            ts.month = (unsigned char) systime.tm_mon;
            ts.day = (unsigned char) systime.tm_mday;
            ts.hour = (unsigned char) systime.tm_hour;
            ts.minute = (unsigned char) systime.tm_min;
            ts.second = (unsigned char) systime.tm_sec;
            ts.millisecond = (unsigned short) (tv.tv_usec / 1000);
            return;
#else
#  error not implemented
#endif
        }

        bool
        system::atomic_cmpxchg(system::atomic_value_ptr destination,
                               system::atomic_value exchange,
                               system::atomic_value compare)
        {
        #if defined(AGGE_COMPILER_GNUC) && !defined(AGGE_OS_MACOSX)
        #  if defined(AGGE_CPU_I386) || defined(AGGE_CPU_X86_64)
            register system::atomic_value result;
            __asm__ __volatile__ (
                    "lock; cmpxchgl %2,%3"
                    : "=a" (result)
                    : "a" (compare), "q"(exchange), "m" (*destination)
                    : "memory"
            );
            return result==compare;
        #  else
        #  error CPU type not supported.
        #  endif
        #elif defined(AGGE_OS_MACOSX)
            return OSAtomicCompareAndSwap32(compare, exchange, (int32_t*) destination);
        #elif defined(AGGE_OS_WINDOWS)
            return InterlockedCompareExchange((LONG *)destination, exchange, compare) == compare;
        #else
        #  error "Missing port."
        #endif
        }

        system::atomic_value
        system::atomic_inc(system::atomic_value_ptr target)
        {
        #if defined(AGGE_COMPILER_GNUC)
        #  if defined(AGGE_CPU_I386) || defined(AGGE_CPU_X86_64)
            system::atomic_value result;
            volatile int value = 1;
            __asm__ __volatile__ ("lock; xaddl %0, %1"
                    : "=r" (result), "=m" (*target)
                    : "0" (value), "m" (*target)
            );
            return result + 1; // must return the new value
        #  else
        #    error Missing port
        #  endif
        #elif defined(AGGE_OS_MACOSX)
            return OSAtomicIncrement32((int32_t *) target);
        #elif defined(AGGE_OS_WINDOWS)
            return InterlockedIncrement((LONG *)target);
        #else
        #  error "Missing port."
        #endif
        }


        system::atomic_value
        system::atomic_dec(system::atomic_value_ptr target)
        {
        #if defined(AGGE_COMPILER_GNUC)
        #  if defined(AGGE_CPU_I386) || defined(AGGE_CPU_X86_64)
            system::atomic_value result;
            system::atomic_value increment = -1;
            __asm__ __volatile__ (
                    "lock\n\t"
                    "xaddl %0,(%1)"
                    :"=r" (result)
                    :"r" (target), "0" (increment)
                    :"memory" );
            return result - 1;
        #  else
        #    error Missing port
        #  endif
        #elif defined(AGGE_OS_MACOSX)
            return OSAtomicDecrement32((int32_t *) target);
        #elif defined(AGGE_OS_WINDOWS)
            return InterlockedDecrement((LONG *)target);
        #else
        #  error "Missing port."
        #endif
        }

        struct type_info_name_less
        {
            inline bool operator()(const char *a, const char *b) const
            {
                return strcmp(a, b) < 0;
            }
        };


        class type_info_name_cache
        {
        public:
            typedef std::map<const char *, std::string, type_info_name_less> cache_map;
            typedef cache_map::const_iterator cache_iterator;

            inline type_info_name_cache()
            {
            }

            inline ~type_info_name_cache()
            {
            }

            inline cache_iterator nil() const
            {
                return m_cached_values.end();
            }

            inline cache_iterator find(const char *name) const
            {
                return m_cached_values.find(name);
            }

            static inline type_info_name_cache *instance()
            {
                static type_info_name_cache cache;
                return &cache;
            }

            inline const std::string& put(const std::string& key,
                                          const std::string& value)
            {
                m_names.insert(key);
                std::set<std::string>::iterator i = m_names.find(key);
                m_cached_values.insert(std::make_pair(i->c_str(), value));
                cache_iterator ci = find(key.c_str());
                return ci->second;
            }

        private:
            std::set<std::string> m_names;
            cache_map m_cached_values;
        };


        const std::string&
        system::get_type_name(const std::type_info& typeinfo)
        {
            type_info_name_cache::cache_iterator ci =
                    type_info_name_cache::instance()->find(typeinfo.name());
            if (ci != type_info_name_cache::instance()->nil()) {
                return ci->second;
            }

            std::string str(typeinfo.name());
        #ifdef AGGE_COMPILER_GNUC
            __SIZE_TYPE__ sz = 511;
            int status;
            char *buffer;
            buffer = (char *) malloc(sz + 1);
            __cxa_demangle(str.c_str(), buffer, &sz, &status);
            if (status == 0) {
                std::string result(buffer);
                free(buffer);
                return type_info_name_cache::instance()->put(str, result);
            } else {
                switch (status) {
                case -1:
                    throw AGGE_EXCEPTION(out_of_memory) << "Memory allocation error.";
                    break;
                case -2:
                    throw AGGE_EXCEPTION(illegal_argument)
                            << "'" << str << "' is not a valid mangled type name.";
                    break;
                case -3:
                    throw AGGE_EXCEPTION(illegal_state)
                            << "Invalid arguments of demangling operation.";
                    break;
                }
            }
            throw AGGE_EXCEPTION(illegal_state)
                    << "Unknown error while determining type name.";

        #elif defined(AGGE_COMPILER_MSVC)
            if(str[0] == 'c' &&
                    str[1] == 'l' &&
                    str[2] == 'a' &&
                    str[3] == 's' &&
                    str[4] == 's' &&
                    str[5] == ' ') {
                return type_info_name_cache::instance()->put(str, std::string(str, 6));
            } else if(str[0] == 's' &&
                    str[1] == 't' &&
                    str[2] == 'r' &&
                    str[3] == 'u' &&
                    str[4] == 'c' &&
                    str[5] == 't' &&
                    str[6] == ' ') {
                return type_info_name_cache::instance()->put(str, std::string(str, 7));
            } else {
                return type_info_name_cache::instance()->put(str, str);
            }
        #else
        #  error Unknown compiler.
        #endif
        }


        std::string
        system::get_cwd()
        {
#ifdef AGGE_OS_WINDOWS
            again:
            DWORD sz = GetCurrentDirectory(0, 0);
            if(sz == 0) {
                AGGE_THROW_SYSTEMERROR;
            } else {
                char * ptr = new char[sz + 1];
                DWORD read_sz = GetCurrentDirectory(sz, ptr);
                if(read_sz == 0) {
                    AGGE_THROW_SYSTEMERROR;
                } else if(read_sz <= sz) {
                    std::string result(ptr);
                    delete [] ptr;
                    return result;
                } else {
                    delete [] ptr;
                    goto again;
                }
            }

            return std::string();
#elif defined(HAVE_UNISTD_H)
            std::size_t buffersize = 512;
            again: char *buffer = new char[buffersize];
            if (getcwd(buffer, buffersize) == 0) {
                if (errno == ERANGE) {
                    delete[] buffer;
                    buffersize += 256;
                    goto again;
                } else {
                    AGGE_THROW_SYSTEMERROR;
                }
            }

            std::string result(buffer);
            delete[] buffer;
            return result;
#else
#  error Missing port
#endif
        }

        void
        system::abort(const char *format, ...)
        {
            va_list ap;
            va_start(ap, format);

            char *buffer = (char *)alloca(strlen(format) + 1024);
            vsprintf(buffer, format, ap);
            std::cerr << buffer << std::endl;
            ::abort();
        }

        void
        system::debug_break()
        {
#ifdef AGGE_OS_WINDOWS
            DebugBreak();
#else
#endif
        }


        std::string
        system::get_unique_id(const std::string& prefix)
        {
            static system::atomic_value value;
            std::stringstream stream;
            stream << prefix << system::atomic_inc(&value);
            return stream.str();
        }

        std::string
        system::get_executable_name()
        {
#if defined(AGGE_OS_LINUX) || defined(AGGE_OS_MACOSX)
            return std::string(__progname);
#else
            char buffer[2048];
            GetModuleFileName(0, buffer, sizeof(buffer));
            char *dot = strrchr(buffer, '.');
            if(dot == nullptr) {
                throw AGGE_EXCEPTION(agge::core::illegal_state)
                   << "Cannot compute executable name (suffix not found).";
            }
            const char *basenamestart = strrchr(buffer, '\\');
            if(basenamestart == nullptr) {
                throw AGGE_EXCEPTION(agge::core::illegal_state)
                   << "Cannot compute executable name (directory not found).";
            }
            return std::string((const char *)basenamestart+1,
                               (const char *)dot);
#endif
        }

    } // core
} // agge

