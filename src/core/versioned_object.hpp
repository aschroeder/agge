/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_VERSIONED_OBJECT_HPP
#define AGGE_CORE_VERSIONED_OBJECT_HPP

#include "dllexport.hpp"
#include "globals.hpp"

namespace agge {
    namespace core {

        /**
         * Versioned object, allows to track mutations using a stamp.
         */
        class AGGE_EXPORT versioned_object
        {
        public:
            versioned_object();

            versioned_object(const versioned_object&) = default;
            versioned_object& operator =(const versioned_object&) = default;

            /**
             * Get version.
             * @return version
             */
            unsigned int get_version() const;

            /**
             * Update version.
             */
            inline void update_version()
            {
                m_version++;
                return;
            }

            /**
             * Perform a closure versioned. If the closure exists
             * normally, the version is increased.
             */
            template <typename Function>
            void do_versioned(Function f)
            {
                f();
                update_version();
            }

        private:
            unsigned int m_version;
        };

    }
}

#endif
