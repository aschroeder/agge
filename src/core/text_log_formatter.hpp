/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_TEXT_LOG_FORMATTER_HPP
#define AGGE_CORE_TEXT_LOG_FORMATTER_HPP

#include "agge/core/log_formatter.hpp"

namespace agge {
    namespace core {

        class text_log_formatter : public log_formatter
        {
        public:
            text_log_formatter();
            virtual ~text_log_formatter();

            virtual void format(std::ostream& os, const log_record& r);

            static void format_record(std::ostream& os, const log_record& r);
        private:
            static const char *format_severity(const log_severity& severity);
        };

    }
}

#endif
