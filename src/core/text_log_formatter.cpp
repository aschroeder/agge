/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "text_log_formatter.hpp"
#include <iostream>
#include <iomanip>
#include <cstdio>

namespace agge {
        namespace core {

        text_log_formatter::text_log_formatter()
        {}

        text_log_formatter::~text_log_formatter()
        {}

        const char *
        text_log_formatter::format_severity(const log_severity& severity)
        {
            if (severity == log_severity::E()) {
                return "E";
            } else if (severity == log_severity::W()) {
                return "W";
            } else if (severity == log_severity::I()) {
                return "I";
            } else if (severity == log_severity::D()) {
                return "D";
            } else {
                return "E";
            }
        }

        void
        text_log_formatter::format(std::ostream& os, const log_record& r)
        {
            format_record(os, r);
        }

        void
        text_log_formatter::format_record(std::ostream& os, const log_record& r)
        {
            os << r.get_timestamp() << " " << format_severity(r.get_severity()) << " "
               << std::left << std::setw(16) << r.get_category() << std::internal << " "
               << r.get_message() << std::endl;
        }

        AGGE_REGISTER_IMPLEMENTATION_ALIAS(text_log_formatter, log_formatter, text);

    }
}
