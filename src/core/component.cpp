/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/component.hpp"
#include "agge/core/init_on_demand.hpp"
#include "agge/core/exception.hpp"
#include "agge/core/string_functions.hpp"
#include "agge/core/log.hpp"

#include <map>
#include <list>
#include <vector>


namespace agge {
    namespace core {

        namespace internal {

            component_registry_entry_base::component_registry_entry_base()
            {}

            component_registry_entry_base::~component_registry_entry_base()
            {}

            void component_registry_entry_base::register_component(
                component_registry_entry_base *component)
            {
                component::register_component(component);
            }

            implementation_registry_entry_base::implementation_registry_entry_base()
            {}

            implementation_registry_entry_base::~implementation_registry_entry_base()
            {}

            void implementation_registry_entry_base::register_implementation(
                implementation_registry_entry_base *implementation)
            {
                component::register_implementation(implementation);
            }

            class component_registry
            {
            public:
                static component_registry& get()
                {
                    static init_on_demand<component_registry> registry;
                    return *(registry.ptr());
                }

                void register_component(component_registry_entry_base* component)
                {
                    AGGE_INFO_LOG(COMPONENT) << "Register component "
                                             << component->get_name() << "." << log::flush;

                    auto i = m_components.find(
                            component->get_name());
                    if (i == m_components.end()) {
                        m_components.insert(
                                std::make_pair(component->get_name(), component));
                        check_pending_implementations();
                    } else {
                        AGGE_THROW(duplicate_element) << "Component "
                                                      << component->get_name()
                                                      << " is already registered";
                    }
                }

                void register_implementation(
                        implementation_registry_entry_base *implementation)
                {
                    std::string component_name(implementation->get_component_name());
                    if (!is_component_registered(component_name)) {
                        add_pending_implementation(implementation);
                    } else {
                        add_implementation(component_name, implementation);
                    }
                }

                bool is_component_registered(const std::string& component_name) const
                {
                    return m_components.find(component_name) != m_components.end();
                }

                bool is_implementation_registered(
                        const std::string& component_name,
                        const std::string& implementation_name) const
                {
                    auto i = m_implementations.find(
                            component_name);
                    if (i != m_implementations.end()) {
                        if (i->second.find(implementation_name) != i->second.end()) {
                            return true;
                        }
                        auto j = m_aliases.find(
                                component_name);
                        if (j != m_aliases.end()) {
                            std::string normalized_impl_name(implementation_name);
                            trim(normalized_impl_name);
                            to_lower(normalized_impl_name);
                            if (j->second.find(normalized_impl_name) != j->second.end()) {
                                return true;
                            }
                        }
                    }
                    return false;
                }

                implementation_registry_entry_base *
                get_implementation(const std::string& component_name,
                                   const std::string& implementation_name) const
                {
                    auto i = m_implementations.find(
                            component_name);
                    if (i != m_implementations.end()) {
                        implementation_map::const_iterator impl_it;
                        if ((impl_it = i->second.find(implementation_name)) != i->second.end()) {
                            return impl_it->second;
                        }
                        auto j = m_aliases.find(
                                component_name);
                        if (j != m_aliases.end()) {
                            std::string normalized_impl_name(implementation_name);
                            trim(normalized_impl_name);
                            to_lower(normalized_impl_name);
                            implementations_by_alias::const_iterator alias_it;
                            if ((alias_it = j->second.find(normalized_impl_name)) != j->second.end()) {
                                impl_it = i->second.find(alias_it->second);
                                if (impl_it == i->second.end()) {
                                    AGGE_THROW(illegal_state)
                                            << "Internal error: registration of "
                                            << component_name << " corrupted";
                                }
                                return impl_it->second;
                            }
                        }
                    }
                    return 0;
                }

                void get_implementations(const std::string& component_name,
                                         std::vector<std::string>& implementations)
                {
                    implementations.clear();
                    auto i = m_implementations.find(component_name);
                    if (i != m_implementations.end()) {
                        auto impl_i = i->second.begin();
                        auto impl_e = i->second.end();
                        implementations.reserve(i->second.size());
                        while (impl_i != impl_e) {
                            implementations.push_back(impl_i->first);
                            ++impl_i;
                        }
                    }
                    return;
                }

            private:
                void check_pending_implementations()
                {
                    auto i = m_pending_implementations.begin();
                    while (i != m_pending_implementations.end()) {
                        if (is_component_registered((*i)->get_component_name())) {
                            add_implementation((*i)->get_component_name(), *i);
                            i = m_pending_implementations.erase(i);
                        } else {
                            ++i;
                        }
                    }
                }

                void add_pending_implementation(
                        implementation_registry_entry_base *implementation)
                {
                    m_pending_implementations.push_back(implementation);
                }

                void add_implementation(const std::string& component_name,
                                        implementation_registry_entry_base *implementation)
                {
                    AGGE_INFO_LOG(COMPONENT) << "Register implementation "
                                             << implementation->get_name()
                                             << " of component " << component_name << "."
                                             << log::flush;

                    implementation_map& m = m_implementations[component_name];
                    if (m.find(implementation->get_name()) != m.end()) {
                        AGGE_THROW(duplicate_element)
                                << "Implementation " << implementation->get_name()
                                << " already exists for component "
                                << implementation->get_component_name();
                    } else {
                        m.insert(
                                std::make_pair(implementation->get_name(), implementation));
                        add_aliases(component_name, implementation->get_name(),
                                    implementation->get_alias_names());
                    }
                }

                void add_aliases(const std::string& component_name,
                                 const char *implementation_name, const char *alias_names)
                {
                    std::vector<std::string> alias_names_collection;
                    split(alias_names_collection,
                          alias_names,
                          [] (char c) { return isspace(c); });
                    for(auto alias: alias_names_collection) {
                        to_lower(alias);
                        trim(alias);
                        if (!is_alias_known(component_name, alias)) {
                            aliases_by_component::iterator i = m_aliases.find(
                                    component_name);
                            if (i == m_aliases.end()) {
                                m_aliases.insert(
                                        std::make_pair(component_name,
                                                       implementations_by_alias()));
                                i = m_aliases.find(component_name);
                            }
                            i->second.insert(std::make_pair(alias, implementation_name));
                        }
                    }
                }

                bool is_alias_known(const std::string& component_name,
                                    const std::string& alias) const
                {
                    auto i = m_aliases.find(component_name);
                    if (i == m_aliases.end()) {
                        return false;
                    } else {
                        auto j = i->second.find(alias);
                        if (j != i->second.end()) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }

                typedef std::map<std::string, component_registry_entry_base *> component_map;
                typedef std::list<implementation_registry_entry_base *> pending_implementations_collection;
                typedef std::map<std::string, implementation_registry_entry_base *> implementation_map;
                typedef std::map<std::string, implementation_map> implementations_by_component;
                typedef std::map<std::string, std::string> implementations_by_alias;
                typedef std::map<std::string, implementations_by_alias> aliases_by_component;

                component_map m_components;
                pending_implementations_collection m_pending_implementations;
                implementations_by_component m_implementations;
                aliases_by_component m_aliases;

            };

        }

        component::component()
        {}

        component::~component()
        {}

        component_ref
        component::create(const std::string& component_name,
                          const std::string& impl_name)
        {
            internal::implementation_registry_entry_base *factory =
                    internal::component_registry::get().get_implementation(
                            component_name, impl_name);
            if (factory) {
                return factory->create();
            } else {
                return component_ref();
            }
        }

        void
        component::get_implementations(const std::string& component_name,
                                       std::vector<std::string>& implementations)
        {
            internal::component_registry::get().get_implementations(component_name,
                                                                    implementations);
        }

        bool
        component::is_component_registered(const std::string& component_name)
        {
            return internal::component_registry::get().is_component_registered(
                    component_name);
        }

        bool
        component::is_implementation_registered(const std::string& component_name,
                                                const std::string& implementation_name)
        {
            return internal::component_registry::get().is_implementation_registered(
                    component_name, implementation_name);
        }

        void
        component::register_component(const internal::component_registry_entry_base* component)
        {
            internal::component_registry::get().register_component(
                    const_cast<internal::component_registry_entry_base*>(component));
        }

        void
        component::register_implementation(internal::implementation_registry_entry_base* component)
        {
            internal::component_registry::get().register_implementation(component);
        }

    }
}
