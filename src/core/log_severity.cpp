/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/log_severity.hpp"
#include "agge/core/string_functions.hpp"

#include <cstring>

namespace agge {
     namespace core {

        const log_severity debug_log_severity("DEBUG", "D", 8);
        const log_severity& log_severity::LOG_SEVERITY_DEBUG()
        {
            return debug_log_severity;
        }

        const log_severity& log_severity::D()
        {
            return debug_log_severity;
        }

        const log_severity info_log_severity("INFO", "I", 4);

        const log_severity& log_severity::LOG_SEVERITY_INFO()
        {
            return info_log_severity;
        }

        const log_severity& log_severity::I()
        {
            return info_log_severity;
        }

        const log_severity warning_log_severity("WARNING", "W", 2);

        const log_severity& log_severity::LOG_SEVERITY_WARNING()
        {
            return warning_log_severity;
        }

        const log_severity& log_severity::W()
        {
            return warning_log_severity;
        }

        const log_severity error_log_severity("ERROR", "E", 1);

        const log_severity& log_severity::E()
        {
            return error_log_severity;
        }

        const log_severity& log_severity::LOG_SEVERITY_ERROR()
        {
            return error_log_severity;
        }

        const log_severity all_log_severity("ALL", "A", 15);
        const log_severity& log_severity::LOG_SEVERITY_ALL()
        {
            return all_log_severity;
        }

        log_severity::log_severity(const char *name,
                                   const char *abbrev,
                                   unsigned int level)
        :m_name(name),
         m_abbrev(abbrev),
         m_level(level)
        {}

        static inline const log_severity&
        get_severity_by_letter(const char c)
        {
            switch (c) {
            case 'i':
            case 'I':
                return log_severity::I();
            case 'd':
            case 'D':
                return log_severity::D();
            case 'W':
            case 'w':
                return log_severity::W();
            case 'E':
            case 'e':
                return log_severity::E();
            case 'A':
            case 'a':
                return log_severity::LOG_SEVERITY_ALL();
            default:
                return log_severity::E();
            }
        }

        const log_severity&
        log_severity::get(const std::string& namestr)
        {
            const char *name = namestr.c_str();

            if (name[0] && !name[1]) {
                return get_severity_by_letter(name[0]);
            } else {
                switch (name[0]) {
                case 'I':
                    if (strcmp(name, "INFO") == 0) {
                        return log_severity::I();
                    }
                    break;
                case 'D':
                    if (strcmp(name, "DEBUG") == 0) {
                        return log_severity::D();
                    }
                    break;
                case 'W':
                    if (strcmp(name, "WARNING") == 0) {
                        return log_severity::W();
                    }
                    break;
                case 'A':
                    if (strcmp(name, "ALL") == 0) {
                        return log_severity::LOG_SEVERITY_ALL();
                    }
                    break;
                case 'E':
                    return log_severity::E();
                default:
                    if(isspace(name[0])) {
                        return get(trim_copy(namestr));
                    } else {
                        return log_severity::E();
                    }
                }
            }
            return log_severity::E();
        }

    }
}
