/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "memory_log_handler.hpp"
#include <iostream>

namespace agge {
    namespace core {

        memory_log_handler::memory_log_handler()
        {}

        memory_log_handler::~memory_log_handler()
        {}

        void
        memory_log_handler::log(const log_record& record)
        {
            m_log.push_back(record);
        }

        void
        memory_log_handler::configure(const configuration&)
        {
            return;
        }

        memory_log_handler::const_iterator
        memory_log_handler::begin() const
        {
            return m_log.begin();
        }

        memory_log_handler::const_iterator
        memory_log_handler::end() const
        {
            return m_log.end();
        }

    }
}
