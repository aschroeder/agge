/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/backtrace.hpp"
#include "agge/core/globals.hpp"

#include <set>

#ifdef AGGE_COMPILER_MSVC
#  include <DbgHelp.h>
#  include <TlHelp32.h>
#  ifdef AGGE_CPU_X86_64
extern "C" {
    __declspec(dllimport) void RtlCaptureContext(CONTEXT *);
}
#  endif
#endif

namespace agge {
    namespace core {

        bool backtrace::is_supported()
        {
#ifdef AGGE_COMPILER_MSVC
            return true;
#else
            return false;
#endif
        }

        backtrace::backtrace()
        {
            fill();
        }

        backtrace::backtrace(const backtrace& b)
                        : base_type(b)
        {
        }

        backtrace::backtrace(backtrace && b)
                        : base_type(b)
        {
        }

        backtrace::~backtrace()
        {
        }

        backtrace &
        backtrace::operator =(const backtrace& b)
        {
            base_type::operator =(b);
            return *this;
        }

        std::ostream&
        operator <<(std::ostream& os, const backtrace& bt)
        {
            for (auto e : bt) {
                os << e << std::endl;
            }
            return os;
        }

        bool
        backtrace::contains(const char *methodname) const
        {
            for(const auto& e: *this) {
                if(e.get_function() == methodname) {
                    return true;
                }
            }
            return false;
        }

        void backtrace::dump(std::ostream& os)
        {
            backtrace bt;
            if (bt.empty()) {
                return;
            }
            bt.erase(bt.begin());
            os << bt;
            return;
        }

#ifdef AGGE_COMPILER_MSVC

        static bool symbols_initialized = false;
        static bool backtrace_possible = true;
        static std::set<HMODULE> modules;

        static void update_module_info()
        {
            HANDLE snap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, 0);
            if (snap == INVALID_HANDLE_VALUE) {
                return;
            }

            MODULEENTRY32 moduleentry;
            moduleentry.dwSize = sizeof(moduleentry);
            if (!Module32First(snap, &moduleentry)) {
                CloseHandle(snap);
                return;
            }

            HANDLE process = GetCurrentProcess();

            do {
                if (modules.find(moduleentry.hModule) != modules.end()) {
                    continue;
                }

                modules.insert(moduleentry.hModule);
#ifdef _WIN64
                DWORD64 baseaddr = (DWORD64)moduleentry.modBaseAddr;
#else
                DWORD64 baseaddr = (DWORD64)(DWORD)moduleentry.modBaseAddr;
#endif
                SymLoadModule64(process, nullptr,
                                moduleentry.szExePath,
                                moduleentry.szModule,
                                baseaddr,
                                moduleentry.modBaseSize);
                // cannot signal an error

            } while (Module32Next(snap, &moduleentry));

            CloseHandle(process);
            CloseHandle(snap);
        }

        static std::string stackbacktrace_get_symbol_search_path()
        {
            std::string result;
            char buffer[1024];

            if (GetCurrentDirectory(sizeof(buffer), buffer)) {
                result += buffer;
                result += ";";
            }

            if (GetModuleFileName(0, buffer, sizeof(buffer))) {
                size_t s = strlen(buffer) - 1;
                while (s != 0) {
                    if (buffer[s] == '/' || buffer[s] == ':'
                        || buffer[s] == '\\') {
                        break;
                    }
                    --s;
                }
                if (s != 0) {
                    if (buffer[s] == ':') {
                        buffer[s + 1] = '\0';
                    } else {
                        buffer[s] = '\0';
                    }
                    result += buffer;
                    result += ";";
                }
            }

            return result;
        }

        static void symbols_initialize()
        {
            DWORD sym_options = SymGetOptions();
            sym_options |= SYMOPT_LOAD_LINES | SYMOPT_DEFERRED_LOADS;
            SymSetOptions(sym_options);

            std::string path = stackbacktrace_get_symbol_search_path();

            BOOL rc = SymInitialize(GetCurrentProcess(), path.c_str(), TRUE);
            if (!rc) {
                backtrace_possible = false;
            }
            symbols_initialized = true;
        }

        void backtrace::fill()
        {
            if (!symbols_initialized) {
                symbols_initialize();
            }

            if (!backtrace_possible) {
                return;
            }

            update_module_info();

            if (!backtrace_possible) {
                return;
            }

            HANDLE current_process = GetCurrentProcess();
            HANDLE current_thread = GetCurrentThread();

            CONTEXT context;
            memset(&context, 0, sizeof(context));
            bool skipping = true;
#ifdef AGGE_CPU_I386
            context.ContextFlags = CONTEXT_CONTROL;
#ifndef CDT_EDITOR
            __asm {
                context_label:
                mov [context.Ebp], ebp;
                mov[context.Esp], esp;
                mov eax, [context_label];
                mov[context.Eip], eax
            }
#endif
            STACKFRAME64 frame;
            memset(&frame, 0, sizeof(frame));

            // DWORD machine_type = IMAGE_FILE_MACHINE_I386;
            frame.AddrPC.Offset = context.Eip;
            frame.AddrPC.Mode = AddrModeFlat;
            frame.AddrFrame.Offset = context.Ebp;
            frame.AddrFrame.Mode = AddrModeFlat;
            frame.AddrStack.Offset = context.Esp;
            frame.AddrStack.Mode = AddrModeFlat;

            while (size() < 100) {
                BOOL rc = StackWalk64(IMAGE_FILE_MACHINE_I386, current_process,
                                      current_thread, &frame,
                                      NULL,
                                      NULL, SymFunctionTableAccess64,
                                      SymGetModuleBase64,
                                      NULL);
                if (!rc || frame.AddrPC.Offset == 0) {
                    break;
                }
                IMAGEHLP_MODULE64 moduleinfo;
                memset(&moduleinfo, 0, sizeof(moduleinfo));
                moduleinfo.SizeOfStruct = sizeof(IMAGEHLP_MODULE64);
                if (!SymGetModuleInfo64(current_process, frame.AddrPC.Offset,
                                        &moduleinfo)) {
                    strcpy(moduleinfo.ModuleName, "");
                }

                // get symbol information
#define MAX_SYMBOL_NAME_LEN  1024
                DWORD64 symbolinfo_buffer[(sizeof(IMAGEHLP_SYMBOL64)
                                + MAX_SYMBOL_NAME_LEN)
                                          / sizeof(DWORD64)
                                          + 1];
                static IMAGEHLP_SYMBOL64* p_symbolinfo;
                static DWORD64 symboldisplacement;
                const char *methodname;

                p_symbolinfo = (IMAGEHLP_SYMBOL64 *) symbolinfo_buffer;
                p_symbolinfo->SizeOfStruct = sizeof(symbolinfo_buffer);
                p_symbolinfo->MaxNameLength = MAX_SYMBOL_NAME_LEN;
                symboldisplacement = 0;

                if (SymGetSymFromAddr64(current_process, frame.AddrPC.Offset,
                                        &symboldisplacement, p_symbolinfo)) {
                    methodname = (const char *) &(p_symbolinfo->Name[0]);
                } else {
                    methodname = "";
                }

                if (skipping) {
                    if (strcmp(methodname, "agge::core::backtrace::backtrace") == 0) {
                        skipping = false;
                    }
                    continue;
                }

                static IMAGEHLP_LINE64 lineinfo;
                static DWORD linedisplacement;

                lineinfo.SizeOfStruct = sizeof(IMAGEHLP_LINE64);

                if (SymGetLineFromAddr64(current_process, frame.AddrPC.Offset,
                                         &linedisplacement, &lineinfo)) {

                    push_back(backtrace_element((void *) frame.AddrPC.Offset,
                                                methodname, lineinfo.FileName,
                                                lineinfo.LineNumber,
                                                moduleinfo.ModuleName));

                } else {
                    push_back(backtrace_element((void *) frame.AddrPC.Offset,
                                                methodname, "", 0,
                                                moduleinfo.ModuleName));
                }
            }

#elif defined(AGGE_CPU_X86_64)
            RtlCaptureContext(&context);
            STACKFRAME64 frame;
            memset(&frame, 0, sizeof(frame));

            frame.AddrPC.Offset = context.Rip;
            frame.AddrPC.Mode = AddrModeFlat;
            frame.AddrFrame.Offset = context.Rsp;
            frame.AddrFrame.Mode = AddrModeFlat;
            frame.AddrStack.Offset = context.Rsp;
            frame.AddrStack.Mode = AddrModeFlat;

            while (size() < 100) {
                BOOL rc = StackWalk64(IMAGE_FILE_MACHINE_AMD64,
                                GetCurrentProcess(), GetCurrentThread(),
                                &frame,
                                &context,
                                NULL, SymFunctionTableAccess64,
                                SymGetModuleBase64,
                                NULL);
                if(!rc || frame.AddrPC.Offset == 0) {
                    break;
                }


                IMAGEHLP_MODULE64 moduleinfo;
                memset(&moduleinfo, 0, sizeof(moduleinfo));
                moduleinfo.SizeOfStruct = sizeof(IMAGEHLP_MODULE64);
                if (!SymGetModuleInfo64(current_process,
                                                frame.AddrPC.Offset,
                                                &moduleinfo)) {
                    strcpy(moduleinfo.ModuleName, "");
                }

                // get symbol information
#define MAX_SYMBOL_NAME_LEN  1024
                DWORD64 symbolinfo_buffer[(sizeof(IMAGEHLP_SYMBOL64)
                                + MAX_SYMBOL_NAME_LEN)
                / sizeof(DWORD64)
                + 1];
                static IMAGEHLP_SYMBOL64* p_symbolinfo;
                static DWORD64 symboldisplacement;
                const char *methodname;

                p_symbolinfo = (IMAGEHLP_SYMBOL64 *) symbolinfo_buffer;
                p_symbolinfo->SizeOfStruct = sizeof(symbolinfo_buffer);
                p_symbolinfo->MaxNameLength = MAX_SYMBOL_NAME_LEN;
                symboldisplacement = 0;

                if (SymGetSymFromAddr64(current_process,
                                                frame.AddrPC.Offset,
                                                &symboldisplacement, p_symbolinfo)) {
                    methodname = (const char *) &(p_symbolinfo->Name[0]);
                } else {
                    methodname = "";
                }

                if (skipping) {
                    if (strcmp(methodname, "agge::core::backtrace::backtrace") == 0) {
                        skipping = false;
                    }
                    continue;
                }


                static IMAGEHLP_LINE64 lineinfo;
                static DWORD linedisplacement;

                lineinfo.SizeOfStruct = sizeof(IMAGEHLP_LINE64);

                if (SymGetLineFromAddr64(current_process,
                                                frame.AddrPC.Offset,
                                                &linedisplacement, &lineinfo)) {

                    push_back(backtrace_element((void *) frame.AddrPC.Offset,
                                                    methodname,
                                                    lineinfo.FileName,
                                                    lineinfo.LineNumber,
                                                    moduleinfo.ModuleName));

                } else {
                    push_back(backtrace_element((void *) frame.AddrPC.Offset,
                                                    methodname,
                                                    "",
                                                    0,
                                                    moduleinfo.ModuleName));
                }
            }
#endif
            CloseHandle(current_process);
            CloseHandle(current_thread);
        }
#else
    void backtrace::fill()
    {
        return;
    }
#endif

}
}
