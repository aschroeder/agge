/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/path.hpp"
#include "agge/core/string_functions.hpp"

namespace agge {
    namespace core {

        path::path()
        :m_absolute(false)
        {}

        path::path(const char *p)
        :m_absolute(false)
        {
            construct(std::string(p));
            normalize();
        }

        void
        path::construct(const std::string& p)
        {
            split(m_elements, std::string(p),
                  [] (char c) { return c=='/' || c =='\\'; });
#if AGGE_OS_WINDOWS
            if(*m_elements[0].rbegin()==':') {
                m_absolute=true;
            }
#else
            m_absolute = p[0] == '/';
#endif
        }

        void
        path::normalize()
        {
            std::vector<std::string> new_elements;
            for(auto s : m_elements) {
                if(!s.empty()) {
                    new_elements.push_back(s);
                }
            }
            m_elements = new_elements;

        }

        path::path(const std::string& p)
        :m_absolute(false)
        {
            construct(p);
            normalize();
        }

        path::path(const path &p)
        :m_elements(p.m_elements),
         m_absolute(p.m_absolute)
        {}

        path::path(const path &p, const char *f)
        :m_elements(p.m_elements),
         m_absolute(p.m_absolute)
        {
            operator /=(f);
        }

        path::path(const path &p, const std::string& f)
        :m_elements(p.m_elements),
         m_absolute(p.m_absolute)
        {
            operator /=(f);
        }

        path::path(path &&p)
        :m_elements(p.m_elements),
         m_absolute(p.m_absolute)
        {}

        path::~path()
        {}

        path&
        path::operator /=(const path& p)
        {
            if(&p == this) {
                path pcopy(p);
                operator /= (pcopy);
            } else {
                for(auto s : p.m_elements) {
                    m_elements.push_back(s);
                }
            }
            return *this;
        }

        path&
        path::operator /=(const std::string& p)
        {
            return operator /=(path(p));
        }

        path&
        path::operator /=(const char * p)
        {
            return operator /=(path(p));
        }

        std::string
        path::get_native_string() const
        {
            std::string str;
#ifdef AGGE_OS_WINDOWS
            join(str, m_elements, '\\');
#else
            if(m_absolute) {
                str += "/";
            }
            join(str, m_elements, '/');
#endif
            return str;
        }

        std::string
        path::get_generic_string() const
        {
            std::string str;
#ifndef AGGE_OS_WINDOWS
            if(m_absolute) {
                str += "/";
            }
#endif
            join(str, m_elements, '/');
            return str;
        }

        bool
        path::is_absolute() const
        {
            return m_absolute;
        }

        std::string
        path::get_extension() const
        {
            if(!m_elements.empty()) {
                const char *dot = strrchr(m_elements.rbegin()->c_str(),
                                          '.');
                if(dot != nullptr) {
                    return std::string(dot);
                }
            }
            return "";
        }

        std::string
        path::get_file_name() const
        {
            if(m_elements.empty()) {
                return "";
            } else {
                return *m_elements.rbegin();
            }
        }

        AGGE_CORE_EXPORT std::ostream&
        operator << (std::ostream& os, const path& p)
        {
            os << p.get_generic_string();
            return os;
        }
    }
}
