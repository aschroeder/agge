/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/input_stream.hpp"

#include <cstring>

#define AGGE_STREAMBUF_BUFFER_SIZE 1024

namespace agge {
    namespace core {

        class input_stream_buffer : public std::streambuf
        {
        public:
            input_stream_buffer()
                            : m_stream(0),
                              m_buffer(0),
                              m_buffer_size(0),
                              m_buffer_used(0)
            {
            }

            input_stream_buffer(input_stream *stream)
                            : m_stream(stream),
                              m_buffer(0),
                              m_buffer_size(0),
                              m_buffer_used(0)
            {
                if (m_stream) {
                    m_buffer = new char[AGGE_STREAMBUF_BUFFER_SIZE];
                    m_buffer_size = AGGE_STREAMBUF_BUFFER_SIZE;
                    setg(m_buffer, m_buffer, m_buffer);
                }
            }

            input_stream_buffer(const input_stream_buffer& buffer)
                            : std::streambuf(),
                              m_stream(buffer.m_stream),
                              m_buffer(0),
                              m_buffer_size(0),
                              m_buffer_used(0)
            {
                if (m_stream) {
                    m_buffer_size = buffer.m_buffer_size;
                    m_buffer = new char[buffer.m_buffer_size];
                    memcpy(m_buffer, buffer.m_buffer, buffer.m_buffer_used);
                    setg(m_buffer, m_buffer + (buffer.gptr() - buffer.eback()),
                         m_buffer + m_buffer_used);
                }
            }

            ~input_stream_buffer()
            {
                if (m_buffer) {
                    delete[] m_buffer;
                }
            }

            input_stream_buffer& operator =(const input_stream_buffer& buffer)
            {
                if (this != &buffer) {
                    m_stream = buffer.m_stream;
                    delete[] m_buffer;
                    m_buffer = 0;
                    m_buffer_size = 0;
                    m_buffer_used = 0;
                    setg(0, 0, 0);
                    if (m_stream) {
                        m_buffer = new char[buffer.m_buffer_size];
                        m_buffer_size = buffer.m_buffer_size;
                        m_buffer_used = buffer.m_buffer_used;
                        memcpy(m_buffer, buffer.m_buffer, m_buffer_used);
                        setg(m_buffer,
                             m_buffer + (buffer.gptr() - buffer.eback()),
                             m_buffer + m_buffer_used);
                    }
                }
                return *this;
            }

            virtual std::streambuf::pos_type seekoff(
                            std::streamoff offset, std::ios_base::seekdir way,
                            std::ios_base::openmode)
            {
                input_stream::offset_type pos = -1;

                if (way == std::ios_base::beg) {
                    pos = m_stream->seek(offset, input_stream::POS_BEG);
                } else if (way == std::ios_base::end) {
                    pos = m_stream->seek(offset, input_stream::POS_END);
                } else {
                    pos = m_stream->seek(offset, input_stream::POS_CUR);
                }

                if (pos != -1) {
                    m_buffer_used = 0;
                    setg(m_buffer, m_buffer, m_buffer);
                    if (way != std::ios_base::end) {
                        underflow();
                    }
                }

                return pos;
            }

            virtual std::streambuf::int_type underflow()
            {
                if (gptr() < egptr()) {
                    return *(gptr());
                } else {
                    input_stream::streamsize_type bytes_read =
                                    m_stream->read(m_buffer,
                                                   static_cast<input_stream::streamsize_type>(m_buffer_size));
                    if (bytes_read == -1) {
                        setg(0, 0, 0);
                        return traits_type::eof();
                    } else {
                        m_buffer_used = (size_t)bytes_read;
                        setg(m_buffer, m_buffer, m_buffer + m_buffer_used);
                        return *(gptr());
                    }
                }
            }

        private:
            input_stream *m_stream;
            char *m_buffer;
            size_t m_buffer_size;
            size_t m_buffer_used;
        };

        class std_istream : public std::istream, public noncopyable
        {
        public:
            std_istream(input_stream *inputstream)
                            : std::istream(&m_buffer), m_buffer(inputstream)
            {
            }

            ~std_istream()
            {
            }
        private:
            input_stream_buffer m_buffer;
        };

        input_stream::input_stream()
        {
        }

        input_stream::~input_stream()
        {
        }

        std::istream&
        input_stream::get_istream()
        {
            if (!m_stdstream) {
                m_stdstream = std::make_shared<std_istream>(this);
            }

            return *m_stdstream;
        }

        input_stream::streamsize_type input_stream::read()
        {
            unsigned char c;
            if (read(&c, 1) == 1) {
                return (streamsize_type) c;
            } else {
                return -1;
            }
        }

        input_stream::streamsize_type input_stream::read(
                        void *, input_stream::streamsize_type)
        {
            return 0;
        }

        input_stream::offset_type input_stream::get_position()
        {
            return -1;
        }

        input_stream::offset_type input_stream::seek(
                        input_stream::offset_type, direction_type)
        {
            return -1;
        }

    }
}
