/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/file_access_factory.hpp"
#include "agge/core/file_access.hpp"
#include "agge/core/input_stream.hpp"
#include "agge/core/io_error.hpp"
#include "agge/core/file.hpp"
#include "agge/core/path.hpp"
#include "agge/core/log.hpp"

#include <iostream>
#include <cstdio>
#include <cstring>

#ifndef AGGE_OS_WINDOWS
#  include <sys/stat.h>
#  include <unistd.h>
#  include <dirent.h>
#endif

namespace agge {
    namespace core {

        class system_file_input_stream : public input_stream
        {
        public:
            system_file_input_stream(const std::string& filename)
            {
                m_file = fopen(filename.c_str(), "rb");
                if (!m_file) {
                    if (errno == ENOENT) {
                        throw AGGE_EXCEPTION(file_not_found)
                                        << "Cannot open file " << filename
                                        << " for reading";
                    } else {
                        throw AGGE_EXCEPTION(io_error) << "Cannot open file "
                                                       << filename
                                                       << " for reading";
                    }
                }
            }

            ~system_file_input_stream()
            {
                if (m_file) {
                    fclose(m_file);
                }
            }

            virtual offset_type get_position()
            {
                if (m_file) {
                    return ftell(m_file);
                } else {
                    return -1;
                }
            }

            virtual offset_type seek(offset_type offset, direction_type dir)
            {
#ifdef AGGE_OS_WINDOWS
#  define fseek _fseeki64
#endif
                if (m_file) {
                    int whence = get_whence(dir);
                    if (fseek(m_file, offset, whence)) {
                        throw AGGE_EXCEPTION(io_error)
                                        << "Seek operation failed";
                    }
                    return system_file_input_stream::get_position();
                } else {
                    return -1;
                }
#ifdef AGGE_OS_WINDOWS
#  undef fseek
#endif
            }

            virtual input_stream::streamsize_type read(
                            void *destination,
                            input_stream::streamsize_type size)
            {
                if (size < 0) {
                    throw AGGE_EXCEPTION(illegal_argument)
                                    << "Read size must be >= 0";
                }

                streamsize_type readbytes = (streamsize_type) fread(
                                destination, 1, (size_t) size, m_file);
                if (readbytes != size) {
                    if (!feof(m_file)) {
                        throw AGGE_EXCEPTION(io_error)
                                        << "File read failed, failed to read "
                                        << size << " bytes";
                    }
                }

                return readbytes;
            }

        private:
            static int get_whence(direction_type dir)
            {
                switch (dir) {
                case POS_BEG:
                    return SEEK_SET;
                case POS_CUR:
                    return SEEK_CUR;
                case POS_END:
                    return SEEK_END;
                default:
                    throw AGGE_EXCEPTION(io_error)
                                    << "Unsupported seek direction";
                }
            }
            FILE *m_file;
        };

        class system_file_access : public file_access
        {
        public:
            system_file_access(const std::string& path);
            system_file_access(const std::string& path,
                               const std::string& name);
            virtual ~system_file_access();

            virtual bool exists() const;
            virtual bool is_file() const;
            virtual bool is_directory() const;
            virtual bool is_system_file() const;

            virtual void mkdir();
            virtual void rmdir();
            virtual void list(std::vector<file>& files);

            virtual input_stream_ref open_for_input() const;
        private:
            path m_system_path;
        };

        system_file_access::system_file_access(const std::string& path)
                        : file_access(path), m_system_path(path)
        {
        }

        system_file_access::system_file_access(const std::string& path,
                                               const std::string& name)
                        : file_access(), m_system_path(path)
        {
            m_system_path /= name;
            set_path(m_system_path.get_generic_string());
        }

        system_file_access::~system_file_access()
        {
        }

        bool system_file_access::exists() const
        {
            std::string file_name = m_system_path.get_native_string();
#ifdef AGGE_OS_WINDOWS
            DWORD attrs = GetFileAttributes(file_name.c_str());
            return attrs != INVALID_FILE_ATTRIBUTES;
#else
            struct stat stat_info;
            int rc = ::stat(file_name.c_str(), &stat_info);
            return rc == 0;
#endif
        }

        bool system_file_access::is_file() const
        {
            std::string file_name = m_system_path.get_native_string();
#ifdef AGGE_OS_WINDOWS
            DWORD attrs = GetFileAttributes(file_name.c_str());
            if(attrs == INVALID_FILE_ATTRIBUTES) {
                return false;
            } else {
                return (attrs & FILE_ATTRIBUTE_DIRECTORY) == 0;
            }
#else
            struct stat stat_info;
            int rc = ::stat(file_name.c_str(), &stat_info);
            return rc == 0 && ((stat_info.st_mode & S_IFDIR) == 0);
#endif

        }

        bool system_file_access::is_directory() const
        {
            std::string file_name = m_system_path.get_native_string();
#ifdef AGGE_OS_WINDOWS
            DWORD attrs = GetFileAttributes(file_name.c_str());
            if(attrs == INVALID_FILE_ATTRIBUTES) {
                return false;
            } else {
                return (attrs & FILE_ATTRIBUTE_DIRECTORY) != 0;
            }
#else
            struct stat stat_info;
            int rc = ::stat(file_name.c_str(), &stat_info);
            return rc == 0 && ((stat_info.st_mode & S_IFDIR) != 0);
#endif
        }

        bool system_file_access::is_system_file() const
        {
            return true;
        }

        void system_file_access::mkdir()
        {
            std::string file_name = m_system_path.get_native_string();
#ifdef AGGE_OS_WINDOWS
            BOOL rc = CreateDirectory(file_name.c_str(), NULL);
            if(!rc) {
                try {
                    AGGE_THROW_SYSTEMERROR;
                } catch(const exception& e) {
                    throw AGGE_EXCEPTION(filesystem_error)
                    << "Cannot create directory '"
                    << get_path() << "': "
                    << e.get_message();
                }
            }
#else
            int rc = ::mkdir(file_name.c_str(),
                             S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
            if (rc == -1) {
                try {
                    AGGE_THROW_SYSTEMERROR;
                } catch(const exception& e) {
                    throw AGGE_EXCEPTION(filesystem_error)
                    << "Cannot create directory '"
                    << get_path() << "': "
                    << e.get_message();
                }
            }
#endif
            return;
        }

        void system_file_access::rmdir()
        {
            if (!is_directory()) {
                throw AGGE_EXCEPTION(filesystem_error)
                                << "'" << get_path() << "' is not a directory";
            }
            std::string file_name = m_system_path.get_native_string();
#ifdef AGGE_OS_WINDOWS
            BOOL rc = RemoveDirectory(file_name.c_str());
            if(!rc) {
                try {
                    AGGE_THROW_SYSTEMERROR;
                } catch(const exception& e) {
                    throw AGGE_EXCEPTION(filesystem_error)
                    << "Cannot remove directory '"
                    << get_path() << "': "
                    << e.get_message();
                }
            }
#else
            int rc = ::rmdir(file_name.c_str());
            if (rc != 0) {
                try {
                    AGGE_THROW_SYSTEMERROR;
                } catch(const exception& e) {
                    throw AGGE_EXCEPTION(filesystem_error)
                    << "Cannot remove directory '"
                    << get_path() << "': "
                    << e.get_message();
                }
            }
#endif
        }

        input_stream_ref system_file_access::open_for_input() const
        {
            std::string s = m_system_path.get_native_string();

            AGGE_DEBUG_LOG(SYSTEMFILE) << "Open '" << s << "' for input"
                                        << log::flush;

            return ::std::make_shared<system_file_input_stream>(s);
        }

        void system_file_access::list(std::vector<file>& files)
        {
            std::string pattern = m_system_path.get_native_string();
#ifdef AGGE_OS_WINDOWS
            pattern += "\\*";
            WIN32_FIND_DATA find_info;
            HANDLE dir_handle = FindFirstFile(pattern.c_str(),
                            &find_info);
            if(dir_handle != INVALID_HANDLE_VALUE) {
                do {
                    if(strcmp(find_info.cFileName, ".")!=0
                                    && strcmp(find_info.cFileName, "..")!=0) {
                        path p(m_system_path, find_info.cFileName);
                        files.push_back(file(p.get_generic_string()));
                    }
                }while(FindNextFile(dir_handle, &find_info));
                FindClose(dir_handle);
            }
#else
            DIR *dir = opendir(pattern.c_str());

            if (dir != NULL) {
                struct dirent *dp;
                while ((dp = readdir(dir)) != nullptr) {
                    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..")
                                    != 0) {
                        path p(m_system_path, dp->d_name);
                        files.push_back(file(p.get_generic_string()));
                    }
                }
                closedir(dir);
            }
#endif
        }

        class system_file_access_factory : public file_access_factory
        {
        public:
            system_file_access_factory();
            virtual ~system_file_access_factory();
            virtual file_access_ref get_file_access(const std::string& path);
            virtual file_access_ref get_file_access(
                            const std::string& path,
                            const std::string& filename);
        };

        AGGE_REGISTER_IMPLEMENTATION_ALIAS(system_file_access_factory,
                                           file_access_factory,
                                           system default file);

        system_file_access_factory::system_file_access_factory()
        {
        }

        system_file_access_factory::~system_file_access_factory()
        {
        }

        file_access_ref system_file_access_factory::get_file_access(
                        const std::string& path)
        {
            file_access_ref result = std::make_shared<system_file_access>(path);
            return result;
        }

        file_access_ref system_file_access_factory::get_file_access(
                        const std::string& path, const std::string& filename)
        {
            file_access_ref result = std::make_shared<system_file_access>(
                            path, filename);
            return result;
        }

    }
}
