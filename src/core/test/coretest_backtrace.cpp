/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"
#include "agge/core/backtrace.hpp"

using agge::core::backtrace;

#ifdef AGGE_COMPILER_MSVC

TEST(backtrace, construct)
{
    backtrace b;
    EXPECT_LT(0u, b.size());
    EXPECT_STREQ("backtrace_construct_Test::TestBody", b[0].get_function().c_str());
}


TEST(backtrace, contains_true)
{
    backtrace b;
    EXPECT_LT(0u, b.size());
    EXPECT_TRUE(b.contains("backtrace_contains_true_Test::TestBody"));
}

TEST(backtrace, contains_false)
{
    backtrace b;
    EXPECT_LT(0u, b.size());
    EXPECT_FALSE(b.contains("backtrace_contains_true_Test::TestBody"));
}

#endif
