/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "system.hpp"
#include "agge/test/googletest.hpp"

using namespace agge::core;

TEST( system, get_current_nanos )
{
    std::int64_t nanos = agge::core::system::get_current_nanos();
    std::int64_t nanos2 = agge::core::system::get_current_nanos();
    EXPECT_GE(nanos2, nanos);
}

TEST( system, atomic_inc )
{
    agge::core::system::atomic_value val(0);
    EXPECT_EQ(0, val);
    EXPECT_EQ(1, agge::core::system::atomic_inc(&val));
    EXPECT_EQ(1, val);
}

TEST( system, atomic_dec )
{
    agge::core::system::atomic_value val(1);
    EXPECT_EQ(1, val);
    EXPECT_EQ(0, agge::core::system::atomic_dec(&val));
    EXPECT_EQ(0, val);
}

TEST( system, atomic_cmpxchg )
{
    agge::core::system::atomic_value val(1);
    EXPECT_EQ(true, system::atomic_cmpxchg(&val, 17, 1));
    EXPECT_EQ(17, val);
    EXPECT_FALSE(system::atomic_cmpxchg(&val, 17, 1));
    EXPECT_EQ(17, val);
}
struct my_struct
{
};
class my_class
{
};
namespace my_namespace {
class my_class2
{
};
}

TEST ( system, type_name )
{
    EXPECT_EQ(std::string("my_struct"),
              system::get_type_name(typeid(my_struct)));
    EXPECT_EQ(std::string("my_class"), system::get_type_name(typeid(my_class)));
    EXPECT_EQ(std::string("int"), system::get_type_name(typeid(int)));
    EXPECT_EQ(std::string("my_namespace::my_class2"),
              system::get_type_name(typeid(my_namespace::my_class2)));
}

TEST ( system, current_time_year_is_absolute)
{
    system::timestamp ts;
    system::get_current_time(ts);
    EXPECT_LE(2000, ts.year);
}

TEST ( system, executable_name)
{
    std::string name = system::get_executable_name();
    EXPECT_STREQ("coretest", name.c_str());
}
