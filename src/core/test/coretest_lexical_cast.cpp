/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"
#include "lexical_cast.hpp"
#include <string>

TEST(lexical_cast, simple_int_to_string)
{
    std::string target;
    target = agge::core::lexical_cast<std::string>(17);
    EXPECT_EQ(std::string("17"), target);
}

TEST(lexical_cast, string_to_int_bad_cast)
{
    std::string source("wurst");
    EXPECT_THROW(int target = agge::core::lexical_cast<int>(source),
                 agge::core::bad_cast);
}

TEST(lexical_cast, string_to_int_incomplete_bad_cast)
{
    std::string source("124 wurst");
    EXPECT_THROW(int target = agge::core::lexical_cast<int>(source),
                 agge::core::bad_cast);
}
