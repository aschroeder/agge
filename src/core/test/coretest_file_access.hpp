/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef CORETEST_FILE_ACCESS_HPP
#define CORETEST_FILE_ACCESS_HPP

#include "agge/test/googletest.hpp"
#include "agge/test/googlemock.hpp"

#include "agge/core/file_access.hpp"
#include "agge/core/file.hpp"

using agge::core::input_stream_ref;

class MOCK_file_access : public agge::core::file_access
{
public:
    MOCK_file_access(const std::string& path)
                    : agge::core::file_access(path)
    {
    }

    MOCK_file_access()
                    : agge::core::file_access()
    {
    }

    MOCK_CONST_METHOD0(exists, bool());
    MOCK_CONST_METHOD0(is_file, bool());
    MOCK_CONST_METHOD0(is_directory, bool());
    MOCK_CONST_METHOD0(is_system_file, bool());
    MOCK_METHOD0(mkdir, void());
    MOCK_METHOD0(rmdir, void());
    MOCK_METHOD1(list, void(std::vector<agge::core::file>&));

    MOCK_CONST_METHOD0(open_for_input, input_stream_ref());

    void set_path(const std::string& path)
    {
        file_access::set_path(path);
    }
};

#endif
