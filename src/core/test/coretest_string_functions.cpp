/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"
#include "string_functions.hpp"

TEST(string_functions, trim_copy_both_sides)
{
    std::string to_trim("   wurst   ");
    EXPECT_EQ(std::string("wurst"), agge::core::trim_copy(to_trim));
}

TEST(string_functions, trim_copy_start)
{
    std::string to_trim("   wurst");
    EXPECT_EQ(std::string("wurst"), agge::core::trim_copy(to_trim));
}

TEST(string_functions, trim_copy_end)
{
    std::string to_trim("wurst   ");
    EXPECT_EQ(std::string("wurst"), agge::core::trim_copy(to_trim));
}

TEST(string_functions, trim_copy_nothing)
{
    std::string to_trim("wurst");
    EXPECT_EQ(std::string("wurst"), agge::core::trim_copy(to_trim));
}

TEST(string_functions, trim)
{
    std::string s("   wurst   ");
    agge::core::trim(s);
    EXPECT_EQ(std::string("wurst"), s);
}

TEST(string_functions, to_lower)
{
    std::string s("ABC");
    agge::core::to_lower(s);
    EXPECT_EQ(std::string("abc"), s);
}

TEST(string_functions, split_by_char)
{
    std::vector<std::string> v;
    agge::core::split(v, std::string("a,b,c"), ',');
    ASSERT_EQ(3,v.size());
    EXPECT_EQ(std::string("a"), v[0]);
    EXPECT_EQ(std::string("b"), v[1]);
    EXPECT_EQ(std::string("c"), v[2]);
}

TEST(string_functions, split_by_pred)
{
    std::vector<std::string> v;
    agge::core::split(v, std::string("a,b/c"),
                      [](char c){ return c==',' || c=='/';});
    ASSERT_EQ(3,v.size());
    EXPECT_EQ(std::string("a"), v[0]);
    EXPECT_EQ(std::string("b"), v[1]);
    EXPECT_EQ(std::string("c"), v[2]);
}
