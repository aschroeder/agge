/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"

#include "agge/core/init_on_demand.hpp"

using namespace agge::core;

struct init_on_demand_check
{
    init_on_demand_check()
    {
        initialized = true;
        destructed = false;
        field = 17;
    }

    ~init_on_demand_check()
    {
        destructed = true;
    }

    static bool initialized;
    static bool destructed;
    int field;
};

bool init_on_demand_check::initialized = false;
bool init_on_demand_check::destructed = false;

TEST(init_on_demand, init)
{
    {
        init_on_demand<init_on_demand_check> check;
        EXPECT_FALSE(init_on_demand_check::initialized);
        EXPECT_EQ(17, check->field);
        EXPECT_TRUE(init_on_demand_check::initialized);
    }
    EXPECT_TRUE(init_on_demand_check::destructed);
}

