/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"

#include "agge/core/configuration.hpp"

#include <fstream>

using agge::core::configuration;
using agge::core::configuration_key_not_found;

class configuration_test : public ::testing::Test
{
public:
    void SetUp()
    {
        configuration c;
        c.clear();
    }
};

TEST_F(configuration_test, empty_constructed)
{
    configuration c;
    EXPECT_EQ(true, c.empty());
}

TEST_F(configuration_test, simple_store)
{
    configuration c;
    c.set("configuration_test.key", "value");
    c.store();
}

TEST_F(configuration_test, simple_retrieve)
{
    {
        configuration c;
        c.set("configuration_test.key", "value");
        c.store();
    }
    {
        configuration c;
        std::string value = c.get("configuration_test.key");
        EXPECT_EQ(std::string("value"), value);
    }
}

TEST_F(configuration_test, simple_contains_key)
{
    {
        configuration c;
        c.set("configuration_test.key", "value");
        c.store();
    }
    {
        configuration c;
        EXPECT_TRUE(c.contains_key("configuration_test.key"));
    }
}

TEST_F(configuration_test, get_keys)
{
    {
        configuration c;
        c.set("configuration_test.keyA", "value");
        c.set("configuration_test.keyB", "value");
        c.store();
    }
    {
        configuration c;
        configuration::key_list l = c.get_keys();
        EXPECT_EQ(std::string("configuration_test.keyA"), l[0]);
        EXPECT_EQ(std::string("configuration_test.keyB"), l[1]);
    }
}

TEST_F( configuration_test, get_non_existent )
{
    configuration c;
    EXPECT_THROW(c.get<std::string>("anonexistentkey"),
                 configuration_key_not_found);
}

TEST_F( configuration_test, contains_key_for_non_existent )
{
    configuration c;
    EXPECT_FALSE(c.contains_key("anonexistentkey"));
}


TEST_F( configuration_test, clear )
{
    configuration c;
    c.set("configuration_test.key", "value");
    EXPECT_FALSE(c.empty());
    c.clear();
    EXPECT_TRUE(c.empty());
}

TEST_F( configuration_test, clear_clears_persistence )
{
    configuration c;
    c.set("configuration_test.key", "value");
    EXPECT_FALSE(c.empty());
    c.clear();
    configuration c2;
    EXPECT_TRUE(c2.empty());
}



