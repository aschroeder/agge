/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"

#include "agge/core/stdexceptions.hpp"

using namespace agge::core;
using testing::Types;

typedef testing::Types<illegal_argument, null_pointer, illegal_state,
                index_out_of_bounds, not_implemented, system_error,
                configuration_error, duplicate_element, out_of_memory,
                unsupported_operation, runtime_exception, no_such_element,
                pure_virtual_call> thrown_exceptions;

template<class T>
class core_exceptions : public testing::Test
{
};

TYPED_TEST_CASE(core_exceptions, thrown_exceptions);

TYPED_TEST(core_exceptions, check_catch){
try {
    throw AGGE_EXCEPTION(TypeParam);
    FAIL() << "expected exception notthrown";
} catch(const agge::core::exception&) {
} catch(...) {
    FAIL() << "exception not catched as agge::core::exception";
}
}

TEST(exception, what_returns_message)
{
    agge::core::exception e(__FILE__, __LINE__);
    e << "My hoovercraft is full of eels";
    ASSERT_STREQ("My hoovercraft is full of eels", e.what());
}

TEST(exception, pure_virtual_call)
{
    int line = 0;
    const char *file = __FILE__;
    try {
        line = __LINE__ + 1;
        AGGE_THROW_PURE_VIRTUAL_CALL;
        FAIL()<< "no exception thrown";
    } catch(const agge::core::pure_virtual_call& e) {
        EXPECT_EQ(std::string(file), e.get_file());
        EXPECT_EQ(line, e.get_line());
        EXPECT_EQ(std::string("Pure virtual method called."), e.get_message());
    } catch(...) {
        FAIL() << "exception not catched as agge::core::exception";
    }
}

TEST(exception, last_exception)
{
    int line = 0;
    const char *file = __FILE__;
    try {
        line = __LINE__ + 1;
        throw AGGE_EXCEPTION(agge::core::exception);
        FAIL()<< "no exception thrown";
    } catch(const agge::core::exception& e) {
        EXPECT_EQ(std::string(file), e.get_file());
        EXPECT_EQ(line, e.get_line());
        EXPECT_EQ(e.get_file(), exception::get_last_exception()->get_file());
        EXPECT_EQ(e.get_line(), exception::get_last_exception()->get_line());
    } catch(...) {
        FAIL() << "exception not catched as agge::core::exception";
    }
}

TEST(exception, not_implemented)
{
    int line = 0;
    const char *file = __FILE__;
    try {
        line = __LINE__ + 1;
        AGGE_THROW_NOT_IMPLEMENTED;
        FAIL()<< "no exception thrown";
    } catch(const agge::core::not_implemented& e) {
        EXPECT_EQ(std::string(file), e.get_file());
        EXPECT_EQ(line, e.get_line());
        EXPECT_EQ(std::string("Not implemented"),e.get_message());
#ifdef AGGE_COMPILER_MSVC
        EXPECT_TRUE(e.get_backtrace());
#endif
    } catch(...) {
        FAIL() << "exception not catched as agge::core::exception";
    }
}

TEST(exception, system_call_error)
{
    try {
        AGGE_THROW_SYSCALL_ERROR(foobar);
        FAIL()<< "No exception thrown";
    } catch(const agge::core::system_error& e) {
        EXPECT_TRUE(strstr(e.get_message().c_str(), "foobar") != 0);
#ifdef AGGE_COMPILER_MSVC
        EXPECT_TRUE(e.get_backtrace());
#endif
    } catch(...) {
        FAIL() << "Unexpected exception thrown, expected system error";
    }
}
#ifdef AGGE_COMPILER_MSVC
TEST(exception, add_backtrace)
{
    try {
        throw AGGE_EXCEPTION(agge::core::exception) << agge::core::exception::backtrace;
    } catch(const agge::core::exception& e) {
        agge::core::backtrace_ref b = e.get_backtrace();
        EXPECT_LT(0u, b->size());
        EXPECT_TRUE(b->contains("exception_add_backtrace_Test::TestBody"));
    }
}
#endif
