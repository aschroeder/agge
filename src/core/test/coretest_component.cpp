/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "agge/test/googletest.hpp"

#include "agge/core/component.hpp"

using agge::core::component;

class a_component: public agge::core::component
{
public:
    virtual ~a_component()
    {
    }

    virtual int foo() = 0;
};

AGGE_REGISTER_COMPONENT(a_component);

TEST( component, is_registered )
{
    EXPECT_TRUE(component::is_component_registered("a_component"));
}

class a_implementation: public a_component
{
public:
    virtual ~a_implementation()
    {
    }

    int foo()
    {
        return 42;
    }
};

AGGE_REGISTER_IMPLEMENTATION_ALIAS(a_implementation, a_component,
                                   adummy bdummy);

TEST( implementation, is_registered )
{
    EXPECT_TRUE(
            component::is_implementation_registered("a_component",
                                                    "a_implementation"));
    EXPECT_TRUE(
            component::is_implementation_registered("a_component", "adummy"));
    EXPECT_TRUE(
            component::is_implementation_registered("a_component", "bdummy"));
    EXPECT_TRUE(
            component::is_implementation_registered("a_component",
                                                    "   bdummy   "));
    EXPECT_TRUE(
            component::is_implementation_registered("a_component", "ADUMMY"));
}

TEST( component, get_implementations )
{
    std::vector<std::string> implementations;
    component::get_implementations("a_component", implementations);
    ASSERT_EQ(1, implementations.size());
    EXPECT_EQ("a_implementation", implementations[0]);
}

TEST( component, get_implementations_macro )
{
    std::vector<std::string> implementations;
    AGGE_GET_IMPLEMENTATIONS(a_component, implementations);
    ASSERT_EQ(1, implementations.size());
    EXPECT_EQ("a_implementation", implementations[0]);
}

TEST( implementation, create )
{
    std::shared_ptr<a_component> c = AGGE_CREATE_INSTANCE(a_component,
                                                            "a_implementation");
    ASSERT_FALSE(c == 0);
    EXPECT_EQ(42, c->foo());
}
