/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"
#include "gtest/gtest.h"

using agge::core::path;

TEST(path, construct)
{
    path p("c:/wurst/salat");
    EXPECT_EQ(std::string("c:/wurst/salat"), p.get_generic_string());
}

TEST(path, get_native_string)
{
#ifdef AGGE_OS_WINDOWS
    path p("c:/wurst/salat");
    EXPECT_EQ(std::string("c:\\wurst\\salat"), p.get_native_string());
#else
    path p("brot/wurst");
    EXPECT_EQ(std::string("brot/wurst"), p.get_native_string());
    path p2("/usr/brot/wurst");
    EXPECT_EQ(std::string("/usr/brot/wurst"), p2.get_native_string());

#endif
}

TEST(path, is_absolute)
{
#ifdef AGGE_OS_WINDOWS
    path p("c:/wurst");
    EXPECT_TRUE(p.is_absolute());
#else
    path p("/wurst");
    EXPECT_TRUE(p.is_absolute());
#endif
    path p2("brot");
    EXPECT_FALSE(p2.is_absolute());
}
