/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"

#include "log_severity.hpp"

using agge::core::log_severity;

template <typename T>
void EXPECT_SFMT(const char *match, const T& t)
{
    std::stringstream ss;
    ss << t;
    EXPECT_EQ(std::string(match), ss.str());
}

TEST(log_severity, compare)
{
    EXPECT_EQ(log_severity::LOG_SEVERITY_DEBUG(),
              log_severity::LOG_SEVERITY_DEBUG());
    EXPECT_NE(log_severity::LOG_SEVERITY_DEBUG(),
              log_severity::LOG_SEVERITY_INFO());
    EXPECT_LT(log_severity::LOG_SEVERITY_DEBUG(),
              log_severity::LOG_SEVERITY_INFO());
}

TEST(log_severity, shortcuts)
{
    EXPECT_EQ(log_severity::D(), log_severity::LOG_SEVERITY_DEBUG());
    EXPECT_EQ(log_severity::I(), log_severity::LOG_SEVERITY_INFO());
    EXPECT_EQ(log_severity::W(), log_severity::LOG_SEVERITY_WARNING());
    EXPECT_EQ(log_severity::E(), log_severity::LOG_SEVERITY_ERROR());
}

TEST(log_severity, get)
{
    EXPECT_EQ(log_severity::I(), log_severity::get("I"));
    EXPECT_EQ(log_severity::D(), log_severity::get("D"));
    EXPECT_EQ(log_severity::W(), log_severity::get("W"));
    EXPECT_EQ(log_severity::E(), log_severity::get("E"));
    EXPECT_EQ(log_severity::E(), log_severity::get("X"));
    EXPECT_EQ(log_severity::I(), log_severity::get("i"));
    EXPECT_EQ(log_severity::D(), log_severity::get("d"));
    EXPECT_EQ(log_severity::W(), log_severity::get("w"));
    EXPECT_EQ(log_severity::E(), log_severity::get("e"));
    EXPECT_EQ(log_severity::I(), log_severity::get("INFO"));
    EXPECT_EQ(log_severity::D(), log_severity::get("DEBUG"));
    EXPECT_EQ(log_severity::W(), log_severity::get("WARNING"));
    EXPECT_EQ(log_severity::E(), log_severity::get("ERROR"));
}

TEST(log_severity, stream)
{
    EXPECT_SFMT("INFO", log_severity::I());
    EXPECT_SFMT("DEBUG", log_severity::D());
    EXPECT_SFMT("WARNING", log_severity::W());
    EXPECT_SFMT("ERROR", log_severity::E());
}

TEST(log_severity, abbrev)
{
    EXPECT_EQ(std::string("I"), log_severity::I().get_abbreviation());
    EXPECT_EQ(std::string("D"), log_severity::D().get_abbreviation());
    EXPECT_EQ(std::string("W"), log_severity::W().get_abbreviation());
    EXPECT_EQ(std::string("E"), log_severity::E().get_abbreviation());
}
