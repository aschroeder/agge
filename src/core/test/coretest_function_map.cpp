/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"
#include "agge/core/function_map.hpp"


TEST(function_map, insert_return_key)
{
    agge::core::function_map<void()> m;
    unsigned int key = m.insert([]{});
    EXPECT_TRUE(key != 0);
}

TEST(function_map, erase_by_key)
{
    agge::core::function_map<void()> m;
    unsigned int key = m.insert([]{});
    m.erase(key);
    EXPECT_TRUE(m.empty());
}

TEST(function_map, lookup_and_call)
{
    bool called=false;
    agge::core::function_map<void()> m;
    unsigned int key = m.insert([&]{called=true;});
    EXPECT_TRUE(key != 0);
    m[key]();
    EXPECT_TRUE(called);
}
