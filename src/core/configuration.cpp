/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/configuration.hpp"
#include "agge/core/configuration_database.hpp"

#include <iostream>

namespace agge {
    namespace core {

        static
        configuration::config_values_map&
        configuration_overrides()
        {
            static configuration::config_values_map overrides;
            return overrides;
        }

        configuration::configuration()
        {
            configuration_database::get_instance()
                .read_configuration_values(m_values);
        }

        configuration::configuration(const std::string& prefix)
        :m_prefix(prefix)
        {
            configuration_database::get_instance()
                .read_configuration_values(prefix.c_str(), m_values);
        }

        configuration::configuration(std::string&& prefix)
        :m_prefix(prefix)
        {
            configuration_database::get_instance()
                .read_configuration_values(prefix.c_str(), m_values);
        }

        configuration::configuration(const configuration& c)
        :m_prefix(c.m_prefix),
         m_values(c.m_values)
        {}

        configuration::configuration(configuration&& c)
        :m_prefix(c.m_prefix),
         m_values(c.m_values)
        {}

        configuration&
        configuration::operator=(const configuration& c)
        {
            m_prefix = c.m_prefix;
            m_values = c.m_values;
            return *this;
        }


        void
        configuration::store()
        {
            if(m_prefix.empty()) {
                configuration_database::get_instance()
                    .store_configuration_values(m_values);
            } else {
                configuration_database::get_instance()
                    .store_configuration_values(m_prefix.c_str(), m_values);
            }
        }

        void
        configuration::clear()
        {
            configuration_database::get_instance()
                .clear_configuration(m_prefix.c_str());
            m_values.clear();
        }

        const std::string&
        configuration::get(const std::string& key) const
        {
            if(m_prefix.empty()) {
                auto override_i = configuration_overrides().find(key);
                if(override_i != configuration_overrides().end()) {
                    return override_i->second;
                }
            } else {
                auto override_i = configuration_overrides().find(m_prefix + "." + key);
                if(override_i != configuration_overrides().end()) {
                    return override_i->second;
                }
            }

            auto i = m_values.find(key);
            if(i==m_values.end()) {
                AGGE_THROW(configuration_key_not_found);
            } else {
                return i->second;
            }
        }

        bool
        configuration::contains_key(const char *key) const
        {
            return contains_key(std::string(key));
        }

        bool
        configuration::contains_key(const std::string& key) const
        {
            if(m_prefix.empty()) {
                auto override_i = configuration_overrides().find(key);
                if(override_i != configuration_overrides().end()) {
                    return true;
                }
            } else {
                auto override_i = configuration_overrides().find(m_prefix + "."
                                                   + key);
                if(override_i != configuration_overrides().end()) {
                    return true;
                }
            }
            auto i = m_values.find(key);
            return i!=m_values.end();
        }


        void
        configuration::set_override(const char *key, const char *value)
        {
            configuration_overrides()[key] = value;
        }

        void
        configuration::clear_override(const char *key)
        {
            auto i = configuration_overrides().find(key);
            if(i != configuration_overrides().end()) {
                configuration_overrides().erase(i);
            }
        }


        configuration_key_not_found::configuration_key_not_found()
        {}

        configuration_key_not_found::configuration_key_not_found(const configuration_key_not_found& e)
        :no_such_element(e)
        {}

        configuration_key_not_found::configuration_key_not_found(const char *file, int line)
        :no_such_element(file, line)
        {}

        configuration_key_not_found::~configuration_key_not_found()
        {}

    }
}
