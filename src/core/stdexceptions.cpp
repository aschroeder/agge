/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/stdexceptions.hpp"

namespace agge {
    namespace core {

#define IMPLEMENT_EXCEPTION(name)               \
        name::name()                            \
        {}                                      \
                                                \
        name::name(const name& e)               \
        :exception(e)                           \
        {}                                      \
                                                \
        name::name(const char *file, int line)  \
        :exception(file, line)                  \
        {}                                      \
                                                \
        name::~name()                           \
        {}                                      \
                                                \
        name&                                   \
        name::operator =(const name& e)         \
        {                                       \
            agge::core::exception::operator =(e); \
            return *this;                        \
        }


        IMPLEMENT_EXCEPTION(illegal_argument)
        IMPLEMENT_EXCEPTION(illegal_state)
        IMPLEMENT_EXCEPTION(runtime_exception)
        IMPLEMENT_EXCEPTION(no_such_element)
        IMPLEMENT_EXCEPTION(out_of_memory)
        IMPLEMENT_EXCEPTION(system_error)
        IMPLEMENT_EXCEPTION(null_pointer)
        IMPLEMENT_EXCEPTION(duplicate_element)
        IMPLEMENT_EXCEPTION(configuration_error)
        IMPLEMENT_EXCEPTION(pure_virtual_call)
        IMPLEMENT_EXCEPTION(unsupported_operation)
        IMPLEMENT_EXCEPTION(not_implemented)
        IMPLEMENT_EXCEPTION(index_out_of_bounds)
        IMPLEMENT_EXCEPTION(bad_cast)
    }
}
