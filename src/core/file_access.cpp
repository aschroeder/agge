/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/file_access.hpp"
#include "agge/core/path.hpp"

namespace agge {
    namespace core {

        file_not_found::file_not_found()
        {
        }

        file_not_found::file_not_found(const file_not_found& e)
                        : exception(e)
        {
        }

        file_not_found::file_not_found(const char *file, int line)
                        : exception(file, line)
        {
        }

        file_not_found::~file_not_found()
        {
        }

        file_not_found&
        file_not_found::operator =(const file_not_found& e)
        {
            agge::core::exception::operator =(e);
            return *this;
        }

        filesystem_error::filesystem_error()
        {
        }

        filesystem_error::filesystem_error(const filesystem_error& e)
                        : exception(e)
        {
        }

        filesystem_error::filesystem_error(const char *file, int line)
                        : exception(file, line)
        {
        }

        filesystem_error::~filesystem_error()
        {
        }

        filesystem_error&
        filesystem_error::operator =(const filesystem_error& e)
        {
            agge::core::exception::operator =(e);
            return *this;
        }

        file_access::file_access(const std::string& path)
                        : m_path(path)
        {
        }

        file_access::file_access()
        {
        }

        file_access::~file_access()
        {
        }

        const std::string&
        file_access::get_path() const
        {
            return m_path;
        }

        std::string file_access::get_file_name() const
        {
            path p(m_path);
            return p.get_file_name();
        }

        void file_access::set_path(const std::string& path)
        {
            m_path = path;
        }

        std::string file_access::get_extension() const
        {
            path p(m_path);
            return p.get_extension();
        }
    }
}
