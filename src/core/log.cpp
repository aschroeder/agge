/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/log.hpp"
#include "agge/core/init_on_demand.hpp"

#include "agge/core/string_functions.hpp"

#include "memory_log_handler.hpp"

namespace agge {
    namespace core {

        log::flush_tag log::flush;

        log::log()
        :m_global_level(0),
         m_config_status(CONFIG_STATUS_INITIAL),
         m_startup_handler(0),
         m_current_severity(0)
        {
            system::get_current_time(m_current_timestamp);
            m_startup_handler = new memory_log_handler();
        }

        log::~log()
        {
            delete m_startup_handler;
        }

        log&
        log::instance()
        {
            static init_on_demand<log> log_instance;
            return *log_instance;
        }

        bool
        log::is_enabled(const char *category, const log_severity& severity)
        {
            return is_enabled(std::string(category), severity);
        }

        bool
        log::is_enabled(const std::string& category, const log_severity& severity)
        {
            log& l = instance();

            switch (l.get_configuration_status()) {
            case CONFIG_STATUS_INITIAL:
                l.configure();
                if (l.get_configuration_status() != CONFIG_STATUS_CONFIGURED) {
                    return true;
                }
                break;
            case CONFIG_STATUS_CONFIGURING:
                return true;
            case CONFIG_STATUS_CONFIGURED:
                break;
            }

            if (l.is_globally_enabled(severity)) {
                return true;
            } else {
                return l.is_category_enabled(category, severity);
            }
        }

        bool
        log::is_category_enabled(const std::string& category,
                                 const log_severity& severity) const
        {
            auto i = m_category_levels.find(category);
            if (i != m_category_levels.end()) {
                if ((i->second & severity.get_level()) != 0) {
                    return true;
                }
            }

            return false;
        }

        log&
        log::log_entry(const char *category, const log_severity& severity)
        {
            log& l = instance();
            if (l.m_current_severity) {
                l.flush_entry();
            }
            std::string str;
            l.m_current_entry_stream.str(str);
            l.m_current_category = category;
            l.m_current_severity = const_cast<log_severity*>(&severity);
            system::get_current_time(l.m_current_timestamp);
            return l;
        }

        log::config_status
        log::get_configuration_status() const
        {
            return m_config_status;
        }

        bool
        log::is_globally_enabled(const log_severity& severity) const
        {
            if ((m_global_level & severity.get_level()) != 0) {
                return true;
            } else {
                return false;
            }
        }

        void
        log::configure_log_levels()
        {
            configuration config("log.level");
            std::vector<std::string> categories = config.get_keys();
            for(auto category : categories) {
                configure_log_category(category, config.get(category));
            }
            m_global_level = m_category_levels["ALL"];
        }

        void log::configure_log_category(const std::string& category,
                                         const std::string& levels)
        {
            std::vector<std::string> level_list;
            split(level_list, levels, ',');
            for(auto level : level_list) {
                enable_log_level(category, level);
            }
        }


        void
        log::enable_log_level(const std::string& category,
                              const std::string& level)
        {
            m_category_levels[category] |= log_severity::get(level).get_level();
            AGGE_DEBUG_LOG(LOG) << "Set log severity " << level << " for category "
                                << category << "." << log::flush;
        }

        void
        log::store_default_config(configuration& config)
        {
            config.set("handlers", "defaultfile");
            config.set("handler.defaultfile.class", "file");
            config.set("handler.defaultfile.formatter", "text");
            config.set("handler.defaultfile.file", "log.txt");
            config.set("level.ALL", "ALL");
            config.store();
        }

        void
        log::configure()
        {
            switch (m_config_status) {
            case CONFIG_STATUS_INITIAL:
                m_config_status = CONFIG_STATUS_CONFIGURING;
                break;
            case CONFIG_STATUS_CONFIGURED:
            case CONFIG_STATUS_CONFIGURING:
            default:
                // recursive calls while configuring
                return;
            }
            try {
                handler_collection configured_handlers;
                configuration log_config("log");
                if(!log_config.contains_key("handlers")) {
                    store_default_config(log_config);
                }
                std::string handlers_str = log_config.get("handlers");
                std::vector<std::string> handler_names;
                split(handler_names, handlers_str, ',');

                log_handler_ref handler;
                for(auto name : handler_names) {
                    std::string config_name("log.handler.");
                    config_name += name;
                    configuration handler_config(config_name);
                    std::string handler_class = handler_config.get("class");

                    handler = AGGE_CREATE_INSTANCE(log_handler,
                                                   handler_class);
                    if(!handler) {
                        m_config_status = CONFIG_STATUS_INITIAL;
                        return;
                    } else {
                        try {
                            handler->configure(handler_config);
                            configured_handlers.push_back(handler);
                        } catch(const illegal_state& e) {
                            AGGE_ERROR_LOG(LOG) << "Cannot configure_handler "
                                                << name  << ": "
                                                << e.get_message() << log::flush;
                            m_config_status = CONFIG_STATUS_INITIAL;
                            return;
                        }
                    }
                }

                configure_log_levels();
                m_handlers.swap(configured_handlers);
                process_memory_log_records();
                m_config_status = CONFIG_STATUS_CONFIGURED;
                return;
            } catch (const std::exception&) {
                process_memory_log_records();
                m_config_status = CONFIG_STATUS_CONFIGURED;
                return;
            }
        }

        void
        log::process_memory_log_records()
        {
            memory_log_handler *lh = m_startup_handler;
            m_startup_handler = 0;

            for(auto i = lh->begin(); i != lh->end();
                    ++i) {
                if (is_enabled(i->get_category(), i->get_severity())) {
                    publish(*i);
                }
            }

            delete lh;
        }

        void
        log::flush_entry()
        {
            log_record record(*m_current_severity, m_current_timestamp,
                              m_current_category, m_current_entry_stream.str());
            m_current_severity = 0;
            publish(record);
        }

        void
        log::publish(const log_record& r)
        {
            if (m_startup_handler) {
                m_startup_handler->log(r);
            } else {
                for (auto i = m_handlers.begin();
                        i != m_handlers.end(); ++i) {
                    (*i)->log(r);
                }
            }
        }

    }
}
