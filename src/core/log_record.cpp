/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/log_record.hpp"
#include "text_log_formatter.hpp"

namespace agge {
        namespace core {

        log_record::log_record()
        :m_severity(&log_severity::E())
        {
        }

        log_record::log_record(const log_severity& severity,
                               const system::timestamp& timestamp,
                               const std::string& component, const std::string& message)
        :m_severity(&severity),
         m_timestamp(timestamp),
         m_category(component),
         m_message(message)
        {}

        log_record::log_record(const log_record& r)
        :m_severity(r.m_severity),
         m_timestamp(r.m_timestamp),
         m_category(r.m_category),
         m_message(r.m_message)
        {}

        log_record&
        log_record::operator =(const log_record& r)
        {
            m_severity = r.m_severity;
            m_timestamp = r.m_timestamp;
            m_category = r.m_category;
            m_message = r.m_message;
            return *this;
        }

        const log_severity&
        log_record::get_severity() const
        {
            if (m_severity) {
                return *m_severity;
            } else {
                return log_severity::E();
            }
        }

        AGGE_CORE_EXPORT std::ostream&
        operator <<(std::ostream& os, const log_record& r)
        {
            text_log_formatter::format_record(os, r);
            return os;
        }

    }
}
