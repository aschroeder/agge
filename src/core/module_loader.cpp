/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/module_loader.hpp"
#include "agge/core/log.hpp"
#include "agge/core/init_on_demand.hpp"
#include "agge/core/exception.hpp"
#include "agge/core/noncopyable.hpp"
#include "agge/core/path.hpp"
#include "agge/core/file.hpp"

#include <cstring>

#if defined(AGGE_OS_LINUX) || defined(AGGE_OS_MACOSX)
#  include <dlfcn.h>
#endif

namespace agge {
    namespace core {

        namespace internal {

            class module_handle: public noncopyable
            {
            public:
                module_handle(const char *filename);
                ~module_handle();
            private:
            #if defined(AGGE_OS_WINDOWS)
                HINSTANCE m_handle;
            #elif defined(AGGE_OS_LINUX) || defined(AGGE_OS_MACOSX)
                void *m_handle;
            #else
            #  error Unsupported platform!
            #endif
            };

            module_handle::module_handle(const char *filename)
            :m_handle(0)
            {
            #ifdef AGGE_OS_WINDOWS
                m_handle = LoadLibrary(filename);
                if(m_handle == 0) {
                    AGGE_THROW_SYSTEMERROR;
                }
            #elif defined(AGGE_OS_LINUX) || defined(AGGE_OS_MACOSX)
                m_handle = dlopen(filename, RTLD_NOW);
                if (m_handle == 0) {
                    throw AGGE_EXCEPTION(system_error) << "Cannot load library " << filename
                                                       << ":" << dlerror();
                }
            #else
            #  error Unsupported system!
            #endif
            }

            module_handle::~module_handle()
            {}
        }

        enum module_load_status
        {
            MODULE_LOAD_INITIAL = 0,
            MODULE_LOAD_LOADING = 1,
            MODULE_LOAD_DONE = 2
        };

        static module_load_status load_status = MODULE_LOAD_INITIAL;
        typedef std::map<std::string, std::shared_ptr<internal::module_handle> > module_map;
        static init_on_demand<module_map> loaded_modules;

        bool
        module_loader::modules_loaded()
        {
            return load_status == MODULE_LOAD_DONE;
        }

        static bool
        name_starts_with_prefix(const std::string& filename,
                                std::set<std::string>& prefixes)
        {
            for (auto prefix : prefixes) {
                if (prefix.length() <= filename.length()) {
                    if (memcmp(filename.c_str(), prefix.c_str(), prefix.length()) == 0) {
                        return true;
                    }
                }
            }
            return false;
        }

        static void
        load_module(const path& filename)
        {
            std::string modulename = filename.get_native_string();
            AGGE_DEBUG_LOG(MODULE) << "Loading file " << modulename << log::flush;
            if (loaded_modules->find(modulename) == loaded_modules->end()) {
                auto hnd = std::make_shared<internal::module_handle>(modulename.c_str());
                loaded_modules->insert(std::make_pair(modulename, hnd));
            }
        }

        static bool
        name_has_module_extension(const file& path)
        {
#ifdef AGGE_OS_WINDOWS
            if(path.get_extension() == std::string(".dll")) {
                return true;
            }
#endif
#ifdef AGGE_OS_LINUX
            if(path.get_extension() == std::string(".so")) {
                return true;
            }
#endif
#ifdef AGGE_OS_MACOSX
            if (path.get_extension() == std::string(".dylib") || path.get_extension()
                    == std::string(".so")) {
                return true;
            }
#endif
            return false;
        }

        static void load_modules_from_path(const path& path,
                                           std::set<std::string>& prefixes)
        {
            file path_file(path);
            if(!path_file.is_directory()) {
                return;
            }

            std::vector<file> files;
            path_file.list(files);
            for(const file& f : files) {
                std::string name = f.get_file_name();
                if(name_starts_with_prefix(name, prefixes)) {
                    if(name_has_module_extension(f)) {
                       load_module(f.get_path());
                    }
                } else if(f.is_directory()) {
                   load_modules_from_path(agge::core::path(f.get_path()),
                                          prefixes);
                }
            }

            return;
        }

        void
        module_loader::load_modules()
        {
            if (load_status != MODULE_LOAD_INITIAL) {
                return;
            }

            load_status = MODULE_LOAD_LOADING;
            AGGE_INFO_LOG(MODULE) << "Loading modules." << log::flush;

            try {
                std::vector<path> loadpaths;

                path p(".");
                loadpaths.push_back(p);
                p /= "modules";
                loadpaths.push_back(p);

                for(auto cp : loadpaths) {
                    AGGE_INFO_LOG(MODULE) << "Looking in path: " << cp << log::flush;
                }

                std::set<std::string> prefixes;
                prefixes.insert("agge_module");
                prefixes.insert("libagge_module");

                for(auto loadpath : loadpaths) {
                    load_modules_from_path(loadpath, prefixes);
                }
                load_status = MODULE_LOAD_DONE;

            } catch (const exception& e) {
                AGGE_ERROR_LOG(MODULE) << "Error during module load: "
                                       << e.get_message() << log::flush;
                load_status = MODULE_LOAD_INITIAL;
            }
            return;
        }

    }
}
