/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/core/backtrace_element.hpp"

#include <stdio.h>

namespace agge {
    namespace core {

        backtrace_element::backtrace_element()
                        : m_address(nullptr), m_line(0)
        {
        }

        backtrace_element::backtrace_element(void *address)
                        : m_address(address), m_line(0)
        {
        }

        backtrace_element::backtrace_element(void *address,
                                             const std::string& function)
                        : m_address(address), m_function(function), m_line(0)
        {
        }

        backtrace_element::backtrace_element(void *address,
                                             const std::string& function,
                                             const std::string& file,
                                             unsigned int line,
                                             const std::string& module)
                        : m_address(address),
                          m_function(function),
                          m_file(file),
                          m_module(module),
                          m_line(line)
        {
        }

        backtrace_element::backtrace_element(const backtrace_element& e)
                        : m_address(e.m_address),
                          m_function(e.m_function),
                          m_file(e.m_file),
                          m_module(e.m_module),
                          m_line(e.m_line)
        {
        }

        backtrace_element::backtrace_element(backtrace_element&& e)
                        : m_address(e.m_address),
                          m_function(e.m_function),
                          m_file(e.m_file),
                          m_module(e.m_module),
                          m_line(e.m_line)
        {
        }

        backtrace_element::~backtrace_element()
        {
        }

        backtrace_element&
        backtrace_element::operator =(const backtrace_element& e)
        {
            m_address = e.m_address;
            m_function = e.m_function;
            m_file = e.m_file;
            m_module = e.m_module;
            m_line = e.m_line;
            return *this;
        }

        void*
        backtrace_element::get_address() const
        {
            return m_address;
        }

        const std::string&
        backtrace_element::get_function() const
        {
            return m_function;
        }

        const std::string&
        backtrace_element::get_file() const
        {
            return m_file;
        }

        unsigned int backtrace_element::get_line() const
        {
            return m_line;
        }

        const std::string&
        backtrace_element::get_module() const
        {
            return m_module;
        }

        std::ostream& operator <<(std::ostream& os, const backtrace_element& e)
        {
            char address[64];
            sprintf(address, "%p", e.get_address());
            os << address;
            if (e.get_function().size()) {
                os << " " << e.get_function();
            }
            if (e.get_file().size()) {
                os << " " << e.get_file() << ":" << e.get_line();
            }
            if (e.get_module().size()) {
                os << " " << e.get_module();
            }
            return os;
        }
    }
}
