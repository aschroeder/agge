/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/test/googletest.hpp"
#include "agge/event/event.hpp"

class my_event : public agge::event::event
{
public:
    my_event(unsigned int type, unsigned int category, size_t s =
                    sizeof(my_event))
                    : agge::event::event(type, category, s)
    {
    }
};

TEST(event, type)
{
    my_event e(1, 2);
    EXPECT_EQ(1, e.get_type());
}

TEST(event, category)
{
    my_event e(1, 2);
    EXPECT_EQ(2, e.get_category());
}

TEST(event, size)
{
    my_event e(1, 2);
    EXPECT_EQ(sizeof(my_event), e.get_size());
}

TEST(event, aligned_size)
{
    for (size_t i = 0; i < 32; ++i) {
        my_event e(1, 2, i);
        EXPECT_EQ(i, e.get_size());
        EXPECT_TRUE(e.get_aligned_size() % 8 == 0);
        EXPECT_TRUE(e.get_aligned_size() >= i);
    }
}
