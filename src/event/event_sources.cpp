/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/event/event_sources.hpp"
#include "agge/core/init_on_demand.hpp"

namespace agge {
    namespace event {
        void event_sources::fetch_events()
        {
            get().do_fetch_events();
        }

        void event_sources::add_source(const event_source&)
        {

        }

        void event_sources::remove_source(const event_source&)
        {

        }

        event_sources& event_sources::get()
        {
            static agge::core::init_on_demand<event_sources> sources;
            return *(sources.ptr());
        }

        void event_sources::do_fetch_events()
        {
            for (const auto f : m_sources) {
                f();
            }
        }
    }
}
