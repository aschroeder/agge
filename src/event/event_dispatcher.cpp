/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/event/event_dispatcher.hpp"
#include "agge/event/event_queue.hpp"

namespace agge {
    namespace event {

        event_dispatcher::event_dispatcher()
        {
            event_queue::get().add(this);
        }

        event_dispatcher::~event_dispatcher()
        {
            event_queue::get().remove(this);
        }

        bool event_dispatcher::is_triggered(const event&) const
        {
            return false;
        }

        bool event_dispatcher::dispatch(const event&) const
        {
            return false;
        }
    }
}
