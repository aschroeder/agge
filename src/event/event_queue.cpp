/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/event/event_queue.hpp"
#include "agge/event/event_dispatcher.hpp"
#include "agge/core/init_on_demand.hpp"

#include "agge/event/event.hpp"
#include "agge/core/exception.hpp"
#include "agge/core/log.hpp"

#include <algorithm>
#include <utility>
#include <cstring>

using agge::core::log;

namespace agge {
    namespace event {

        event_queue&
        event_queue::get()
        {
            static agge::core::init_on_demand<event_queue> s_eventqueue;
            return *(s_eventqueue.ptr());
        }

        event_queue::event_queue()
        {
        }

        event_queue::~event_queue()
        {
        }

        bool event_queue::is_live() const
        {
            return !empty() || !m_sources.empty();
        }

        void event_queue::loop(bool& terminate)
        {
            AGGE_INFO_LOG(EVENT) << "Start event queue loop." << log::flush;
            while (!terminate && is_live()) {
                collect();
                dispatch();
            }
            AGGE_INFO_LOG(EVENT) << "End event queue loop." << log::flush;
        }

        bool event_queue::empty() const
        {
            return m_offsets.empty();
        }

        void event_queue::add(const event& e)
        {
            size_t ofs = m_data.size();
            m_offsets.push_back(ofs);
            m_data.resize(ofs + e.get_aligned_size());
            memcpy(&m_data[ofs], &e, e.get_size());
            return;
        }

        void event_queue::add(event_dispatcher *dispatcher)
        {
            if (!is_dispatcher_registered(dispatcher)) {
                m_dispatchers.push_back(dispatcher);
                AGGE_INFO_LOG(EVENT) << "Add event dispatcher." << log::flush;
            }
        }

        void event_queue::remove(event_dispatcher *dispatcher)
        {
            auto i = std::find(m_dispatchers.begin(), m_dispatchers.end(),
                               dispatcher);
            if (i != m_dispatchers.end()) {
                m_dispatchers.erase(i);
                AGGE_INFO_LOG(EVENT) << "Remove event dispatcher."
                                       << log::flush;
            }
        }

        bool event_queue::is_dispatcher_registered(
                        event_dispatcher *dispatcher) const
        {
            return std::find(m_dispatchers.begin(), m_dispatchers.end(),
                             dispatcher)
                   != m_dispatchers.end();
        }

        void event_queue::collect()
        {
            clear();
            insert_iterator inserter(this);
            for (auto source : m_sources) {
                source(inserter);
            }

            if (!m_sources_to_remove.empty()) {
                for (auto s : m_sources_to_remove) {
                    for (auto i = m_sources.begin(); i != m_sources.end();
                                    ++i) {
                        if (s.target_type() == i->target_type()) {
                            if (s.target<void(insert_iterator&)>() == i
                                    ->target<void(insert_iterator&)>()) {
                                m_sources.erase(i);
                                break;
                            }
                        }
                    }
                }
                m_sources_to_remove.clear();
            }


            return;
        }

        void event_queue::add_source(const event_queue::event_source& s)
        {
            m_sources.push_back(s);
        }

        void event_queue::remove_source(const event_queue::event_source& s)
        {
            m_sources_to_remove.push_back(s);
        }

        void event_queue::dispatch()
        {
            for (auto offset : m_offsets) {
                event *current_event = reinterpret_cast<event*>(&m_data[offset]);
                for (auto dispatcher : m_dispatchers) {
                    if (dispatcher->is_triggered(*current_event)) {
                        if (dispatcher->dispatch(*current_event)) {
                            break;
                        }
                    }
                }
            }
        }

        void event_queue::clear()
        {
            m_data.clear();
            m_offsets.clear();
        }

        event_queue::insert_iterator::insert_iterator(event_queue *queue)
                        : m_queue(queue)
        {
            if (!m_queue) {
                AGGE_THROW(agge::core::null_pointer);
            }
        }

        event_queue::insert_iterator::~insert_iterator()
        {
        }

        event_queue::insert_iterator&
        event_queue::insert_iterator::operator =(const event& e)
        {
            m_queue->add(e);
            return *this;
        }

        event_queue::insert_iterator&
        event_queue::insert_iterator::operator *()
        {
            return *this;
        }

        event_queue::insert_iterator&
        event_queue::insert_iterator::operator ++()
        {
            return *this;
        }

        event_queue::insert_iterator&
        event_queue::insert_iterator::operator ++(int)
        {
            return *this;
        }

    }
}
