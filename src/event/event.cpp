/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/event/event.hpp"

namespace agge {
    namespace event {

        event::event()
                        : m_type(0), m_category(0), m_size(sizeof(event))
        {
        }

        event::event(unsigned int type, unsigned int category, size_t size)
                        : m_type(type), m_category(category), m_size(size)
        {
        }

        event::event(const event& e)
                        : m_type(e.m_type),
                          m_category(e.m_category),
                          m_size(e.m_size)
        {
        }

        event::event(event&& e)
                        : m_type(e.m_type),
                          m_category(e.m_category),
                          m_size(e.m_size)
        {
        }

        event&
        event::operator =(const event& e)
        {
            m_type = e.m_type;
            m_category = e.m_category;
            m_size = e.m_size;
            return *this;
        }

        event&
        event::operator =(event&& e)
        {
            m_type = e.m_type;
            m_category = e.m_category;
            m_size = e.m_size;
            return *this;
        }

        unsigned int event::get_category() const
        {
            return m_category;
        }

        unsigned int event::get_type() const
        {
            return m_type;
        }

        size_t event::get_size() const
        {
            return m_size;
        }

        size_t event::get_aligned_size() const
        {
            return (((m_size + 7) >> 3) << 3);
        }
    }
}
