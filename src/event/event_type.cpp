/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/event/event_type.hpp"
#include "agge/core/stdexceptions.hpp"

#include <map>

namespace agge {
    namespace event {

        typedef std::map<unsigned int, event_type *> event_type_map;

        static event_type_map& get_event_type_map()
        {
            static event_type_map m;
            return m;
        }

        event_type::event_type(const char *tag, unsigned int type, std::size_t size)
                        : m_tag(tag), m_type(type), m_size(size)
        {
            register_event_type(this);
        }

        event_type::~event_type()
        {
            unregister_event_type(this);
        }

        unsigned int event_type::get_type() const
        {
            return m_type;
        }

        const char * event_type::get_tag() const
        {
            return m_tag;
        }

        std::size_t event_type::get_size() const
        {
            return m_size;
        }

        void event_type::register_event_type(event_type *et)
        {
            if (!et) {
                AGGE_THROW(agge::core::null_pointer);
            }

            auto insert_result = get_event_type_map().insert(
                            std::make_pair(et->get_type(), et));
            if (!insert_result.second) {
                throw AGGE_EXCEPTION(agge::core::illegal_state)<< "Event type " << et->get_type() << " ("
                << et->get_tag() << ") cannot be registered as there is already "
                << get_event_type_map()[et->get_type()]->get_tag() << " registered";
            }
        }

        void event_type::unregister_event_type(event_type *et)
        {
            get_event_type_map().erase(et->get_type());
        }
    }
}
