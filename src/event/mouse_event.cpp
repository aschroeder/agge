/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "agge/event/mouse_event.hpp"
#include "agge/event/event_type.hpp"

AGGE_DEFINE_EVENT_TYPE(MOUSE, 1, agge::event::mouse_event);
AGGE_USE_EVENT_TYPE_LOCAL(MOUSE);

namespace agge {
    namespace event {
        mouse_event::mouse_event(mouse_event::category c, unsigned int button,
                                 unsigned int x, unsigned int y)
                        : event(EVENT_TYPE_MOUSE.get_type(), c,
                                EVENT_TYPE_MOUSE.get_size()),
                          m_button(button),
                          m_x(x),
                          m_y(y)
        {
        }

        mouse_event::mouse_event(const mouse_event& e)
                        : event(e), m_button(e.m_button), m_x(e.m_x), m_y(e.m_y)
        {
        }

        mouse_event::mouse_event(mouse_event&& e)
                        : event(e), m_button(e.m_button), m_x(e.m_x), m_y(e.m_y)
        {
        }

        mouse_event&
        mouse_event::operator =(const mouse_event& e)
        {
            event::operator =(e);
            m_button = e.m_button;
            m_x = e.m_x;
            m_y = e.m_y;
            return *this;
        }

        mouse_event&
        mouse_event::operator =(mouse_event&& e)
        {
            event::operator =(e);
            m_button = e.m_button;
            m_x = e.m_x;
            m_y = e.m_y;
            return *this;
        }

    }
}
