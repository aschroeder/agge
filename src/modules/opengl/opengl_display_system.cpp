/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "opengl_display_system.hpp"
#include "opengl_common.hpp"
#include "opengl_error.hpp"
#include "opengl_monitor.hpp"
#include "opengl_offscreen_render_device.hpp"
#include "opengl_window.hpp"

using agge::core::log;
using agge::graphics::render_device_ref;

namespace agge {
    namespace opengl {

        display_system::display_system()
        {
            init_glfw();
            init_primary_monitor();
            init_system_info();
        }

        display_system::~display_system()
        {
            finalize_glfw();
        }

        static void glfw_error_callback(int errorcode, const char *message)
        {
            throw AGGE_EXCEPTION(opengl_error) << "GLFW error " << errorcode
                                               << ": " << message;
        }

        void display_system::init_glfw()
        {
            int rc = glfwInit();
            if (rc == GL_TRUE) {
                AGGE_INFO_LOG(OPENGL) << "GLFW library initialized."
                                        << agge::core::log::flush;
            } else {
                AGGE_ERROR_LOG(OPENGL) << "GLFW initialization failed."
                                        << log::flush;
                throw AGGE_EXCEPTION(opengl_error)
                                << "GLFW initialization failed.";
            }
            glfwSetErrorCallback(&glfw_error_callback);
        }

        void display_system::init_system_info()
        {
            render_device_ref r = std::make_shared<offscreen_render_device>(
                            agge::graphics::dimension(0, 0, 640, 480));
            r->make_current();

            std::string vendor;
            std::string product;
            std::string version;

            vendor = (const char *) glGetString(GL_VENDOR);
            product = (const char *) glGetString(GL_RENDERER);
            version = (const char *) glGetString(GL_VERSION);

            m_system_info = agge::graphics::display_system_info("opengl",
                                                                version, vendor,
                                                                product);

            AGGE_INFO_LOG(OPENGL) << "GL_VENDOR      :" << vendor
                                    << log::flush;
            AGGE_INFO_LOG(OPENGL) << "GL_RENDERER    :" << product
                                    << log::flush;
            AGGE_INFO_LOG(OPENGL) << "GL_VERSION     :" << version
                                    << log::flush;
        }

        void display_system::init_primary_monitor()
        {
            GLFWmonitor *m = glfwGetPrimaryMonitor();
            if (m) {
                m_primary_monitor = std::make_shared<opengl::monitor>(m, true);
            }
            return;
        }

        void display_system::finalize_glfw()
        {
            AGGE_INFO_LOG(OPENGL) << "Shutting down GLFW" << log::flush;
            try {
                m_primary_monitor.reset();
                glfwTerminate();
            } catch (const std::exception& e) {
                AGGE_ERROR_LOG(OPENG) << "Exception when terminating glfw: "
                                       << e.what() << log::flush;
            }

            return;
        }

        const agge::graphics::display_system_info&
        display_system::get_system_info() const
        {
            return m_system_info;
        }

        agge::graphics::monitor_ref display_system::get_primary_monitor() const
        {
            return m_primary_monitor;
        }

        agge::graphics::window_ref display_system::create_window(
                        const agge::graphics::dimension& d,
                        const std::string& title, bool fullscreen)
        {
            agge::graphics::window_ref w = std::make_shared<window>(d, title,
                                                                    fullscreen);
            return w;
        }

        AGGE_REGISTER_IMPLEMENTATION_ALIAS(display_system,
                                           agge::graphics::display_system,
                                           opengl gl);

    }
}
