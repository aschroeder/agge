/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "opengl_render_device.hpp"
#include "opengl_error.hpp"
#include "agge/core/log.hpp"

using agge::core::log;

namespace agge {
    namespace opengl {

        render_device::render_device(const agge::graphics::dimension& d)
                        : agge::graphics::render_device(d), m_window(nullptr)
        {
        }

        render_device::~render_device()
        {
            if (m_window) {
                try {
                    if (is_current()) {
                        glfwMakeContextCurrent(NULL);
                    }
                    glfwDestroyWindow(m_window);
                } catch (...) {
                    AGGE_WARNING_LOG(OPENGL) << "Exception destroying window."
                                             << log::flush;
                }
            }
        }

        void render_device::set_window(GLFWwindow *window)
        {
            if (!m_window) {
                m_window = window;
                init_gl3w();
            } else {
                m_window = window;
            }
        }

        void render_device::init_gl3w()
        {
            if (!s_gl3w_initialized) {
                make_current();
                int rc = gl3wInit();
                if (rc != 0) {
                    throw AGGE_EXCEPTION(opengl_error)
                                    << "Cannot initialize gl3w helper library";
                }
                s_gl3w_initialized = true;
            }
        }

        void render_device::on_make_current()
        {
            glfwMakeContextCurrent(m_window);
        }

        void render_device::on_flush()
        {
            glfwSwapBuffers(m_window);
        }

        void render_device::on_clear(const agge::graphics::rgba_color& c)
        {
            glClearColor(c.get_red(), c.get_green(), c.get_blue(),
                         c.get_alpha());
            CHECK_OPENGL_ERROR(glClearColor);
            glClear(GL_COLOR_BUFFER_BIT);
            CHECK_OPENGL_ERROR(glClear(GL_COLOR_BUFFER_BIT));
        }

        bool render_device::s_gl3w_initialized;
    }
}
