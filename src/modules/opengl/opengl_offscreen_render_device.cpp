/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "opengl_offscreen_render_device.hpp"

namespace agge {
    namespace opengl {

        offscreen_render_device::offscreen_render_device(
                        const agge::graphics::dimension& d)
                        : render_device(d)
        {
            init_window();
        }

        offscreen_render_device::~offscreen_render_device()
        {
        }

        void offscreen_render_device::init_window()
        {
            const agge::graphics::dimension& d = get_dimension();
            glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
            glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
            // throws error through glfw callback
            GLFWwindow *win = glfwCreateWindow(d.get_width(), d.get_height(),
                                               "", NULL,
                                               NULL);
            set_window(win);
        }

    }
}
