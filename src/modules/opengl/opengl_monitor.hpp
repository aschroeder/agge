/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_OPENGL_OPENGL_MONITOR_HPP
#define AGGE_OPENGL_OPENGL_MONITOR_HPP

#include "agge/graphics/monitor.hpp"
#include "opengl_common.hpp"

namespace agge {
    namespace opengl {
        
        class monitor : public agge::graphics::monitor
        {
        public:
            monitor(GLFWmonitor *monitor, bool primary);
            ~monitor();

            bool is_primary() const;
            const std::string& get_name() const;
            const agge::graphics::dimension& get_dimension() const;
            agge::graphics::monitor::video_mode_list get_video_modes() const;
        private:
            GLFWmonitor *m_monitor;
            bool m_is_primary;
            agge::graphics::dimension m_dimension;
            std::string m_name;
        };
        
    }
}


#endif
