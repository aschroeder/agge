/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_OPENGL_OPENGL_WINDOW_HPP
#define AGGE_OPENGL_OPENGL_WINDOW_HPP

#include "agge/graphics/window.hpp"
#include "agge/event/event_queue.hpp"
#include "opengl_render_device.hpp"
#include "opengl_common.hpp"

namespace agge {
    namespace opengl {

        class window : public agge::graphics::window
        {
        public:
            window(const agge::graphics::dimension& d, const std::string& title,
                   bool fullscreen);
            ~window();

            agge::graphics::render_device& get_render_device();
            void on_show();
            void on_hide();
        private:
            void create();
            void install_event_source();
            void uninstall_event_source();
            static void close_function(GLFWwindow *window);
            static void mouse_button_function(GLFWwindow*,int,int,int);
            static void key_function(GLFWwindow*,int,int,int,int);

            agge::event::event_queue::event_source m_event_source;
            agge::event::event_queue::insert_iterator *m_current_iterator;
            render_device m_device;
        };
    }
}

#endif
