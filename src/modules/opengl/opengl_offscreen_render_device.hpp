/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_OPENGL_OFFSCREEN_RENDER_DEVICE_HPP
#define AGGE_OPENGL_OFFSCREEN_RENDER_DEVICE_HPP

#include "opengl_render_device.hpp"

namespace agge {
    namespace opengl {
        class offscreen_render_device : public render_device
        {
        public:
            offscreen_render_device(const agge::graphics::dimension& d);
            virtual ~offscreen_render_device();
        private:
            void init_window();
        };

    }
}

#endif
