/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef AGGE_OPENGL_ERROR_HPP
#define AGGE_OPENGL_ERROR_HPP

#include "opengl_common.hpp"
#include "agge/core/exception.hpp"

namespace agge {
    namespace opengl {

        class opengl_error : public agge::core::exception
        {
        public:
            opengl_error();
            opengl_error(const opengl_error& e);
            opengl_error(const char *file, int line);
            virtual ~opengl_error();

            opengl_error& operator=(const opengl_error& e);

            template<typename T>
            opengl_error& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }

            static void raise(const char *file, int line, const char *function,
                              GLenum glerror);

            static void check_error(const char *file, int line,
                                    const char *function);
            static void log_error(const char *file, int line,
                                  const char *function);
            static void clear_error();
        };

#define THROW_OPENGL_ERROR(function, error) \
    ::agge::opengl::opengl_error::raise(__FILE__ ,__LINE__ ,#function, error)

#define CHECK_OPENGL_ERROR(function) \
    ::agge::opengl::opengl_error::check_error(__FILE__, __LINE__, #function)

#define CLEAR_OPENGL_ERROR agge::opengl::opengl_error::clear_error()

#define LOG_OPENGL_ERROR(function)    \
    ::agge::opengl::opengl_error::log_error(__FILE__, __LINE__, #function)

    }
}

#endif
