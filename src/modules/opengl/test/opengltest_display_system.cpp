/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "opengltest_test.hpp"
#include "agge/graphics/monitor.hpp"
#include "agge/graphics/display_system.hpp"

namespace agge {
    namespace opengl {
        class display_system_test : public agge::opengl::opengltest
        {
        };

        TEST_F(display_system_test, primary_monitor_present)
        {
            agge::graphics::monitor_ref m =
                            display_system->get_primary_monitor();
            EXPECT_TRUE(m.get() != nullptr);
        }

        TEST_F(display_system_test, primary_monitor_is_primary)
        {
            agge::graphics::monitor_ref m =
                            display_system->get_primary_monitor();
            EXPECT_TRUE(m->is_primary());
        }

        TEST_F(display_system_test, primary_monitor_dimension)
        {
            agge::graphics::monitor_ref m =
                            display_system->get_primary_monitor();
            auto d = m->get_dimension();
            EXPECT_NE(0, d.get_width());
            EXPECT_NE(0, d.get_height());
        }

        TEST_F(display_system_test, primary_monitor_has_name)
        {
            agge::graphics::monitor_ref m =
                            display_system->get_primary_monitor();
            std::string n = m->get_name();
            EXPECT_TRUE(n.length() != 0);
        }

        TEST_F(display_system_test, primary_monitor_has_video_modes)
        {
            agge::graphics::monitor_ref m =
                            display_system->get_primary_monitor();
            agge::graphics::monitor::video_mode_list modes =
                            m->get_video_modes();
            EXPECT_TRUE(!modes.empty());
        }

        TEST_F(display_system_test, display_system_info_is_filled)
        {
            agge::graphics::display_system_info info = display_system->get_system_info();
            EXPECT_STREQ("opengl", info.get_api().c_str());
            EXPECT_TRUE(info.get_gpu_product().length() > 0);
            EXPECT_TRUE(info.get_gpu_vendor().length() > 0);
            EXPECT_TRUE(info.get_version().length() > 0);
        }

    }
}

