/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "opengl_window.hpp"
#include "agge/event/mouse_event.hpp"

namespace agge {
    namespace opengl {
        window::window(const agge::graphics::dimension& d,
                       const std::string& title, bool fullscreen)
                        : agge::graphics::window(d, title, fullscreen),
                          m_device(d)
        {
            create();
        }

        window::~window()
        {
        }

        agge::graphics::render_device& window::get_render_device()
        {
            return m_device;
        }

        void window::close_function(GLFWwindow *w)
        {
            window *win = (agge::opengl::window*) glfwGetWindowUserPointer(w);
            if (!win->on_closing()) {
                glfwSetWindowShouldClose(w, GL_FALSE);
            }
            win->on_close();
        }

        void window::mouse_button_function(GLFWwindow *w, int button,
                                           int action, int)
        {
            window *win = (agge::opengl::window*) glfwGetWindowUserPointer(w);
            if (win->m_current_iterator) {
                agge::event::mouse_event e(
                                action == GLFW_PRESS ?
                                                agge::event::mouse_event::MOUSE_PRESS :
                                                agge::event::mouse_event::MOUSE_RELEASE,
                                (unsigned) button, // glfw uses left-right-middle numbering
                                0, 0);
                *(*win->m_current_iterator) = e;
                (*win->m_current_iterator)++;
            }
        }

        void window::key_function(GLFWwindow*, int, int, int, int)
        {
            return;
        }

        void window::create()
        {
            const agge::graphics::dimension& d = get_dimension();
            std::string title = get_title();

            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
            glfwWindowHint(GLFW_OPENGL_PROFILE,
            GLFW_OPENGL_CORE_PROFILE);
            GLFWwindow *win = glfwCreateWindow(d.get_width(), d.get_height(),
                                               title.c_str(),
                                               NULL,
                                               NULL);
            glfwSetWindowUserPointer(win, this);
            glfwSetWindowCloseCallback(win, close_function);
            glfwSetMouseButtonCallback(win, mouse_button_function);
            glfwSetKeyCallback(win, key_function);
            m_device.set_window(win);
        }

        void window::on_show()
        {
            glfwShowWindow(m_device.get_window());
            install_event_source();
        }

        void window::on_hide()
        {
            uninstall_event_source();
            glfwHideWindow(m_device.get_window());
        }

        void window::install_event_source()
        {
            if (!m_event_source) {
                m_event_source =
                                [&](agge::event::event_queue::insert_iterator& i) {
                                    m_current_iterator = &i;
                                    glfwPollEvents();
                                };
                agge::event::event_queue::get().add_source(m_event_source);
            }
            return;
        }

        void window::uninstall_event_source()
        {
            if (m_event_source) {
                agge::event::event_queue::get().remove_source(m_event_source);
                m_event_source = agge::event::event_queue::event_source();
            }
            return;
        }
    }
}
