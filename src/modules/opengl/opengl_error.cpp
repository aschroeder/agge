/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "opengl_error.hpp"
#include "agge/core/log.hpp"

namespace agge {
    namespace opengl {

        static const char *
        gl_error_message(GLenum error)
        {
            switch (error) {
            case GL_NO_ERROR:
                return "No error";
            case GL_INVALID_ENUM:
                return "Invalid enum value";
            case GL_INVALID_VALUE:
                return "Invalid value";
            case GL_INVALID_OPERATION:
                return "Invalid operation";
            case GL_OUT_OF_MEMORY:
                return "Out of memory";
            default:
                return "Unknown error";
            }
        }

        opengl_error::opengl_error()
        {
        }

        opengl_error::opengl_error(const opengl_error& e)
                        : ::agge::core::exception(e)
        {
        }

        opengl_error::opengl_error(const char *file, int line)
                        : ::agge::core::exception(file, line)
        {
        }

        opengl_error::~opengl_error()
        {
        }

        opengl_error&
        opengl_error::operator =(const opengl_error& e)
        {
            agge::core::exception::operator =(e);
            return *this;
        }

        void opengl_error::raise(const char *file, int line, const char *method,
                                 GLenum glerror)
        {
            opengl_error e(file, line);
            e << "OpenGL error in call to '" << method << "': ("
              << (int) glerror << ") " << gl_error_message(glerror)
              << core::exception::backtrace;
            AGGE_ERROR_LOG(OPENGL) << e << agge::core::log::flush;
            throw e;
        }

        void opengl_error::check_error(const char *file, int line,
                                       const char *method)
        {
            GLenum err = glGetError();
            if (err == GL_NO_ERROR) {
                return;
            }
            raise(file, line, method, err);
        }

        void opengl_error::log_error(const char *file, int line,
                                     const char *method)
        {
            GLenum err = glGetError();
            if (err == GL_NO_ERROR) {
                return;
            }
            AGGE_ERROR_LOG(OPENGL) << "OpenGL error in call to '" << method
                                    << "':"  << file << ":" << line
                                    << ": (" << err << ") "
                                    << gl_error_message(err)
                                    << agge::core::log::flush;
            return;
        }

        void opengl_error::clear_error()
        {
            GLenum err = glGetError();
            if (err != GL_NO_ERROR) {
                AGGE_WARNING_LOG(OPENGL) << "OpenGL error " << err
                                         << " ignored."
                                         << agge::core::log::flush;
            }
        }

    }
}
