/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "opengl_monitor.hpp"

namespace agge {
    namespace opengl {

        monitor::monitor(GLFWmonitor *monitor, bool primary)
                        : m_monitor(monitor), m_is_primary(primary)
        {
            int x, y, w, h;

            glfwGetMonitorPos(monitor, &x, &y);
            glfwGetMonitorPhysicalSize(monitor, &w, &h);

            m_dimension = agge::graphics::dimension(x, y, w, h);

            m_name = glfwGetMonitorName(monitor);
        }

        monitor::~monitor()
        {
        }

        bool monitor::is_primary() const
        {
            return m_is_primary;
        }

        const std::string&
        monitor::get_name() const
        {
            return m_name;
        }

        const agge::graphics::dimension&
        monitor::get_dimension() const
        {
            return m_dimension;
        }

        agge::graphics::monitor::video_mode_list monitor::get_video_modes() const
        {
            agge::graphics::monitor::video_mode_list l;
            const GLFWvidmode* modes;
            int count;
            modes = glfwGetVideoModes(m_monitor, &count);
            if (modes && count > 0) {
                for (int i = 0; i < count; ++i) {
                    l.push_back(agge::graphics::video_mode(
                                    (unsigned int) modes[i].width,
                                    (unsigned int) modes[i].height,
                                    (unsigned int) modes[i].redBits,
                                    (unsigned int) modes[i].greenBits,
                                    (unsigned int) modes[i].blueBits,
                                    (unsigned int) modes[i].refreshRate));
                }
            }
            return l;
        }

    }
}
