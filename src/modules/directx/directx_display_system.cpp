/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "directx_display_system.hpp"

namespace agge {
    namespace directx {

        display_system::display_system()
        {
        }

        display_system::~display_system()
        {
        }

        const agge::graphics::display_system_info&
        display_system::get_system_info() const
        {
            static agge::graphics::display_system_info system_info("directx", "",
                                                                   "", "");
            return system_info;
        }

        AGGE_REGISTER_IMPLEMENTATION_ALIAS(display_system,
                                           agge::graphics::display_system,
                                           directx);

    }
}
