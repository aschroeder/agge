/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_DIRECTX_DISPLAY_SYSTEM_HPP
#define AGGE_DIRECTX_DISPLAY_SYSTEM_HPP

#include "agge/graphics/display_system.hpp"

namespace agge {
    namespace directx {

        class display_system : public agge::graphics::display_system
        {
        public:
            display_system();
            ~display_system();

            virtual const agge::graphics::display_system_info& get_system_info() const;
        };

    }
}
#endif
