/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "directx_error.hpp"

namespace agge {
    namespace directx {
        directx_error::directx_error()
        {
        }

        directx_error::directx_error(const directx_error &e)
                        : ::agge::core::exception(e)
        {
        }

        directx_error::directx_error(const char *file, int line)
                        : ::agge::core::exception(file, line)
        {
        }

        directx_error::~directx_error()
        {
        }

        directx_error&
        directx_error::operator =(const directx_error& e)
        {
            ::agge::core::exception::operator =(e);
            return *this;
        }

        void directx_error::check_error(const char *file, int line,
                                        const char *function, HRESULT hresult)
        {
            if (hresult == S_OK) {
                return;
            }
            throw new_error(file, line, function, hresult);
        }

        directx_error directx_error::new_error(const char *file, int line,
                                               const char *function,
                                               HRESULT hresult)

        {
            directx_error e(file, line);
            e << "Error calling function '" << function << "': ";
            switch (hresult) {
            case D3D11_ERROR_FILE_NOT_FOUND:
                e << "File not found";
                break;
            case E_INVALIDARG:
                e << "Invalid argument";
                break;
            default:
                e << "Unknown error, HRESULT=" << (unsigned int) hresult;
                break;
            }
            return e;
        }

    }
}
