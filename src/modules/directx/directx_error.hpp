/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_DIRECTX_ERROR_HPP
#define AGGE_DIRECTX_ERROR_HPP

#include "agge/core/exception.hpp"
#include "directx_common.hpp"


namespace agge {
    namespace directx {

        class directx_error : public agge::core::exception
        {
        public:
            directx_error();
            directx_error(const directx_error& e);
            directx_error(const char *file, int line);
            virtual ~directx_error();
            directx_error& operator=(const directx_error& e);
            template<typename T>
            directx_error& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }

            static void check_error(const char *file, int line,
                                    const char *function, HRESULT hresult);
            static directx_error new_error(const char *file, int line,
                                           const char *function, HRESULT error);
        };

#define CHECK_DIRECTX_ERROR(function, hresult) \
    ::agge::directx::directx_error::check_error(__FILE__, __LINE__, #function, hresult)

#define DIRECTX_ERROR(function, hresult) \
        ::agge::directx::directx_error::new_error(__FILE__, __LINE__, #function, hresult)

    }
}
#endif
