/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_APPLICATION_HPP
#define AGGE_CORE_APPLICATION_HPP

#include "agge/core/globals.hpp"
#include "agge/core/component.hpp"
#include "agge/core/function_map.hpp"
#include "agge/application/dllexport.hpp"


#include <string>
#include <vector>
#include <functional>

namespace agge {
    namespace application {

        /// Reference to application object.
        AGGE_DECLARE_REF(application);

        /**
         * Application framework.
         */
        class AGGE_APPLICATION_EXPORT application : public agge::core::component
        {
        public:
            typedef std::function<void(std::int64_t)> update_listener;
            typedef std::function<void(float)> display_listener;


            /**
             * Constructor.
             */
            application();

            /**
             * Destructor.
             */
            ~application();

            /**
             * Set application arguments.
             * @param argc argument count
             * @param argv argument values
             */
            void set_arguments(int argc, const char **argv);

            /**
             * Get the argument count.
             * @return argument count
             */
            int get_argc() const;

            /**
             * Get argument values.
             * @return argument values
             */
            const char **get_argv() const;

            /**
             * Get argument,
             * @param index argument index
             * @return index-th argument
             */
            const std::string& get_argument(int index) const;

            /**
             * Get whole command line (including program name).
             * @return command line
             */
            const std::string get_commandline() const;

            /**
             * Get the program name.
             * @return program name
             */
            const std::string& get_program_name() const;

            /**
             * Set application quit flag, i.e. the application shall terminate.
             * @param quit flag value
             * @throw illegal_state if flag was set to @c true and is tried to set
             *        back to @c false
             */
            void set_quit(bool quit);

            /**
             * Set the update rate. The update rate determines how often per
             * second the main loop invokes the update listeners, i.e.
             * processes the state of the application and input events.
             * @param rate
             */
            void set_update_rate(int rate);

            /**
             * Get the update rate. Default update rate is 25.
             * @return update rate
             */
            int get_update_rate() const;

            /**
             * Get max number of frames to drop to favor update instead of
             * rendering. Default value is 5, which implies 5 fps.
             * @return max frame skip
             */
            int get_max_frame_skip() const;

            /**
             * Get the quit flag.
             * @return quit flag
             */
            bool is_quit() const;

            /**
             * Run application instance.
             * @param argc argument count
             * @param argv argument values
             * @return result of application run
             */
            static int run_application(int argc, const char **argv);

            /**
             * Runs the application.
             * @return application exit code
             */
            virtual int run();

            /**
             * Add an update listener.
             * @param l update listener
             * @return id of update listener
             */
            unsigned int add_update_listener(const update_listener& l);

            /**
             * Add a display listener.
             * @param l display listener
             * @return id of display listener
             */
            unsigned int add_display_listener(const display_listener& l);

            /**
             * Removes a display listener.
             * @param id id of listener
             */
            bool remove_display_listener(unsigned int id);

            /**
             * Removes a update listener.
             * @param id id of update listener
             */
            bool remove_update_listener(unsigned int id);

            /**
             * Get application instance.
             * @return instance, or 0 if there is none
             */
            static application* get_instance();

        protected:
            /**
             * Run the main loop, using the application's
             * as termination flag.
             */
            void run_main_loop();

            /**
             * Update the application state.
             * @param nanoseconds current nanoseconds
             */
            void update_application(std::int64_t nanoseconds);

            /**
             * Informs displays to publish a frame.
             * @param interpolation amount of time into the next update time
             */
            void display_application(float interpolation);

        private:
            /// @internal Log information on application startup.
            void log_application_info(int argc, const char **argv);
            /// @internal Process command line arguments
            void process_arguments(int argc, const char **);
            /// @internal Filter out and process configuration overrides.
            void filter_configuration_arguments();
            std::vector<std::string> m_arguments; //!< stored command line arguments
            std::string m_program_name;    //!< program name
            char **m_argv;            //!< argument values as supplied
            bool m_quit;              //!< flag: shall the application quit
            int  m_update_rate;       //!< rate of update, in Hz
            int  m_max_frame_skip;    //!< max number of frames skipped to favor update
            std::int64_t m_last_time; //!< timestamp of last update call
            std::int64_t m_elapsed_time; //!< time since last update call

            agge::core::function_map<void(std::int64_t)> m_update_listeners;
            agge::core::function_map<void(float)> m_display_listeners;

            static application *s_instance;   //!< pointer to singleton instance
        };

    /**
     * @def AGGE_MAINFUNCTION
     * Standard main function definition. Instantiates the application instance,
     * and runs the application.
     */
#define AGGE_MAINFUNCTION int main(int argc, const char **argv) \
        { return agge::application::application::run_application(argc, argv); }

    }
}

#endif
