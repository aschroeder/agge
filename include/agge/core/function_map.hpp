/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_FUNCTION_MAP_HPP
#define AGGE_CORE_FUNCTION_MAP_HPP

#include <functional>
#include <map>

namespace agge {
    namespace core {

        template<typename T>
        class function_map : public std::map<unsigned int, std::function<T>>
        {
        public:
            typedef std::map<unsigned int, std::function<T>> base_type;

            function_map()
                            : m_sequence(1)
            {
            }

            function_map(const function_map& other)
                            : base_type(other), m_sequence(other.m_sequence)
            {
            }

            function_map(function_map&& other)
                            : base_type(other), m_sequence(other.m_sequence)
            {
            }

            unsigned int insert(const std::function<T>& f)
            {
                unsigned int k = m_sequence++;
                base_type::operator[](k) = f;
                return k;
            }

            unsigned int m_sequence;
        };

    }
}

#endif

