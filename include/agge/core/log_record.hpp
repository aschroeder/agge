/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_LOG_RECORD_HPP
#define AGGE_CORE_LOG_RECORD_HPP

#include "agge/core/setup.hpp"
#include "agge/core/system.hpp"
#include "agge/core/log_severity.hpp"

namespace agge {
        namespace core {

        /**
         * Log record as it will be published by log.
         */
        class AGGE_CORE_EXPORT log_record
        {
        public:
            /**
             * Default constructor.
             */
            log_record();

            /**
             * Constructor.
             * @param severity   log entry severity
             * @param timestamp  timstamp of log entry creation
             * @param category   category
             * @param message    entry message
             */
            log_record(const log_severity& severity, const system::timestamp& timestamp,
                       const std::string& category, const std::string& message);

            /**
             * Copy constructor.
             * @param r copied record
             */
            log_record(const log_record& r);

            /**
             * Assignment operator.
             * @param r assigned record
             * @return @c *this
             */
            log_record& operator =(const log_record& r);

            /**
             * Get record severity.
             * @return record severity, ERROR if not set
             */
            const log_severity& get_severity() const;

            /**
             * Get record category.
             * @return record category
             */
            inline const std::string& get_category() const
            {
                return m_category;
            }

            /**
             * Get record time stamp.
             * @return creation time stamp
             */
            inline const system::timestamp& get_timestamp() const
            {
                return m_timestamp;
            }

            /**
             * Get message.
             * @return message
             */
            inline const std::string& get_message() const
            {
                return m_message;
            }
        private:
            const log_severity* m_severity;   //!< severity
            system::timestamp m_timestamp;  //!< creation time stamp
            std::string m_category;   //!< category
            std::string m_message;    //!< log message
        };

        /**
         * Simple stream dump operator. Dumps in the format of the text
         * formatter.
         * @param os stream
         * @param r log record
         * @return @c os
         */
        AGGE_CORE_EXPORT std::ostream&
        operator << (std::ostream& os, const log_record& r);

    }
}

#endif
