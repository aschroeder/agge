/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_CONFIGURATION_HPP
#define AGGE_CORE_CONFIGURATION_HPP

#include <string>
#include <map>
#include <vector>

#include "agge/core/globals.hpp"
#include "agge/core/stdexceptions.hpp"
#include "agge/core/lexical_cast.hpp"

namespace agge {

    namespace core {

        /**
         * Exception to signal a configuration key was not found.
         */
        class AGGE_CORE_EXPORT configuration_key_not_found
            : public agge::core::no_such_element
        {
        public:
            configuration_key_not_found();
            configuration_key_not_found(const configuration_key_not_found& e);
            configuration_key_not_found(const char *file, int line);
            virtual ~configuration_key_not_found();

            template <typename T>
            exception& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };

        /**
         * Configuration.
         */
        class AGGE_CORE_EXPORT configuration
        {
        public:
            /**
             * Map of configuration values.
             */
            typedef std::map<std::string, std::string> config_values_map;

            /**
             * List of configuration keys.
             */
            typedef std::vector<std::string> key_list;

            /**
             * Default constructor, creates empty configuration.
             */
            configuration();

            /**
             * Constructor.
             * @param prefix configuration prefix (or section)
             */
            configuration(const std::string& prefix);

            /**
             * Constructor (move variant).
             * @param prefix configuration prefix (or section)
             */
            configuration(std::string&& prefix);

            /**
             * Copy constructor.
             * @param c copied configuration
             */
            configuration(const configuration& c);

            /**
             * Copy constructor (move variant).
             * @param c copied configuration
             */
            configuration(configuration&& c);

            /**
             * Assignment operator.
             * @param c assigned configuration
             * @return @c *this
             */
            configuration& operator = (const configuration& c);

            /**
             * Return whether configuration value exist for a key.
             * @param key configuration key
             * @return @c true if value exists
             */
            bool contains_key(const char *key) const;

            /**
             * Return whether configuration value exist for a key.
             * @param key configuration key
             * @return @c true if value exists
             */
            bool contains_key(const std::string& key) const;

            /**
             * Get configuration keys.
             * @return list of configuration keys
             */
            key_list get_keys() const
            {
                key_list result;
                for(auto p: m_values) {
                    result.push_back(p.first);
                }
                return result;
            }

            /**
             * Get configuration value.
             * @param key configuration key
             * @return configuration value
             * @throw @c configuration_key_not_found if no key is found
             */
            std::string get(const char *key) const
            {
                std::string keystr(key);
                return get(keystr);
            }

            /**
             * Get configuration value.
             * @param key configuration key
             * @return configuration value
             * @throw @c configuration_key_not_found if no key is found
             */
            const std::string& get(const std::string& key) const;

            /**
             * Get configuration value.
             * @param key configuration key
             * @param default_value value used if key not found
             * @return configuration value, @c default_value if no such entry
             *         is found
             */
            template <typename T>
            T get(const char *key, const char *default_value=0) const
            {
                try {
                    return lexical_cast<T>(get(key));
                } catch(const configuration_key_not_found&) {
                    if(default_value) {
                        return lexical_cast<T>(default_value);
                    } else {
                        throw;
                    }
                }
            }

            /**
             * Get configuration value.
             * @param key configuration key
             * @param default_value value used if key not found
             * @return configuration value, @c default_value if no such entry
             *         is found
             */
            template <typename T>
            T get(const std::string& key, const char *default_value=0) const
            {
                try {
                    return lexical_cast<T>(get(key));
                } catch(const configuration_key_not_found&) {
                    if(default_value) {
                        return lexical_cast<T>(default_value);
                    } else {
                        throw;
                    }
                }
            }

            /**
             * Return whether configuration has no entries.
             * @return @c true if configuration has no entries
             */
            bool empty() const
            {
                return m_values.empty();
            }

            /**
             * Set a configuration value.
             * @param key configuration key
             * @param value configuration value
             */
            template <typename T>
            inline void set(const char *key, const T& value)
            {
                m_values[key] = lexical_cast<std::string>(value);
            }

            /**
             * Store the configuration persistently.
             */
            void store();

            /**
             * Clear the configuration.
             */
            void clear();

            /**
             * Set a transient override for a stored configuration value.
             * @param key   configuration key
             * @param value configuration value
             */
            static void set_override(const char *key, const char *value);

            /**
             * Deletes transient override value.
             * @param key configuration key
             */
            static void clear_override(const char *key);

        private:
            std::string       m_prefix;
            config_values_map m_values;
        };

    }
}

#endif
