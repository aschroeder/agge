/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_LOG_HANDLER_HPP
#define AGGE_CORE_LOG_HANDLER_HPP

#include "agge/core/setup.hpp"
#include "agge/core/log_record.hpp"
#include "agge/core/component.hpp"
#include "agge/core/configuration.hpp"

namespace agge {
namespace core {

AGGE_DECLARE_REF(log_handler);

/**
 * A log handler processes a log entry.
 */
class AGGE_CORE_EXPORT log_handler: public agge::core::component
{
public:
    /**
     * Constructor.
     */
    log_handler();

    /**
     * Destructor.
     */
    virtual ~log_handler();

    /**
     * Process log entry.
     * @param record log record
     */
    virtual void log(const log_record& record) = 0;

    /**
     * Configure the log handler.
     * @param config handler configuration
     */
    virtual void configure(const configuration& config) = 0;
};
}
}

#endif
