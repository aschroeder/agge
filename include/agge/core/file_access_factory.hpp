/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_FILE_ACCESS_FACTORY_HPP
#define AGGE_CORE_FILE_ACCESS_FACTORY_HPP

#include "component.hpp"
#include "file_access.hpp"

namespace agge {
namespace core {

// @class file_access_factor_ref
// @brief Shared pointer to @c file_access_factory.
AGGE_DECLARE_REF(file_access_factory);

/**
 * Factor class for file access.
 */
class file_access_factory: public component
{
public:
    /**
     * Constructor.
     */
    file_access_factory();
    /**
     * Destructor.
     */
    virtual ~file_access_factory();

    /**
     * Retrieve file access object.
     * @param path path
     * @return file access object
     */
    virtual file_access_ref get_file_access(const std::string& path) = 0;
    /**
     * Retrieve file access object.
     * @param path path
     * @param name file name in path
     * @return file access object
     */
    virtual file_access_ref get_file_access(const std::string& path,
                                            const std::string& name) = 0;
};

}
}
#endif
