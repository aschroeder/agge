/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CONFIGURATION_DATABASE_HPP
#define AGGE_CONFIGURATION_DATABASE_HPP

#include "agge/core/init_on_demand.hpp"

#include <string>
#include <map>

namespace agge {
    namespace core {

        class configuration_database
        {
            static init_on_demand<configuration_database> m_instance;
        public:
            typedef std::map<std::string, std::string> value_map;

            configuration_database();
            ~configuration_database();
            static configuration_database& get_instance();
            void read_configuration_values(
                            value_map& values);
            void read_configuration_values(
                            const char *prefix,
                            value_map& values);
            void store_configuration_values(
                            value_map& values);
            void store_configuration_values(
                            const char *prefix,
                            value_map& values);
            void clear_configuration(const char *prefix);
            void delete_key(const char *key);

        private:
            static std::string get_configuration_directory();
            void open_database();
            void close_database();
            void check_schema();
            bool schema_exists();
            void create_schema();
            void store_value(const std::string& key, const std::string& value);

            void *m_db;
        };

    }
}
#endif
