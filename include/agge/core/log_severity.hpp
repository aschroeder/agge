/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_LOG_SEVERITY_HPP
#define AGGE_CORE_LOG_SEVERITY_HPP

#include "agge/core/setup.hpp"

#include <string>


namespace agge {
    namespace core {

        /**
         * Log severity.
         */
        class AGGE_CORE_EXPORT log_severity
        {
        public:
            static const log_severity& get(const std::string& level);

            static const log_severity& LOG_SEVERITY_DEBUG();
            static const log_severity& LOG_SEVERITY_INFO();
            static const log_severity& LOG_SEVERITY_WARNING();
            static const log_severity& LOG_SEVERITY_ERROR();
            static const log_severity& LOG_SEVERITY_ALL(); //!< pseudo severity used in config

            static const log_severity& D();
            static const log_severity& I();
            static const log_severity& W();
            static const log_severity& E();

            inline bool operator ==(const log_severity& s) const
            {
                return m_level == s.m_level;
            }

            inline bool operator !=(const log_severity& s) const
            {
                return m_level != s.m_level;
            }

            inline bool operator <(const log_severity& s) const
            {
                return m_level > s.m_level;
            }

            inline bool operator <=(const log_severity& s) const
            {
                return m_level >= s.m_level;
            }

            inline bool operator >(const log_severity& s) const
            {
                return m_level < s.m_level;
            }

            inline bool operator >=(const log_severity& s) const
            {
                return m_level <= s.m_level;
            }

            inline const std::string& get_name() const
            {
                return m_name;
            }

            inline unsigned int get_level() const
            {
                return m_level;
            }

            inline const std::string& get_abbreviation() const
            {
                return m_abbrev;
            }

            log_severity(const char *name,
                         const char *abbrev,
                         unsigned int level);
        private:
            std::string m_name;
            std::string m_abbrev;
            unsigned int m_level;
        };

        inline std::ostream&
        operator <<(std::ostream& os, const log_severity& s)
        {
            return os << s.get_name();
        }

    }
}

#endif
