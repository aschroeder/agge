/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_FILE_HPP
#define AGGE_CORE_FILE_HPP

/**
 * @file   agge_file.hpp
 * @brief  File handling.
 */

#include "agge/core/file_access.hpp"
#include "agge/core/path.hpp"

namespace agge {
    namespace core {

        /**
         * Handle to a file (or directory).
         */
        class AGGE_CORE_EXPORT file
        {
        public:
            file(const file_access_ref& access);

            /**
             * Create file.
             * @param path
             */
            file(const char *path);

            /**
             * Create file.
             * @param path
             */
            file(const std::string& path);

            /**
             * Create file from path.
             * @param path path
             */
            file(const path& path);

            /**
             * Destructor.
             */
            ~file();

            /**
             * Constructor.
             * @param path file path
             * @param name file name
             */
            file(const std::string& path, const std::string& name);

            /**
             * Get the extension of the file.
             * @return extension string, may be empty if file has no extension
             */
            std::string get_extension() const;

            /**
             * Get path of file.
             * @return path
             */
            std::string get_path() const;

            /**
             * Get file name (without directory).
             * @return file name
             */
            std::string get_file_name() const;

            /**
             * Check if file exists.
             * @return @c true if exists
             */
            bool exists() const;

            /**
             * Check if file is a directory.
             * @return @c true if it is a directory
             */
            bool is_directory() const;

            /**
             * Check whether file is a real file
             * @return @c true if it is a file
             */
            bool is_file() const;

            /**
             * Check whether the file is a real file system file. Some operations
             * can only done on real file system files.
             * @return @c true if the file is a real file system file
             */
            bool is_system_file() const;

            /**
             * Create the directory described by this file.
             */
            void mkdir();

            /**
             * Remove directory described by this file.
             */
            void rmdir();

            /**
             * List contents of directory.
             * @param files files filled
             */
            void list(std::vector<file>& files);

            /**
             * Opens file for input.
             * @return an input stream reference to opened file
             */
            input_stream_ref open_for_input();

        private:
            file_access_ref m_access;
        };

        /**
         * Stream output operator. Prints the file name.
         * @param os stream
         * @param f file
         * @return @c os
         */
        AGGE_CORE_EXPORT std::ostream&
        operator <<(std::ostream& os, const file& f);
    }
}
#endif
