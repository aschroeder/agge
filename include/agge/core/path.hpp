/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_PATH_HPP
#define AGGE_CORE_PATH_HPP

#include "agge/core/dllexport.hpp"
#include "agge/core/globals.hpp"

#include <vector>
#include <string>
#include <iosfwd>

namespace agge {
    namespace core {
        class AGGE_CORE_EXPORT path
        {
        public:
            path();
            path(const char *p);
            path(const std::string& p);
            path(const path& p);
            path(const path& p, const char *f);
            path(const path& p, const std::string& f);
            path(path&& p);
            ~path();

            path& operator=(const path& p);
            path& operator /=(const char *path);
            path& operator /=(const std::string& path);
            path& operator /=(const path& path);

            std::string get_native_string() const;
            std::string get_generic_string() const;
            std::string get_extension() const;
            std::string get_file_name() const;

            bool is_absolute() const;
        private:
            void construct(const std::string& p);
            void normalize();

            std::vector<std::string> m_elements;
            bool m_absolute;
        };

        AGGE_CORE_EXPORT std::ostream& operator <<(std::ostream& os,
                                                   const path& p);
    }
}

#endif
