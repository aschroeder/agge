/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_INIT_ON_DEMAND_HPP
#define AGGE_CORE_INIT_ON_DEMAND_HPP

/**
 * @file   init_on_demand.hpp
 * @brief  Late initialization.
 */

#include "agge/core/setup.hpp"

namespace agge {
namespace core {

template <typename T>
class init_on_demand
{
public:
    /**
     * Constructor.
     */
    init_on_demand()
            : m_instance(0)
    {
    }

    /**
     * Destructor.
     */
    ~init_on_demand()
    {
        delete m_instance;
    }

    /**
     * Returns the pointer to the data.
     * @return pointer to initialized instances
     */
    inline T * ptr()
    {
        if (m_instance) {
            return m_instance;
        } else {
            T *ptr = new T();
            m_instance = ptr;
            return m_instance;
        }
    }

    inline T* operator ->()
    {
        return ptr();
    }

    inline const T* operator ->() const
    {
        return ptr();
    }

    inline T& operator *()
    {
        return *ptr();
    }

    inline const T& operator *() const
    {
        return *ptr();
    }

private:
    T * m_instance;     //!< instance managed by this object
};

}
}

#endif
