/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_SYSTEM_HPP
#define AGGE_CORE_SYSTEM_HPP

/**
 * @file   system.hpp
 * @brief  System functionality.
 */

#include "agge/core/setup.hpp"

#include <typeinfo>
#include <string>

namespace agge {
namespace core {

/**
 * This class bundles system dependent functionality. Note that
 * there are specific parts of system-dependent functionality
 * remaining, such as threading, file access, and network access
 * which are not covered within this class.
 */
class AGGE_CORE_EXPORT system
{
public:
    /**
     * Type definition of atomic value.
     */
    typedef int atomic_value;

    /**
     * Type definition of pointer to atomic value.
     */
    typedef int * atomic_value_ptr;

    /**
     * A time stamp.
     */
    struct timestamp
    {
        unsigned short year;
        unsigned char month;
        unsigned char day;
        unsigned char hour;
        unsigned char minute;
        unsigned char second;
        unsigned short millisecond;

        timestamp()
        :year(0),
         month(0),
         day(0),
         hour(0),
         minute(0),
         second(0),
         millisecond(0)
        {}
    };

    /**
     * Get the elapsed time since program start.
     * @return nano seconds since program start
     */
    static std::int64_t get_current_nanos();

    /**
     * Get the current time.
     * @param ts receives the time stamp
     */
    static void get_current_time(timestamp& ts);

    /**
     * Atomically compares and exchanges values.
     * @param destination pointer to the value to exchange
     * @param exchange    value to store
     * @param comparand   value to compare
     * @return @c true if the exchange did take place
     */
    static bool atomic_cmpxchg(atomic_value_ptr destination,
                               atomic_value exchange, atomic_value comparand);
    /**
     * Atomically increments the target.
     * @param target incrementation target
     * @return incremented value
     */
    static atomic_value atomic_inc(atomic_value_ptr target);

    /**
     * Atomically increments the target.
     * @param target decrementation target
     * @return decremented value
     */
    static atomic_value atomic_dec(atomic_value_ptr target);

    /**
     * Get the type name of the type identified by the type info.
     * @param typeinfo identifying type info
     * @return type name
     */
    static const std::string& get_type_name(const std::type_info& typeinfo);

    /**
     * Get the current working directory.
     * @return current working directory
     */
    static std::string get_cwd();

    /**
     * Aborts the program. The message is displayed, and the program
     * execution is halted.
     * @param format <tt>printf</tt> format string of the abort message
     * @param ...    abort message arguments
     */
    static void abort(const char *format, ...);


    /**
     * Calls the debugger, if one is attached. Otherwise usually the
     * program will abort.
     */
    static void debug_break();

    /**
     * Get an unique identifier.
     * @param prefix name prefix
     * @return @c prefix with an unique suffix appended
     */
    static std::string get_unique_id(const std::string& prefix);

    /**
     * Get name of current executable program.
     * @return name of executable program
     */
    static std::string get_executable_name();
};

/**
 * Stream output operator for time stamp.
 * @param o output stream
 * @param ts time stamp to print
 * @return @c o
 */
AGGE_CORE_EXPORT std::ostream& operator<<(std::ostream& o,
                                          const system::timestamp& ts);

} // core
} // agge

#endif
