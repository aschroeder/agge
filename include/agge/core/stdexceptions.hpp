/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_STDEXCEPTIONS_HPP
#define AGGE_CORE_STDEXCEPTIONS_HPP

#include "agge/core/exception.hpp"

/**
 * @file   stdexceptions.hpp
 * @brief  Exception class.
 */
namespace agge {
    namespace core {

        /**
         * @class agge::core::illegal_argument
         * Raised when an illegal argument is supplied to a method.
         */
        class AGGE_CORE_EXPORT illegal_argument : public agge::core::exception
        {
        public:
            illegal_argument();
            illegal_argument(const illegal_argument& e);
            illegal_argument(const char *file, int line);
            virtual ~illegal_argument();
            illegal_argument& operator=(const illegal_argument& e);

            template <typename T>
            illegal_argument& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };

        /**
         * @class agge::core::null_pointer
         * Raised when a null pointer or reference is detected at a place where it is
         * inappropriate.
         */
        class AGGE_CORE_EXPORT null_pointer : public agge::core::exception
        {
        public:
            null_pointer();
            null_pointer(const null_pointer& e);
            null_pointer(const char *file, int line);
            virtual ~null_pointer();
            null_pointer& operator=(const null_pointer& e);

            template <typename T>
            null_pointer& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };


        /**
         * @class agge::core::illegal_state
         * Raised when an illegal state is detected.
         */
        class AGGE_CORE_EXPORT illegal_state : public agge::core::exception
        {
        public:
            illegal_state();
            illegal_state(const illegal_state& e);
            illegal_state(const char *file, int line);
            virtual ~illegal_state();
            illegal_state& operator=(const illegal_state& e);

            template <typename T>
            illegal_state& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };


        /**
         * @class agge::core::index_out_of_bounds
         * Raised if an access to a data structure using an invalid index is detected.
         */
        class AGGE_CORE_EXPORT index_out_of_bounds : public agge::core::exception
        {
        public:
            index_out_of_bounds();
            index_out_of_bounds(const index_out_of_bounds& e);
            index_out_of_bounds(const char *file, int line);
            virtual ~index_out_of_bounds();
            index_out_of_bounds& operator=(const index_out_of_bounds& e);

            template <typename T>
            index_out_of_bounds& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };

        /**
         * @class agge::core::not_implemented
         * Raised if a not implemented feature is accessed.
         */
        class AGGE_CORE_EXPORT not_implemented : public agge::core::exception
        {
        public:
            not_implemented();
            not_implemented(const not_implemented& e);
            not_implemented(const char *file, int line);
            virtual ~not_implemented();
            not_implemented& operator=(const not_implemented& e);

            template <typename T>
            not_implemented& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };


        /**
         * @class agge::core::system_error
         * Raised if a system error occurs.
         */
        class AGGE_CORE_EXPORT system_error : public agge::core::exception
        {
        public:
            system_error();
            system_error(const system_error& e);
            system_error(const char *file, int line);
            virtual ~system_error();
            system_error& operator=(const system_error& e);

            template <typename T>
            system_error& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };


        /**
         * @class agge::core::configuration_error
         * Raised if a configuration problem occurs.
         */
        class AGGE_CORE_EXPORT configuration_error : public agge::core::exception
        {
        public:
            configuration_error();
            configuration_error(const configuration_error& e);
            configuration_error(const char *file, int line);
            virtual ~configuration_error();
            configuration_error& operator=(const configuration_error& e);

            template <typename T>
            configuration_error& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };

        /**
         * @class agge::core::duplicate_element
         * Raised if a constraint of uniqueness was violated.
         */
        class AGGE_CORE_EXPORT duplicate_element : public agge::core::exception
        {
        public:
            duplicate_element();
            duplicate_element(const duplicate_element& e);
            duplicate_element(const char *file, int line);
            virtual ~duplicate_element();
            duplicate_element& operator=(const duplicate_element& e);

            template <typename T>
            duplicate_element& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };

        /**
         * @class agge::core::out_of_memoy
         * Raised if a memory allocation fails.
         */
        class AGGE_CORE_EXPORT out_of_memory : public agge::core::exception
        {
        public:
            out_of_memory();
            out_of_memory(const out_of_memory& e);
            out_of_memory(const char *file, int line);
            virtual ~out_of_memory();
            out_of_memory& operator=(const out_of_memory& e);

            template <typename T>
            out_of_memory& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };

        /**
         * @class agge::core::unsupported_operation
         * Raised if an optional feature/operation is requested which isn't supported.
         */
        class AGGE_CORE_EXPORT unsupported_operation : public agge::core::exception
        {
        public:
            unsupported_operation();
            unsupported_operation(const unsupported_operation& e);
            unsupported_operation(const char *file, int line);
            virtual ~unsupported_operation();
            unsupported_operation& operator=(const unsupported_operation& e);

            template <typename T>
            unsupported_operation& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };

        /**
         * @class agge::core::runtime_exception
         * Raised if 3rd party component signals an error that cannot be further handled.
         */
        class AGGE_CORE_EXPORT runtime_exception : public agge::core::exception
        {
        public:
            runtime_exception();
            runtime_exception(const runtime_exception& e);
            runtime_exception(const char *file, int line);
            virtual ~runtime_exception();
            runtime_exception& operator=(const runtime_exception& e);

            template <typename T>
            runtime_exception& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };

        /**
         * @class agge::core::pure_virtual_call
         * Raised if a method is called that wasn't supposed to be called
         * because it has no viable implementation in the base class.
         */
        class AGGE_CORE_EXPORT pure_virtual_call : public agge::core::exception
        {
        public:
            pure_virtual_call();
            pure_virtual_call(const pure_virtual_call& e);
            pure_virtual_call(const char *file, int line);
            virtual ~pure_virtual_call();
            pure_virtual_call& operator=(const pure_virtual_call& e);

            template <typename T>
            pure_virtual_call& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };


        /**
         * @class agge::core::no_such_element
         * Raised if element is is not found in a collection.
         */
        class AGGE_CORE_EXPORT no_such_element : public agge::core::exception
        {
        public:
            no_such_element();
            no_such_element(const no_such_element& e);
            no_such_element(const char *file, int line);
            virtual ~no_such_element();
            no_such_element& operator=(const no_such_element& e);

            template <typename T>
            no_such_element& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };

        /**
         * @class agge::core::bad_cast
         * Raised when an type conversion fails.
         */
        class AGGE_CORE_EXPORT bad_cast : public agge::core::exception
        {
        public:
            bad_cast();
            bad_cast(const bad_cast& e);
            bad_cast(const char *file, int line);
            virtual ~bad_cast();
            bad_cast& operator=(const bad_cast& e);

            template <typename T>
            bad_cast& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };


/**
 * Checks, and possibly throws a system error. Has no effect if no system error is set.
 */
#define AGGE_CHECK_SYSTEMERROR ::agge::core::exception::system_error(__FILE__, __LINE__, true)

/**
 * Macro to signal a pure virtual call.
 */
#define AGGE_THROW_PURE_VIRTUAL_CALL throw AGGE_EXCEPTION(::agge::core::pure_virtual_call) \
        << "Pure virtual method called." << ::agge::core::exception::backtrace

/**
 * Macro to signal a not implemented function.
 */
#define AGGE_THROW_NOT_IMPLEMENTED throw AGGE_EXCEPTION(::agge::core::not_implemented) << "Not implemented" << ::agge::core::exception::backtrace

/**
 * Macro to assert that an argument must not be null.
 * @param name name of argument
 */
#define AGGE_ASSERT_NOT_NULL_ARGUMENT(name) if(!name) AGGE_THROW(::agge::core::null_pointer) << "Argument '" << #name << "' must not be NULL"

/**
 * Macro to raise an unsupported operation.
 * @param name name of operation
 */
#define AGGE_UNSUPPORTED_OPERATION(name) AGGE_THROW(::agge::core::unsupported_operation) << "Unsupported operation: " << #name << ".";

    }
}
#endif
