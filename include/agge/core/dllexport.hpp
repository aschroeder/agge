/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_DLLEXPORT_HPP
#define AGGE_CORE_DLLEXPORT_HPP

/**
 * @file   dllexport.hpp
 * @brief  Macros to declare import/export for DLLs.
 */

/**
 * @def AGGE_DLLEXPORT
 * @brief Macro to declare class or function to be exported from DLL.
 */
/**
 * @def AGGE_DLLIMPORT
 * @brief Macro to declare class or function to be imported from DLL.
 */
/**
 * @def AGGE_EXPORT
 * @brief Macro that expands to @c AGGE_DLLEXPORT when building AGGE,
 *        and to @c AGGE_DLLIMPORT otherwise.
 */

#ifdef _MSC_VER
#define AGGE_DLLEXPORT __declspec(dllexport)
#define AGGE_DLLIMPORT __declspec(dllimport)
#else
#define AGGE_DLLEXPORT
#define AGGE_DLLIMPORT
#endif

#ifdef CDT_INDEXER
#  define AGGE_CORE_EXPORT __declspec(dllexport)
#else
#  ifdef BUILD_AGGE_CORE
#    define AGGE_CORE_EXPORT AGGE_DLLEXPORT
#  else
#    define AGGE_CORE_EXPORT AGGE_DLLIMPORT
#  endif
#endif

#endif
