/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_STRING_FUNCTIONS_HPP
#define AGGE_CORE_STRING_FUNCTIONS_HPP

#include "agge/core/globals.hpp"
#include <cstring>
#include <string>
#include <iterator>
#include <sstream>

namespace agge {
    namespace core {

        inline std::string trim_copy(const std::string& s)
        {
            const char *cstart = s.c_str();
            while(*cstart && isspace(*cstart)) {
                ++cstart;
            }
            const char *cend = cstart;
            while(*cend && !isspace(*cend)) {
                ++cend;
            }
            return std::string(cstart, cend);
        }

        inline void trim(std::string& s)
        {
            const char *cstart = s.c_str();
            while(*cstart && isspace(*cstart)) {
                ++cstart;
            }
            const char *cend = cstart;
            while(*cend && !isspace(*cend)) {
                ++cend;
            }
            s = std::string(cstart, cend);
        }

        inline void to_lower(std::string& str)
        {
            for(size_t i=0; i<str.size(); ++i) {
                str[i] = static_cast<char>(tolower(str[i]));
            }
            return;
        }

        template <typename C, typename P>
        inline void
        split(C& collection, const std::string& s, P predicate)
        {
            auto ins = std::back_inserter(collection);
            const char *cloop= s.c_str();
            const char *cstart=cloop;
            while(*cloop) {
                if(predicate(*cloop)) {
                    *ins = std::string(cstart, cloop);
                    ++ins;
                    cstart = cloop+1;
                }
                ++cloop;
            }
            *ins = std::string(cstart, cloop);
            return;
        }

        template <typename C>
        inline void
        split(C& collection, const std::string& s, char c)
        {
            auto ins = std::back_inserter(collection);
            const char *cloop= s.c_str();
            const char *cstart=cloop;
            while(*cloop) {
                if(*cloop == c) {
                    *ins = std::string(cstart, cloop);
                    ++ins;
                    cstart = cloop+1;
                }
                ++cloop;
            }
            *ins = std::string(cstart, cloop);
            return;
        }

        template <typename C, typename S>
        inline void
        join(std::string& s, const C& collection, const S& separator)
        {
            bool first = true;
            std::stringstream ss;
            for(auto e : collection) {
                if(!first) {
                    ss << separator;
                } else {
                    first = false;
                }
                ss << e;
            }
            s += ss.str();
        }

    }
}

#endif
