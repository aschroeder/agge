/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_INPUT_STREAM_HPP
#define AGGE_CORE_INPUT_STREAM_HPP

#include "globals.hpp"

#include <istream>

namespace agge {
    namespace core {

        AGGE_DECLARE_REF(std_istream);
        AGGE_DECLARE_REF(input_stream);

        /**
         * An input stream to read bytes.
         */
        class AGGE_CORE_EXPORT input_stream
        {
        public:
            /**
             * Type of positions and offsets.
             */
            typedef std::streamoff offset_type;

            /**
             * Type of number of bytes read.
             */
            typedef std::streamsize streamsize_type;

            /// Seek direction type.
            enum direction_type
            {
                POS_BEG, //!< seek from beginning
                POS_CUR, //!< seek from current position
                POS_END  //!< seek from end
            };

            /**
             * Constructor.
             */
            input_stream();

            /**
             * Destructor.
             */
            virtual ~input_stream();

            /**
             * Read one byte from the input stream. The default
             * implementation uses the <code>read(void*,int)</code>
             * method to obtain the data.
             * @return byte that has been read, or -1 at EOF
             * @throw IOException if an I/O error occurs.
             */
            virtual streamsize_type read();

            /**
             * Read more bytes from the input stream. The default
             * implementation is always at EOF.
             * @param destination target buffer
             * @param size        target buffer size
             * @return number of bytes read, 0 on EOF
             */
            virtual streamsize_type read(void *destination,
                                         streamsize_type size);

            /**
             * Get current position.
             * @return current position, -1 if not known
             */
            virtual offset_type get_position();

            /**
             * Seek relative to a specific position.
             * @param offset seek offset
             * @param dir    seek direction
             * @return position after seek, -1 if not known
             */
            virtual offset_type seek(offset_type offset, direction_type dir);

            /**
             * Access the stream as an @c std::istream
             * @return a @c std::istream using this stream's data
             */
            std::istream& get_istream();
        private:
            std_istream_ref m_stdstream;
        };
    }
}

#endif
