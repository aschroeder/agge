/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_BACKTRACE_ELEMENT_HPP
#define AGGE_CORE_BACKTRACE_ELEMENT_HPP

#include "agge/core/globals.hpp"
#include "agge/core/dllexport.hpp"

#include <string>
#include <iostream>

namespace agge {
    namespace core {

        /**
         * Element of stack backtrace.
         */
        class AGGE_CORE_EXPORT backtrace_element
        {
        public:
            /**
             * Constructor.
             */
            backtrace_element();
            /**
             * Constructor.
             * @param address program counter address for backtrace element
             */
            backtrace_element(void *address);

            /**
             * Constructor.
             * @param address program counter address for backtrace element
             * @param function name of function
             */
            backtrace_element(void *address,
                              const std::string& function);

            /**
             * Constructor.
             * @param address program counter address for backtrace element
             * @param function name of function
             * @param file source file name
             * @param line source file line
             * @param module executable or shared object file
             */
            backtrace_element(void *address,
                              const std::string& function,
                              const std::string& file,
                              unsigned int line,
                              const std::string& module);

            /**
             * Copy constructor.
             * @param e copied element
             */
            backtrace_element(const backtrace_element& e);
            /**
             * Move constructor.
             * @param e moved element
             */
            backtrace_element(backtrace_element&& e);

            /**
             * Destructor
             */
            ~backtrace_element();

            /**
             * Assignment.
             * @param e assigned element
             * @return @c *this
             */
            backtrace_element& operator =(const backtrace_element& e);

            void *get_address() const;
            const std::string& get_function() const;
            const std::string& get_file() const;
            const std::string& get_module() const;
            unsigned int get_line() const;
        private:
            void *m_address;
            std::string m_function;
            std::string m_file;
            std::string m_module;
            unsigned int m_line;
        };


        AGGE_CORE_EXPORT std::ostream& operator <<(std::ostream& os, const backtrace_element& e);

    }
}

#endif
