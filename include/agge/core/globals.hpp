/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_GLOBALS_HPP
#define AGGE_CORE_GLOBALS_HPP

#include "setup.hpp"
#include "noncopyable.hpp"

#include <memory>

/**
 * Define a @c std::shared_ptr ref type.
 * @param clazz type to use (class)
 */
#define AGGE_DECLARE_REF(clazz)                     \
    class clazz;                                    \
    typedef ::std::shared_ptr<clazz> clazz##_ref

#define AGGE_DECLARE_WEAK_REF(clazz)                     \
    class clazz;                                         \
    typedef ::std::weak_ptr<clazz> clazz##_weak_ref

#endif
