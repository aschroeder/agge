/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_LEXICAL_CAST_HPP
#define AGGE_CORE_LEXICAL_CAST_HPP

// a lexical cast, much like boost::lexical_cast
// this serves only as a stopgap implementation,
// which is very wasteful by using the intermediate
// stream

#include "agge/core/globals.hpp"
#include "agge/core/stdexceptions.hpp"

#include <typeinfo>
#include <sstream>

namespace agge {
    namespace core {

        namespace internal {

            template <typename T, typename S>
            inline bool convert(T& target , const S& source)
            {
                std::stringstream ss;
                ss << source;
                ss >> target;
                return !(ss.bad() || ss.fail()) && ss.eof();
            }

        }


        template <typename T, typename S>
        T lexical_cast(const S& source)
        {
            T target;
            if(!internal::convert(target, source)) {
                throw AGGE_EXCEPTION(bad_cast)
                    << "Cannot cast from "
                    << typeid(S).name()
                    << " to "
                    << typeid(T).name();
            }
            return target;
        }

    }
}

#endif
