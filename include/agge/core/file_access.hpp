/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_FILE_ACCESS_HPP
#define AGGE_CORE_FILE_ACCESS_HPP

/**
 * @file file_access.hpp
 * @brief File access handling.
 */

#include "globals.hpp"
#include "exception.hpp"
#include "input_stream.hpp"

#include <string>
#include <vector>

namespace agge {
    namespace core {

        /**
         * @class file_not_found
         * Thrown if a file was not found but is required for
         * the requested operation.
         */
        class AGGE_CORE_EXPORT file_not_found : public agge::core::exception
        {
        public:
            file_not_found();
            file_not_found(const file_not_found& e);
            file_not_found(const char *file, int line);
            virtual ~file_not_found();
            file_not_found& operator =(const file_not_found& e);
            template<typename T>
            file_not_found& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };

        /**
         * @class filesystem_error
         * Thrown if an error occurs in a file system method.
         */
        class AGGE_CORE_EXPORT filesystem_error : public agge::core::exception
        {
        public:
            filesystem_error();
            filesystem_error(const filesystem_error& e);
            filesystem_error(const char *file, int line);
            virtual ~filesystem_error();
            filesystem_error& operator =(const filesystem_error& e);
            template<typename T>
            filesystem_error& operator <<(const T& t)
            {
                agge::core::exception::operator <<(t);
                return *this;
            }
        };

        class file;

        /// @class file_access_ref
        /// @brief Shared pointer to file access.
        AGGE_DECLARE_REF(file_access);

        /**
         * File access handler. Handles file access of one file, identified by a path.
         */
        class AGGE_CORE_EXPORT file_access : public noncopyable
        {
        public:
            /**
             * Constructor.
             * @param path path of handled file
             */
            file_access(const std::string& path);

            /**
             * Destructor.
             */
            virtual ~file_access();

            /**
             * Return whether a file exists.
             * @return @c true if file just now exists
             */
            virtual bool exists() const = 0;

            /**
             * Return whether file is a normal file.
             * @return @c true if it is a file
             */
            virtual bool is_file() const = 0;

            /**
             * Return whether file is a directory.
             * @return @c true if it is a directory
             */
            virtual bool is_directory() const = 0;

            /**
             * Return whether file is a real file system file.
             * @return @c true if a real file
             */
            virtual bool is_system_file() const = 0;

            /**
             * Create directory.
             */
            virtual void mkdir() = 0;

            /**
             * Remove directory.
             */
            virtual void rmdir() = 0;

            /**
             * List directory contents.
             * @param files list of files filled
             */
            virtual void list(std::vector<file>& files) = 0;

            /**
             * Get the path handled by this access object.
             * @return path
             */
            const std::string& get_path() const;

            /**
             * Return file name extension.
             * @return extension, may be empty
             */
            std::string get_extension() const;

            /**
             * Return file name (without directory part).
             * @return file name
             */
            std::string get_file_name() const;

            /**
             * Opens the file for input.
             * @return handle to opened stream
             */
            virtual input_stream_ref open_for_input() const = 0;

        protected:
            /// Constructor to be used if path cannot be determined.
            file_access();

            /**
             * Set path.
             * @param path path
             */
            void set_path(const std::string& path);

        private:
            std::string m_path;
        };

    }
}

#endif
