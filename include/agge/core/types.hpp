/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_TYPES_HPP
#define AGGE_CORE_TYPES_HPP

/// @file   types.hpp
/// @brief  Basic type definitions.

#include <cstdlib>
#include <cstdint>

#include "agge/core/config.hpp"

/**
 * Name space of the AGGE toolkit.
 */
namespace agge {
    /**
     * Name space of AGGE core definitions and types.
     */
    namespace core {

        /**
         * Helper structure to signal that members shall be initialized.
         */
         struct initialize_members_tag
         {
         };

         /**
          * Constant to indicate that members shall be initialized.
          */
         const initialize_members_tag initialize_members = initialize_members_tag();

         /**
          * Helper structure to signal UTF8 character encoding.
          */
         struct utf8_encoding_tag
         {
         };

         /**
          * Helper structure to signal UCS2 character encoding.
          */
         struct ucs2_encoding_tag
         {
         };

         /**
          * Constant to indicate UTF8 character encoding.
          */
         const struct utf8_encoding_tag utf8_encoding = utf8_encoding_tag();

         /**
          * Constant to indicate UCS2 character encoding.
          */
         const struct ucs2_encoding_tag ucs2_encoding = ucs2_encoding_tag();

    } // core

} // agge
#endif
