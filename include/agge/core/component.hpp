/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_COMPONENT_HPP
#define AGGE_CORE_COMPONENT_HPP

#include "agge/core/globals.hpp"
#include "agge/core/system.hpp"

#include <vector>
#include <type_traits>

namespace agge {
    namespace core {

        AGGE_DECLARE_REF(component);

        namespace internal {

            /**
             * @internal Base class for registry entry
             * of a component.
             */
            class AGGE_CORE_EXPORT component_registry_entry_base
            {
            public:
                /**
                 * Constructor.
                 */
                component_registry_entry_base();

                /**
                 * Destructor.
                 */
                virtual ~component_registry_entry_base();

                /**
                 * Return the name of the component.
                 * @return name of the component
                 */
                virtual const char *get_name() const = 0;
            protected:
                void register_component(component_registry_entry_base *component);
            };

            /**
             * @internal Base class for registry entry of an
             * implementation.
             */
            class AGGE_CORE_EXPORT implementation_registry_entry_base
            {
            public:
                /**
                 * Constructor.
                 */
                implementation_registry_entry_base();

                /**
                 * Destructor.
                 */
                virtual ~implementation_registry_entry_base();

                /**
                 * Return the name of the component.
                 * @return name of the component
                 */
                virtual const char *get_component_name() const = 0;

                /**
                 * Return the name of the implementation.
                 *
                 * @return name of the implementation
                 */
                virtual const char *get_name() const = 0;

                /**
                 * Get the alias names.
                 *
                 * @return alias names, separated by one or more spaces each
                 */
                virtual const char *get_alias_names() const = 0;

                /**
                 * Create an instance of the implementation.
                 * @return created instance
                 */
                virtual component_ref create() const = 0;
            protected:
                void register_implementation(
                        implementation_registry_entry_base *implementation);
            };

        }

        /**
         * Base class of all objects created as components.
         */
        class AGGE_CORE_EXPORT component
        {
        protected:
            component();
        public:
            virtual ~component();

            /**
             * Creates a reference to an component type @c T by
             * instantiating the implementation identified by the name
             * argument.
             * @param impl_name name of implementation to
             * instantiate
             * @return created instance, or invalid reference if the
             * component or implementation is not found.
             */
            template <typename T>
            static inline ::std::shared_ptr<T> create(const std::string& impl_name)
            {
                return ::std::dynamic_pointer_cast<T, component>(
                        create(system::get_type_name(typeid(T)), impl_name));
            }

            /**
             * Create an instance.
             * @param component_name name of the component to
             * instantiate
             * @param impl_name name (or alias name) of the
             * implementation
             * @return created instance, or invalid reference if the
             * component or implementation is not found.
             */
            static component_ref create(const std::string& component_name,
                                        const std::string& impl_name);

            /**
             * Get implementations of one interface.
             * @param component_name component name
             * @param implementations vector filled with known implementations
             */
            static void get_implementations(const std::string& component_name,
                                            std::vector<std::string>& implementations);

            /**
             * Get implementations of one interface.
             * @param implementations vector filled with known implementations
             */
            template <typename T>
            static inline void get_implementations(
                    std::vector<std::string>& implementations)
            {
                get_implementations(system::get_type_name(typeid(T)), implementations);
            }

            /**
             * Return whether a specific component is known.
             * @param component_name name of component
             * @return @c true if known component
             */
            static bool is_component_registered(const std::string& component_name);

            /**
             * Return whether an interfaces or alias name is known.
             * @param component_name name of component
             * @param implementation_name name of implementation
             * @return @c true if known implementation
             */
            static bool is_implementation_registered(
                    const std::string& component_name,
                    const std::string& implementation_name);

        private:
            /**
             * @internal Register component.
             * @param component component to register
             */
            static void register_component(
                    const internal::component_registry_entry_base* component);

            /**
             * @internal Register implementation.
             * @param component comp to register
             */
            static void register_implementation(
                    internal::implementation_registry_entry_base* component);

            friend class internal::component_registry_entry_base;
            friend class internal::implementation_registry_entry_base;
        };

        namespace internal {

            template <typename C>
            class component_registry_entry: public internal::component_registry_entry_base
            {
            public:
                component_registry_entry()
                        : m_name(system::get_type_name(typeid(C)))
                {
                    static_assert(
                            (::std::is_convertible<C*, ::agge::core::component*>::value),
                            "Component class is not derived from agge::core::component");
                    register_component(this);
                }

                virtual ~component_registry_entry()
                {
                }

                virtual const char *get_name() const
                {
                    return m_name.c_str();
                }

            private:
                std::string m_name;
            };

            template <typename T, typename C>
            class implementation_registry_entry: public internal::implementation_registry_entry_base
            {
            public:
                implementation_registry_entry(const char *alias_names)
                        : m_component_name(system::get_type_name(typeid(C))),
                          m_name(system::get_type_name(typeid(T))),
                          m_alias_names(alias_names)
                {
                    static_assert((::std::is_base_of<C, T>::value),
                                  "Implementation is not derived from base class");
                    static_assert(
                            (::std::is_convertible<C*, ::agge::core::component*>::value),
                            "Component is not derived from agge::core::component");
                    register_implementation(this);
                }

                virtual ~implementation_registry_entry()
                {
                }

                virtual const char *get_component_name() const
                {
                    return m_component_name.c_str();
                }

                virtual const char *get_name() const
                {
                    return m_name.c_str();
                }

                virtual const char *get_alias_names() const
                {
                    return m_alias_names;
                }

                virtual component_ref create() const
                {
                    return std::make_shared<T>();
                }

            private:
                std::string m_component_name;
                std::string m_name;
                const char *m_alias_names;
            };

        }

/**
 * Register a component.
 * @param clazz component class to be registered
 */
#define AGGE_REGISTER_COMPONENT(clazz)                      \
::agge::core::internal::component_registry_entry<clazz>     \
__agge_core_component_registry_entry##clazz

/**
 * Register an implementation.
 * @param clazz implementation class
 * @param component implemented component
 * @param aliasnames alias names for implementation
 */
#define AGGE_REGISTER_IMPLEMENTATION_ALIAS(clazz, component, aliasnames)       \
    AGGE_REGISTER_IMPLEMENTATION_ALIAS_1(clazz, component, #aliasnames, __LINE__)

#define AGGE_REGISTER_IMPLEMENTATION_ALIAS_1(clazz, component, aliasnames_str, line)  \
    ::agge::core::internal::implementation_registry_entry<clazz, component>           \
    __agge_core_implementation_registry_entry##clazz##line( aliasnames_str )

/**
 * Create an instance.
 * @param component_      component
 * @param implementation implementation name
 * @return created instance or invalid reference
 */
#define AGGE_CREATE_INSTANCE(component_, implementation) \
    ::agge::core::component::create<component_>(implementation)

#define AGGE_GET_IMPLEMENTATIONS(component_, implementations) \
    ::agge::core::component::get_implementations<component_>(implementations)
    }
}

#endif
