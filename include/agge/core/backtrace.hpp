/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_BACKTRACE_HPP
#define AGGE_CORE_BACKTRACE_HPP

#include "agge/core/globals.hpp"
#include "agge/core/dllexport.hpp"

#include "agge/core/backtrace_element.hpp"
#include <vector>

namespace agge {
    namespace core {

        AGGE_DECLARE_REF(backtrace);

        /**
         * A stack backtrace.
         *
         * Not all platforms support a stack backtrace,
         * use @c is_supported() to determine whether a stack backtrace can
         * be created.
         */
        class AGGE_CORE_EXPORT backtrace : public std::vector<backtrace_element>
        {
        private:
            typedef std::vector<backtrace_element> base_type;
        public:
            /**
             * Whether we have backtrace on this platform.
             * @return @c true if backtrace is populated
             */
            static bool is_supported();

            backtrace();
            backtrace(const backtrace& b);
            backtrace(backtrace&& b);

            ~backtrace();

            bool contains(const char *methodname) const;

            backtrace& operator =(const backtrace& b);

            static void dump(std::ostream& os);


        private:
            void fill();
        };

        AGGE_CORE_EXPORT std::ostream& operator <<(std::ostream& os,
                                                   const backtrace& bt);

    }
}
#endif
