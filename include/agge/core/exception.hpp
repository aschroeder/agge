/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_EXCEPTION_HPP
#define AGGE_CORE_EXCEPTION_HPP

/**
 * @file   exception.hpp
 * @brief  Exception class.
 */

#include <exception>
#include <string>
#include <sstream>

#include "agge/core/setup.hpp"
#include "agge/core/backtrace.hpp"

namespace agge {
    namespace core {

        class system_error;

        /**
         * An exception. An object of an exception class is thrown to
         * signal an exceptional situation.
         *
         * To throw an exception, you should use the @c AGGE_EXCEPTION
         * macro. The @c << operator defined on the exception class can be
         * used to fill the message text of an exception.
         *
         * An exception message should be a complete sentence, and start with
         * a capital letter.
         */
        class AGGE_CORE_EXPORT exception: virtual public std::exception
        {
        public:
            struct backtrace_tag {};

            static backtrace_tag backtrace;

            /**
             * Default constructor.
             */
            exception();

            /**
             * Constructor.
             * @param file source code file
             * @param line source code line
             */
            exception(const char *file, int line);

            /**
             * Copy constructor.
             * @param e copied exception
             */
            exception(const exception& e);

            /**
             * Destructor.
             */
            virtual ~exception() throw();

            /**
             * Assignment operator.
             * @param e exception to assign
             * @return @c *this
             */
            exception& operator =(const exception& e);

            /**
             * Get the source line number.
             * @return source file line
             */
            int get_line() const;

            /**
             * Get the source file name of the exception.
             * @return source file name
             */
            const std::string& get_file() const;

            /**
             * Get the exception message.
             * @return exception message
             */
            const std::string get_message() const;

            /**
             * Operator for composing the exception message.
             * @param t object to trace into message
             * @return @c *this
             */
            template <typename T>
            exception& operator <<(const T& t)
            {
                std::stringstream ss;
                ss << t;
                add_argument(ss.str());
                return *this;
            }

            /**
             * Helper operator to store backtrace in an exception.
             * @param tag backtrace tag, use @c agge::core::exception::backtrace
             * @return @c *this
             */
            exception& operator <<(const backtrace_tag& tag);

            /**
             * Accept a wide character string. Only ASCII representation is used in
             * message.
             * @param str string to add
             * @return @c *this
             */
            inline exception& operator <<(const std::wstring& str)
            {
                std::stringstream ss;
                for(wchar_t c : str) {
                    if (c < 255) {
                        ss << c;
                    } else {
                        ss << '?';
                    }
                }
                add_argument(ss.str());
                return *this;
            }

            /**
             * Access the stored backtrace.
             * @return stored backtrace, may be a nil reference
             */
            backtrace_ref get_backtrace() const;


            /**
             * Throws a @c system_exception describing the current system
             * error.
             * @param file source file
             * @param line source line
             * @param check whether simply to return if no system error is set
             */
            static void system_error(const char *file, const int line, bool check);

            /**
             * Throws a @c system_exception describing the current system
             * error.
             * @param function function that was called
             * @param file source file
             * @param line source line
             * @param check whether simply to return if no system error is set
             */
            static void system_call_error(const char *function, const char *file, const int line, bool check);

            /**
             * Access the last exception.
             * @return pointer to last constructed exception (or null pointer
             *         if none exist)
             */
            static const exception *get_last_exception();

            /**
             * Get exception message (overwrites std::exception's method).
             */
            virtual const char *what() const throw();

        private:
            int m_line;    //!< Source code line.
            std::string m_file;    //!< Source file name.
            mutable std::string m_message; //!< Materialized message.
            std::stringstream m_message_stream;  //!< Stream for message.
            mutable bool m_stream_modified; //!< Stream modification flag.
            backtrace_ref m_backtrace;
            static const exception* s_last_exception; //!< Last constructed exception.

            /// Install terminate and unexpected exception handlers.
            static void install_handlers();
            /// Materialize message from stream into cached string.
            void materialize_message() const;
            /// Add an argument to the stream.
            void add_argument(const std::string& argument);
        };

        /**
         * Dump an exception.
         * @param o output stream
         * @param e exception
         * @return @c o
         */
        AGGE_CORE_EXPORT std::ostream& operator <<(std::ostream& o, const exception& e);

        /**
         * Creates an instance of @c clazz,
         * initialized with the current source code line and file name,
         * to assist with exception creation.
         * @param clazz exception class to instantiate
         */
        #define AGGE_EXCEPTION(clazz) clazz(__FILE__, __LINE__)

        /**
         * Throw an exception.
         * @param clazz exception class to use
         */
        #define AGGE_THROW(clazz) throw AGGE_EXCEPTION(clazz)

        /**
         * Throws a system error.
         */
        #define AGGE_THROW_SYSTEMERROR ::agge::core::exception::system_error(__FILE__, __LINE__, false)

        /**
         * Throws a system error with information on called function.
         * @param call function that has failed
         */
        #define AGGE_THROW_SYSCALL_ERROR(call) ::agge::core::exception::system_call_error(#call, __FILE__, __LINE__, false)
    } // core
} // agge

#endif
