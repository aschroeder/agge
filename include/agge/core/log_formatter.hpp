/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_LOG_FORMATTER_HPP
#define AGGE_CORE_LOG_FORMATTER_HPP

#include "agge/core/log_record.hpp"
#include "agge/core/component.hpp"

namespace agge {
    namespace core {

        /// Reference to log formatter.
        AGGE_DECLARE_REF(log_formatter);

        /**
         * A log formatter formats published log records.
         */
        class AGGE_CORE_EXPORT log_formatter: public component
        {
        public:
            /**
             * Constructor.
             */
            log_formatter();

            /**
             * Destructor.
             */
            virtual ~log_formatter();

            /**
             * Formats the record
             * @param stream output stream
             * @param record log record
             */
            virtual void format(std::ostream& stream,
                                const log_record& record) = 0;
        };

    }
}

#endif
