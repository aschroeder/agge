/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_LOG_HPP
#define AGGE_CORE_LOG_HPP

#include "agge/core/setup.hpp"
#include "agge/core/configuration.hpp"
#include "agge/core/log_severity.hpp"
#include "agge/core/log_record.hpp"
#include "agge/core/log_handler.hpp"
#include "agge/core/log_repr.hpp"

#include <sstream>
#include <set>
#include <map>

namespace agge {
namespace core {

class memory_log_handler;

/**
 * Log.
 */
class AGGE_CORE_EXPORT log
{
public:
    struct flush_tag
    {
    };

    /// Tag used to signal log flush.
    static flush_tag flush;

    /**
     * Constructor.
     */
    log();

    /**
     * Destructor.
     */
    ~log();

    /**
     * Check whether log in category and severity is enabled.
     * @param category category to check
     * @param severity severity to check
     * @return @c true if log enabled
     */
    static bool is_enabled(const char *category, const log_severity& severity);

    /**
     * Check whether log in category and severity is enabled.
     * @param category category to check
     * @param severity severity to check
     * @return @c true if log enabled
     */
    static bool is_enabled(const std::string& category,
                           const log_severity& severity);

    /**
     * Start a log entry.
     * @param category log category
     * @param severity log entry severity
     * @return @c this
     */
    static log& log_entry(const char *category, const log_severity& severity);

    /**
     * Operator to flush log entry.
     * @param tag flush tag
     * @return @c *this
     */
    inline log& operator <<(const log::flush_tag&)
    {
        flush_entry();
        return *this;
    }

    template <typename T>
    log& operator <<(const T& value)
    {
        m_current_entry_stream << log_repr(value);
        return *this;
    }

private:
    /// Status of logging configuration process.
    enum config_status
    {
        CONFIG_STATUS_INITIAL,     //!< no configuration done
        CONFIG_STATUS_CONFIGURING, //!< in process of configuration
        CONFIG_STATUS_CONFIGURED   //!< log configured
    };

    /**
     * Get current configuration status.
     * @return configuration status
     */
    config_status get_configuration_status() const;

    /**
     * Check whether a severity is globally enabled.
     * @param severity log severity to check
     * @return @c true if enabled
     */
    bool is_globally_enabled(const log_severity& severity) const;

    /**
     * Configure logging.
     */
    void configure();

    /**
     * Configures log levels from config section.
     * @param config configuration section
     */
    void configure_log_levels();

    /**
     * Configure logging of one category.
     * @param category category
     * @param levels   comma-separated string of levels to set
     */
    void configure_log_category(const std::string& category,
                                const std::string& levels);

    /**
     * Stores the default configuration.
     * @param config log configuration
     */
    void store_default_config(configuration& config);


    /**
     * Enables logging for category and level.
     * @param category category
     * @param level    level
     */
    void enable_log_level(const std::string& category,
                          const std::string& level);

    /**
     * Take all log records stored in startup handler into normal handlers.
     */
    void process_memory_log_records();

    /**
     * Access singleton instance.
     * @return log instance
     */
    static log& instance();

    /**
     * Flush current entry.
     * @param r record
     */
    void flush_entry();

    /**
     * Returns whether specific log is enabled for category and severity.
     * @param category checked category
     * @param severity checked severity
     * @return @c true if enabled
     */
    bool is_category_enabled(const std::string& category,
                             const log_severity& severity) const;

    /**
     * Publish log entry.
     */
    void publish(const log_record& r);

    typedef std::map<std::string, unsigned int> configuration_map;
    typedef std::vector<log_handler_ref> handler_collection;

    unsigned int m_global_level;    //!< global level (once configured)
    configuration_map m_category_levels; //!< levels of specific categories
    config_status m_config_status;   //!< status of config process
    memory_log_handler *m_startup_handler; //!< handler used until configured
    handler_collection m_handlers;        //!< configured handlers

    std::string m_current_category;
    log_severity* m_current_severity;
    system::timestamp m_current_timestamp;
    std::stringstream m_current_entry_stream;
};

/**
 * @def AGGE_LOG
 *
 * Get the requested log stream. The macro yields nothing if the log
 * is disabled.
 * @param category_ log category
 * @param level     log level
 * @return log stream if the log is enabled
 */
#define AGGE_LOG(category_, level)                      \
    if(!::agge::core::log::is_enabled(category_, level))\
        (void)0;                                        \
    else                                                \
        ::agge::core::log::log_entry(category_, level)

/**
 * @def AGGE_LOG_ENABLED
 *
 * Check whether log is enabled.
 * @param category category
 * @param level     log level
 * @return @c true if enabled
 */
#define AGGE_LOG_ENABLED(category_, level) ::agge::core::log::is_enabled(category_, level)

/**
 * @def AGGE_ERROR_LOG_ENABLED
 *
 * Check whether error log is enabled.
 * @param category category
 * @return @c true if enabled
 */
#define AGGE_ERROR_LOG_ENABLED(category) AGGE_LOG_ENABLED(#category, ::agge::core::log_severity::LOG_SEVERITY_ERROR())

/**
 * @def AGGE_INFO_LOG_ENABLED
 *
 * Check whether info log is enabled.
 * @param category category
 * @return @c true if enabled
 */
#define AGGE_INFO_LOG_ENABLED(category) AGGE_LOG_ENABLED(#category, ::agge::core::log_severity::LOG_SEVERITY_INFO())

/**
 * @def AGGE_DEBUG_LOG_ENABLED
 *
 * Check whether debug log is enabled.
 * @param category category
 * @return @c true if enabled
 */
#define AGGE_DEBUG_LOG_ENABLED(category) AGGE_LOG_ENABLED(#category, ::agge::core::log_severity::LOG_SEVERITY_DEBUG())

/**
 * @def AGGE_WARNING_LOG_ENABLED
 *
 * Check whether warning log is enabled.
 * @param category category
 * @return @c true if enabled
 */
#define AGGE_WARNING_LOG_ENABLED(category) AGGE_LOG_ENABLED(#category, ::agge::core::log_severity::LOG_SEVERITY_WARNING)

/**
 * @def AGGE_ERROR_LOG
 * Error log access.
 * @param category log category (will be stringified)
 * @return log stream
 */
#define AGGE_ERROR_LOG(category)   AGGE_LOG(#category, ::agge::core::log_severity::LOG_SEVERITY_ERROR())

/**
 * @def AGGE_INFO_LOG
 * Info log access.
 * @param category log category (will be stringified)
 * @return log stream
 */
#define AGGE_INFO_LOG(category)    AGGE_LOG(#category, ::agge::core::log_severity::LOG_SEVERITY_INFO())

/**
 * @def AGGE_WARNING_LOG
 * Error log access.
 * @param category log category (will be stringified)
 * @return log stream
 */
#define AGGE_WARNING_LOG(category) AGGE_LOG(#category, ::agge::core::log_severity::LOG_SEVERITY_WARNING())

/**
 * @def AGGE_DEBUG_LOG
 * Error log access.
 * @param category log category (will be stringified)
 * @return log stream
 */
#define AGGE_DEBUG_LOG(category)   AGGE_LOG(#category, ::agge::core::log_severity::LOG_SEVERITY_DEBUG())

/**
 * @def AGGE_LOG_FLUSH
 * Flushes current log entry.
 */
#define AGGE_LOG_FLUSH (::agge::core::log::flush)

}
}

#endif
