/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_CORE_IO_ERROR_HPP
#define AGGE_CORE_IO_ERROR_HPP

#include "agge/core/exception.hpp"

namespace agge {
    namespace core {

    /**
     * @class io_error
     * @brief Exception thrown if an input/output error occurs.
     */
    class AGGE_CORE_EXPORT io_error : public agge::core::exception
    {
    public:
        io_error();
        io_error(const io_error& e);
        io_error(const char *file, int line);
        virtual ~io_error();
        io_error& operator=(const io_error& e);

        template <typename T>
        io_error& operator <<(const T& t)
        {
            agge::core::exception::operator <<(t);
            return *this;
        }
    };

}
}

#endif
