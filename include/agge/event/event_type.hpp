/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_EVENT_EVENT_TYPE_HPP
#define AGGE_EVENT_EVENT_TYPE_HPP

#include "agge/event/dllexport.hpp"
#include <cstddef>

namespace agge {
    namespace event {

        class AGGE_EVENT_EXPORT event_type
        {
        public:
            event_type(const char *tag, unsigned int type, std::size_t size);
            ~event_type();

            const char *get_tag() const;
            unsigned int get_type() const;
            std::size_t get_size() const;
        private:
            static void register_event_type(event_type *et);
            static void unregister_event_type(event_type *et);

            const char *m_tag;
            unsigned int m_type;
            std::size_t m_size;
        };

/**
 * @def AGGE_DEFINE_EVENT_TYPE(TAG, NUMBER, CLAZZ)
 * Defines a new event type represented by a certain class.
 * @param TAG type tag (text)
 * @param NUMBER type number
 */
#define AGGE_DEFINE_EVENT_TYPE(TAG, NUMBER, CLAZZ)                         \
const AGGE_DLLEXPORT agge::event::event_type& get_EVENT_TYPE_##TAG()       \
{                                                                          \
    static ::agge::event::event_type EVENT_TYPE_##TAG(#TAG,                \
        NUMBER, sizeof(CLAZZ));                                            \
    return EVENT_TYPE_##TAG;                                               \
}

/**
 * @def Uses the event type locally (in same module as defined).
 * @param TAG type tag
 */
#define AGGE_USE_EVENT_TYPE_LOCAL(TAG)                           \
        const ::agge::event::event_type& get_EVENT_TYPE##TAG();  \
        static const agge::event::event_type& EVENT_TYPE_##TAG = get_EVENT_TYPE_##TAG()


    }
}

#endif

