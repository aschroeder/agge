/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_EVENT_EVENT_DISPATCHER_HPP
#define AGGE_EVENT_EVENT_DISPATCHER_HPP

#include "agge/event/dllexport.hpp"
#include "agge/core/globals.hpp"
#include "agge/event/event.hpp"

namespace agge {
    namespace event {

        AGGE_DECLARE_REF(event_dispatcher);

        /**
         * Dispatcher for events.
         */
        class AGGE_EVENT_EXPORT event_dispatcher
        {
        protected:
            /**
             * Constructor.
             */
            event_dispatcher();
        public:
            /**
             * Destructor.
             */
            virtual ~event_dispatcher();

            /**
             * Filter, check if triggered by event.
             * @param e event
             * @return @c true if triggered
             */
            virtual bool is_triggered(const event& e) const;

            /**
             * Dispatch event.
             * @param e event
             * @return @c true if it has been dispatched
             */
            virtual bool dispatch(const event& e) const;
        };
    }
}

#endif
