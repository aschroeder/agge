/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_EVENT_EVENT_SOURCES_HPP
#define AGGE_EVENT_EVENT_SOURCES_HPP

#include "dllexport.hpp"

#include <functional>
#include <vector>

namespace agge {
    namespace event {

        typedef std::function<void()> event_source;

        class AGGE_EVENT_EXPORT event_sources
        {
        public:
            static void fetch_events();
            static void add_source(const event_source& source);
            static void remove_source(const event_source& source);
        private:
            static event_sources& get();
            void do_fetch_events();
            void do_add_source(const event_source& source);
            void do_remove_source(const event_source& source);

            std::vector<event_source> m_sources;
        };
    }
}

#endif
