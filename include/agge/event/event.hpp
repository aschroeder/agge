/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_EVENT_EVENT_HPP
#define AGGE_EVENT_EVENT_HPP

#include "agge/event/dllexport.hpp"

#include <iostream>

namespace agge {
    namespace event {

        /**
         * Event base class.
         */
        class AGGE_EVENT_EXPORT event
        {
        public:
            /**
             * Constructor.
             */
            event();

            /**
             * Get event type.
             * @return event type
             */
            unsigned int get_type() const;

            /**
             * Get event category. This is special to the event type.
             *
             * @return event category
             */
            unsigned int get_category() const;

            /**
             * Get size of this event object.
             * @return size
             */
            size_t get_size() const;

            /**
             * Get size of this event object aligned properly.
             * @return aligned size
             */
            size_t get_aligned_size() const;

        protected:
            /**
             * Constructor.
             * @param type_     event type
             * @param category_ event category
             * @param size      object size
             */
            event(unsigned int type_, unsigned int category_, size_t size);

            /**
             * Copy constructor.
             * @param e copied event
             */
            event(const event& e);

            /**
             * Move constructor.
             * @param e moved event
             */
            event(event&& e);

            /**
             * Assignment
             * @param e assigned event
             * @return @c *this
             */
            event& operator =(const event& e);

            /**
             * Assignment
             * @param e assigned event
             * @return @c *this
             */
            event& operator =(event&& e);

        private:
            unsigned int m_type;     //!< event type
            unsigned int m_category;//!< event category
            size_t m_size;//!< structure size
        };

    }
}

#endif
