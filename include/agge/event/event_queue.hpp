/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_EVENT_EVENT_QUEUE_HPP
#define AGGE_EVENT_EVENT_QUEUE_HPP

#include "agge/core/setup.hpp"
#include "agge/event/dllexport.hpp"
#include "agge/event/event.hpp"

#include <iterator>
#include <vector>
#include <functional>

/// @file event_queue.hpp
/// @brief Event queue.

namespace agge {
    namespace event {

        class event_dispatcher;
        class event_source;

        /**
         * Event queue.
         */
        class AGGE_EVENT_EXPORT event_queue
        {
        public:
            /**
             * Insert iterator for event queue.
             */
            class AGGE_EVENT_EXPORT insert_iterator : public std::iterator<
                            std::output_iterator_tag, void, void, void, void>
            {
            public:
                /**
                 * Constructor.
                 * @param queue event queue affected by iterator
                 */
                insert_iterator(event_queue *queue);

                /**
                 * Destructor.
                 */
                ~insert_iterator();

                /**
                 * Assignment, adds event to event queue.
                 * @param e event to add
                 * @return @c *this
                 */
                insert_iterator& operator =(const event& e);

                /**
                 * Dereference iterator.
                 * @return @c *this
                 */
                insert_iterator& operator *();

                /**
                 * Increment, does nothing.
                 * @return @c *this
                 */
                insert_iterator& operator ++();

                /**
                 * Increment, does nothing.
                 * @return @c *this
                 */
                insert_iterator& operator ++(int);
            private:
                event_queue *m_queue; //!< event queue that is affected
            };

            typedef std::function<void (insert_iterator&)> event_source;

            /**
             * Get singleton instance.
             * @return event queue singleton
             */
            static event_queue& get();

            /**
             * Constructor.
             */
            event_queue();

            /**
             * Destructor.
             */
            ~event_queue();

            /**
             * Return whether the event queue is live, i.e. can produce new events.
             * @return @c true if the event queue has at least one event source
             *         registered.
             */
            bool is_live() const;

            /**
             * Return whether queue is empty.
             * @return @c true if empty
             */
            bool empty() const;

            /**
             * Add an event.
             * @param e event
             */
            void add(const event& e);

            /**
             * Check dispatcher registration.
             * @param dispatcher pointer to checked dispatcher
             * @return @c true if dispatcher is registered
             */
            bool is_dispatcher_registered(event_dispatcher *dispatcher) const;

            /**
             * Loop as long as being live and not being terminated.
             * @param terminate flag to check after loop for being terminated
             */
            void loop(bool& terminate);


            /**
             * Collect events.
             */
            void collect();

            /**
             * Dispatch events.
             */
            void dispatch();

            /**
             * Add a source.
             * @param source source that is added
             */
            void add_source(const event_source& source);

            /**
             * Remove event source.
             * @param source
             */
            void remove_source(const event_source& source);
        private:
            /**
             * Add dispatcher.
             * @param dispatcher dispatcher to add
             */
            void add(event_dispatcher * dispatcher);

            /**
             * Remove dispatcher.
             * @param dispatcher to remove
             */
            void remove(event_dispatcher *dispatcher);

            friend class event_dispatcher;

            /**
             * Clears the events.
             */
            void clear();


            std::vector<size_t> m_offsets;       //!< offsets in data
            std::vector<unsigned char> m_data;   //!< event data

            std::vector<event_source> m_sources;           //!< event sources
            std::vector<event_source> m_sources_to_remove; //!< sources to remove at end of collection

            std::vector<event_dispatcher*> m_dispatchers; //!< event dispatchers
        };
    }
}
#endif
