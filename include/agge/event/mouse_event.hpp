/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_EVENT_MOUSE_EVENT_HPP
#define AGGE_EVENT_MOUSE_EVENT_HPP

#include "agge/event/dllexport.hpp"
#include "agge/event/event.hpp"
namespace agge {
    namespace event {

        class AGGE_EVENT_EXPORT mouse_event : public event
        {
        public:
            enum category
            {
                MOUSE_PRESS = 1,
                MOUSE_RELEASE = 2,
                MOUSE_CLICK = 3,
                MOUSE_MOVE = 4,
                MOUSE_DRAG = 5
            };

            mouse_event(category c, unsigned int button, unsigned int x,
                        unsigned int y);
            mouse_event(const mouse_event& e);
            mouse_event(mouse_event&& e);

            mouse_event& operator =(const mouse_event& e);
            mouse_event& operator =(mouse_event&& e);

        private:
            unsigned int m_button;
            unsigned int m_x;
            unsigned int m_y;
        };
    }
}

#endif
