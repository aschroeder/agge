/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_TEST_OUTPUT_TEST_HPP
#define AGGE_TEST_OUTPUT_TEST_HPP

#include "agge/test/googletest.hpp"
#include <sstream>

namespace agge {
    namespace test {

        template <typename T>
        struct output_expectation
        {
            output_expectation(const T& input_, const char *expectation_)
            :input(input_),
             expectation(expectation_)
            {}

            T input;
            const char *expectation;
        };

        template <typename T>
        class output_test
                : public ::testing::TestWithParam<output_expectation<T> >
        {
        protected:
            void testOutput(const T& input, const char *expectation)
            {
                std::stringstream ss;
                ss << input;
                EXPECT_STREQ(expectation, ss.str().c_str());
            }
        };

    }
}

#endif
