/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_MATH_VEC2F_HPP
#define AGGE_MATH_VEC2F_HPP

#include "agge/core/config.hpp"
#include "agge/core/types.hpp"

#include "agge/math/dllexport.hpp"
#include "agge/math/vector_type.hpp"
#include "agge/math/vector_fwd.hpp"

#include <initializer_list>

namespace agge {
    namespace math {

        class AGGE_MATH_EXPORT vec2f : public vector_type<float, 2>
        {
        public:
            float x;
            float y;

            typedef float value_type;
            typedef std::size_t size_type;

            static const vec2f ZERO;
            static const vec2f X;
            static const vec2f Y;
            static const vec2f XY;

            vec2f();
            vec2f(agge::core::initialize_members_tag);
            vec2f(const vec2f& v);
            vec2f(vec2f&& v);
            explicit vec2f(float f);
            explicit vec2f(float x_, float y_);
            vec2f(const std::initializer_list<float>& l);
            vec2f(const float *f);

            vec2f& operator =(const vec2f& v);
            vec2f& operator =(vec2f&& v);

            float& operator[](size_type i);
            const float& operator[](size_type i) const;

            vec2f& operator +=(const vec2f& v);
            vec2f& operator -=(const vec2f& v);

            template<typename S>
            inline vec2f& operator *=(const S& scalar)
            {
                x *= scalar;
                y *= scalar;
                return *this;
            }

            template<typename S>
            inline vec2f& operator /=(const S& scalar)
            {
                x /= scalar;
                y /= scalar;
                return *this;
            }

            vec2f operator -() const;

            inline iterator begin()
            {
                return &x;
            }

            inline iterator end()
            {
                return &y + 1;
            }

            inline const_iterator begin() const
            {
                return &x;
            }

            inline const_iterator end() const
            {
                return &y + 1;
            }


        };

        AGGE_MATH_EXPORT std::ostream& operator <<(std::ostream& os,
                                                   const vec2f& v);

        AGGE_MATH_EXPORT vec2f operator +(const vec2f& a, const vec2f& b);
        AGGE_MATH_EXPORT vec2f operator -(const vec2f& a, const vec2f& b);

        template<typename S>
        inline vec2f operator *(const vec2f& a, const S& scalar)
        {
            vec2f r(a);
            r *= scalar;
            return r;
        }

        template<typename S>
        inline vec2f operator /(const vec2f& a, const S& scalar)
        {
            vec2f r(a);
            r /= scalar;
            return r;
        }

        AGGE_MATH_EXPORT bool operator ==(const vec2f& a, const vec2f& b);
        AGGE_MATH_EXPORT bool operator !=(const vec2f& a, const vec2f& b);

    }
}

#endif
