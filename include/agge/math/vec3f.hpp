/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_MATH_VEC3F_HPP
#define AGGE_MATH_VEC3F_HPP

#include "agge/core/config.hpp"
#include "agge/core/types.hpp"

#include "agge/math/dllexport.hpp"
#include "agge/math/vector_type.hpp"
#include "agge/math/vector_fwd.hpp"

#include <initializer_list>
#include <iostream>

namespace agge {
    namespace math {

        class AGGE_MATH_EXPORT vec3f : public vector_type<float, 3>
        {
        public:
            float x;
            float y;
            float z;

            typedef float value_type;
            typedef std::size_t size_type;

            static const vec3f ZERO;
            static const vec3f X;
            static const vec3f Y;
            static const vec3f Z;
            static const vec3f XYZ;

            vec3f();
            vec3f(agge::core::initialize_members_tag);
            vec3f(const vec3f& v);
            vec3f(vec3f&& v);
            explicit vec3f(float f);
            explicit vec3f(float x_, float y_, float z_);
            vec3f(const std::initializer_list<float>& l);
            vec3f(const float *f);

            vec3f& operator =(const vec3f& v);
            vec3f& operator =(vec3f&& v);

            float& operator[](size_type i);
            const float& operator[](size_type i) const;

            vec3f& operator +=(const vec3f& v);
            vec3f& operator -=(const vec3f& v);

            template<typename S>
            inline vec3f& operator *=(const S& scalar)
            {
                x *= scalar;
                y *= scalar;
                z *= scalar;
                return *this;
            }

            template<typename S>
            inline vec3f& operator /=(const S& scalar)
            {
                x /= scalar;
                y /= scalar;
                z /= scalar;
                return *this;
            }

            vec3f operator -() const;

            inline iterator begin()
            {
                return &x;
            }

            inline iterator end()
            {
                return &z + 1;
            }

            inline const_iterator begin() const
            {
                return &x;
            }

            inline const_iterator end() const
            {
                return &z + 1;
            }
        };

        AGGE_MATH_EXPORT std::ostream& operator <<(std::ostream& os, const vec3f& v);

        AGGE_MATH_EXPORT vec3f operator +(const vec3f& a, const vec3f& b);
        AGGE_MATH_EXPORT vec3f operator -(const vec3f& a, const vec3f& b);

        template<typename S>
        inline vec3f operator *(const vec3f& a, const S& scalar)
        {
            vec3f r(a);
            r *= scalar;
            return r;
        }

        template<typename S>
        inline vec3f operator /(const vec3f& a, const S& scalar)
        {
            vec3f r(a);
            r /= scalar;
            return r;
        }

        AGGE_MATH_EXPORT bool operator ==(const vec3f& a, const vec3f& b);
        AGGE_MATH_EXPORT bool operator !=(const vec3f& a, const vec3f& b);



    }
}

#endif
