/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_MATH_VECTOR_TYPE_HPP
#define AGGE_MATH_VECTOR_TYPE_HPP

#include <cstddef>
#include <iterator>

namespace agge {
    namespace math {

        /**
         * Base class for all vector types, which delivers
         * meta information on the vector.
         */
        template <typename T, std::size_t C>
        class vector_type
        {
        public:
            typedef T value_type;
            typedef T& reference;
            typedef const T& const_reference;
            typedef T* pointer;
            typedef const T* const_pointer;
            typedef std::size_t size_type;
            typedef std::ptrdiff_t difference_type;

            typedef T* iterator;
            typedef const T* const_iterator;
            typedef std::reverse_iterator<iterator> reverse_iterator;
            typedef std::reverse_iterator<const_iterator> reverse_const_iterator;


            size_type size() const
            {
                return C;
            }

            size_type max_size() const
            {
                return C;
            }

            bool empty() const
            {
                return false;
            }

        };
    }
}

#endif
