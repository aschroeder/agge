/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_MATH_MATRIX_FWD_HPP
#define AGGE_MATH_MATRIX_FWD_HPP

namespace agge {
    namespace math {
        class mat2x2f;
        class mat2x3f;
        class mat2x4f;

        class mat3x2f;
        class mat3x3f;
        class mat3x4f;

        class mat4x2f;
        class mat4x3f;
        class mat4x4f;

        class mat2x2d;
        class mat2x3d;
        class mat2x4d;

        class mat3x2d;
        class mat3x3d;
        class mat3x4d;

        class mat4x2d;
        class mat4x3d;
        class mat4x4d;

        class mat2x2i;
        class mat2x3i;
        class mat2x4i;

        class mat3x2i;
        class mat3x3i;
        class mat3x4i;

        class mat4x2i;
        class mat4x3i;
        class mat4x4i;
    }
}

#endif
