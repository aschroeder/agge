/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_MATH_VEC4F_HPP
#define AGGE_MATH_VEC4F_HPP

#include "agge/core/config.hpp"
#include "agge/core/types.hpp"

#include "agge/math/dllexport.hpp"
#include "agge/math/vector_type.hpp"
#include "agge/math/vector_fwd.hpp"

#include <initializer_list>
#include <iostream>

namespace agge {
    namespace math {

        class AGGE_MATH_EXPORT vec4f : public vector_type<float, 3>
        {
        public:
            float x;
            float y;
            float z;
            float w;

            typedef float value_type;
            typedef std::size_t size_type;

            static const vec4f ZERO;
            static const vec4f X;
            static const vec4f Y;
            static const vec4f Z;
            static const vec4f W;
            static const vec4f XYZW;

            vec4f();
            vec4f(agge::core::initialize_members_tag);
            vec4f(const vec4f& v);
            vec4f(vec4f&& v);
            explicit vec4f(float f);
            explicit vec4f(float x_, float y_, float z_, float w_);
            vec4f(const std::initializer_list<float>& l);
            vec4f(const float *f);

            vec4f& operator =(const vec4f& v);
            vec4f& operator =(vec4f&& v);

            float& operator[](size_type i);
            const float& operator[](size_type i) const;

            vec4f& operator +=(const vec4f& v);
            vec4f& operator -=(const vec4f& v);

            template<typename S>
            inline vec4f& operator *=(const S& scalar)
            {
                x *= scalar;
                y *= scalar;
                z *= scalar;
                w *= scalar;
                return *this;
            }

            template<typename S>
            inline vec4f& operator /=(const S& scalar)
            {
                x /= scalar;
                y /= scalar;
                z /= scalar;
                w /= scalar;
                return *this;
            }

            vec4f operator -() const;

            inline iterator begin()
            {
                return &x;
            }

            inline iterator end()
            {
                return &w + 1;
            }

            inline const_iterator begin() const
            {
                return &x;
            }

            inline const_iterator end() const
            {
                return &w + 1;
            }
        };

        AGGE_MATH_EXPORT std::ostream& operator <<(std::ostream& os, const vec4f& v);

        AGGE_MATH_EXPORT vec4f operator +(const vec4f& a, const vec4f& b);
        AGGE_MATH_EXPORT vec4f operator -(const vec4f& a, const vec4f& b);

        template<typename S>
        inline vec4f operator *(const vec4f& a, const S& scalar)
        {
            vec4f r(a);
            r *= scalar;
            return r;
        }

        template<typename S>
        inline vec4f operator /(const vec4f& a, const S& scalar)
        {
            vec4f r(a);
            r /= scalar;
            return r;
        }

        AGGE_MATH_EXPORT bool operator ==(const vec4f& a, const vec4f& b);
        AGGE_MATH_EXPORT bool operator !=(const vec4f& a, const vec4f& b);

    }
}

#endif
