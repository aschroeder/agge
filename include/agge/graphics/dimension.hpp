/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_GRAPHICS_DIMENSION_HPP
#define AGGE_GRAPHICS_DIMENSION_HPP

#include <initializer_list>

#include "agge/core/stdexceptions.hpp"

namespace agge {
    namespace graphics {

        /**
         * A dimension, that is, a position information and a width and height.
         */
        class dimension
        {
        public:
            /**
             * Default constructor.
             */
            inline dimension()
                            : m_x(0), m_y(0), m_width(0), m_height(0)
            {
            }

            /**
             * Copy constructor.
             * @param d copied dimension
             */
            dimension(const dimension& d)
                            : m_x(d.m_x),
                              m_y(d.m_y),
                              m_width(d.m_width),
                              m_height(d.m_height)
            {
            }

            /**
             * Construct from explicit values.
             * @param x      x position
             * @param y      y position
             * @param width  element width
             * @param height element height
             */
            dimension(int x, int y, int width, int height)
                            : m_x(x), m_y(y), m_width(width), m_height(height)
            {
            }

            /**
             * Construct using width and height. Position will be (0,0).
             * @param width element width
             * @param height element height
             */
            dimension(int width, int height)
                            : m_x(0), m_y(0), m_width(width), m_height(height)
            {
            }

            /**
             * Construct from initializer list, accepts either 2 values (only width
             * and height or 4 values (including position).
             * @param l initializer list of 2 or 4 values
             * @throws @c agge::core::illegal_argument if list size is not 2 or 4
             */
            dimension(const std::initializer_list<int>& l)
            {
                if (l.size() == 2) {
                    m_x = m_y = 0;
                    std::initializer_list<int>::const_iterator i = l.begin();
                    m_width = *i;
                    ++i;
                    m_height = *i;
                } else if (l.size() == 4) {
                    std::initializer_list<int>::const_iterator i = l.begin();
                    m_x = *i;
                    ++i;
                    m_y = *i;
                    ++i;
                    m_width = *i;
                    ++i;
                    m_height = *i;
                    ++i;
                } else {
                    AGGE_THROW(agge::core::illegal_argument)
                        << "Dimension cannot be initialized with list of size "
                        << l.size() << ".";
                }
            }

            /**
             * Assignment.
             * @param d assigned dimension
             * @return @c *this
             */
            inline dimension& operator =(const dimension& d)
            {
                m_x = d.m_x;
                m_y = d.m_y;
                m_width = d.m_width;
                m_height = d.m_height;
                return *this;
            }

            /**
             * Access width.
             * @return width
             */
            inline int get_width() const
            {
                return m_width;
            }

            /**
             * Access height.
             * @return height
             */
            inline int get_height() const
            {
                return m_height;
            }

            /**
             * Access x position.
             * @return x
             */
            inline int get_x() const
            {
                return m_x;
            }

            /**
             * Access y position.
             * @return y
             */
            inline int get_y() const
            {
                return m_y;
            }

            /**
             * Get left corner position.
             * @return left corner
             */
            inline int get_left() const
            {
                return m_x;
            }

            /**
             * Get right corner position.
             * @return right corner
             */
            inline int get_right() const
            {
                return m_x + m_width;
            }

            /**
             * Get top corner position.
             * @return top corner
             */
            inline int get_top() const
            {
                return m_y;
            }

            /**
             * Get bottom corner position.
             * @return bottom corner
             */
            inline int get_bottom() const
            {
                return m_y + m_height;
            }

            /**
             * Comparison operator.
             * @param d compared dimension
             * @return @c true if equal
             */
            inline bool operator ==(const dimension& d) const
            {
                return m_x == d.m_x && m_y == d.m_y && m_width == d.m_width
                       && m_height == d.m_height;
            }

            /**
             * Comparison operator.
             * @param d compared dimension
             * @return @c true if not equal
             */
            inline bool operator !=(const dimension& d) const
            {
                return !operator ==(d);
            }

        private:
            int m_x;
            int m_y;
            int m_width;
            int m_height;
        };

    }
}

#endif
