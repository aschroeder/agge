/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_GRAPHICS_MONITOR_HPP
#define AGGE_GRAPHICS_MONITOR_HPP

#include "dllexport.hpp"
#include "agge/core/globals.hpp"
#include "agge/graphics/dimension.hpp"
#include "agge/graphics/video_mode.hpp"

#include <string>
#include <vector>

namespace agge {
    namespace graphics {

        /**
         * Reference to monitor.
         */
        AGGE_DECLARE_REF(monitor);


        class AGGE_GRAPHICS_EXPORT monitor
        {
        public:
            typedef std::vector<video_mode> video_mode_list;

            virtual ~monitor();

            virtual const dimension& get_dimension() const;
            virtual bool is_primary() const;
            virtual const std::string& get_name() const;
            virtual video_mode_list get_video_modes() const;
        protected:
            monitor();
        };
    }
}

#endif
