/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_GRAPHICS_RGB_COLOR_HPP
#define AGGE_GRAPHICS_RGB_COLOR_HPP

/**
 * @file   rgb_color.hpp
 * @brief  RGB color.
 */

#include <iostream>
#include "dllexport.hpp"

namespace agge {
    namespace graphics {

        /**
         * An RGB color.
         */
        class AGGE_GRAPHICS_EXPORT rgb_color
        {
        public:
            /**
             * Constructor. Intializes to zero.
             */
            inline rgb_color()
                            : r(0.0f), g(0.0f), b(0.0f)
            {
            }

            /**
             * Constructor.
             * @param rgbvalue rgb value (like 0xFFAAEE)
             */
            rgb_color(unsigned int rgbvalue);

            /**
             * Copy constructor.
             * @param c copied color value
             */
            inline rgb_color(const rgb_color& c) = default;

            /**
             * Create RGB color from color name.
             * @param name color name
             */
            rgb_color(const char *name);

            /**
             * Construct (greyscale) color from single value.
             * @param value color value
             */
            inline rgb_color(const float value)
                            : r(value), g(value), b(value)
            {
            }

            /**
             * Creates a color
             * @param r_ red
             * @param g_ green
             * @param b_ blue
             */
            inline rgb_color(float r_, float g_, float b_)
                            : r(r_), g(g_), b(b_)
            {
            }

            /**
             * Initialize from array.
             * @param values color value array
             */
            inline rgb_color(const float *values)
                            : r(values[0]), g(values[1]), b(values[2])
            {
            }

            /**
             * Assignment operator.
             * @param c color to assign.
             * @return @c *this
             */
            rgb_color& operator =(const rgb_color& c) = default;

            /**
             * Set red component,
             * @param r_ red value
             */
            void set_red(const float r_)
            {
                r = r_;
                return;
            }

            /**
             * Set green component.
             * @param g_ green component
             */
            void set_green(const float g_)
            {
                g = g_;
            }

            /**
             * Set blue component.
             * @param b_ blue component
             */
            void set_blue(const float b_)
            {
                b = b_;
            }

            /**
             * Get red component.
             * @return red component
             */
            float get_red() const
            {
                return r;
            }

            /**
             * Get green component.
             * @return green component
             */
            float get_green() const
            {
                return g;
            }

            /**
             * Get blue component.
             * @return blue component
             */
            float get_blue() const
            {
                return b;
            }

            /**
             * Comparison operator.
             * @param c compared color
             * @return @c true if this color equals @c c
             */
            bool operator ==(const rgb_color& c) const;

            /**
             * Comparison operator.
             * @param c compared color
             * @return @c true if this color is different from @c c
             */
            bool operator !=(const rgb_color& c) const;

            float r; //!< red
            float g; //!< green
            float b; //!< blue
        };

        /**
         * Stream dump operator.
         * @param o output stream
         * @param c color to dump
         * @return @c o
         */
        AGGE_GRAPHICS_EXPORT std::ostream& operator <<(std::ostream& o,
                                                       const rgb_color& c);

    } // core
} // agge

#endif
