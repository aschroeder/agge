/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_GRAPHICS_VIDEO_MODE_HPP
#define AGGE_GRAPHICS_VIDEO_MODE_HPP

#include "agge/graphics/dllexport.hpp"

namespace agge {
    namespace graphics {

        class AGGE_GRAPHICS_EXPORT video_mode
        {
        public:
            video_mode();
            video_mode(unsigned int width, unsigned int height,
                       unsigned int red_bits, unsigned int green_bits,
                       unsigned int blue_bits, unsigned int refresh_rate);
            video_mode(const video_mode&) = default;
            video_mode& operator =(const video_mode&) = default;
            ~video_mode();

        private:
            unsigned int m_width;
            unsigned int m_height;
            unsigned int m_red_bits;
            unsigned int m_green_bits;
            unsigned int m_blue_bits;
            unsigned int m_refresh_rate;
        };

    }
}

#endif
