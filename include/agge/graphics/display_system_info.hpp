/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_GRAPHICS_DISPLAY_SYSTEM_INFO_HPP
#define AGGE_GRAPHICS_DISPLAY_SYSTEM_INFO_HPP

#include "agge/core/globals.hpp"
#include "agge/core/noncopyable.hpp"
#include "agge/graphics/dllexport.hpp"

#include <string>

namespace agge {
    namespace graphics {

        class AGGE_GRAPHICS_EXPORT display_system_info
        {
        public:
            display_system_info() = default;
            display_system_info(const std::string& api,
                                const std::string& version,
                                const std::string& gpu_vendor,
                                const std::string& gpu_product);
            display_system_info(const display_system_info&) = default;
            display_system_info& operator =(const display_system_info&) = default;
            ~display_system_info();

            const std::string& get_api() const;
            const std::string& get_version() const;
            const std::string& get_gpu_vendor() const;
            const std::string& get_gpu_product() const;

        private:
            std::string m_api;
            std::string m_version;
            std::string m_gpu_vendor;
            std::string m_gpu_product;
        };

    }
}

#endif
