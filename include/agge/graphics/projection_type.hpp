/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_GRAPHICS_PROJECTION_TYPE_HPP
#define AGGE_GRAPHICS_PROJECTION_TYPE_HPP

#include "dllexport.hpp"

#include <iostream>

namespace agge {
    namespace graphics {
        /**
         * Projection type form camera.
         */
        enum class AGGE_GRAPHICS_EXPORT projection_type
        {
            PERSPECTIVE,    //!< perspective projection
            ORTHOGRAPHIC,   //!< orthographic projection
        };

        /**
         * Stream output operator.
         * @param os stream
         * @param p projection type
         * @return @c os
         */
        AGGE_GRAPHICS_EXPORT std::ostream& operator <<(
                        std::ostream& os, const projection_type& p);
    }
}

#endif
