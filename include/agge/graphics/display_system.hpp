/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_GRAPHICS_DISPLAY_SYSTEM_HPP
#define AGGE_GRAPHICS_DISPLAY_SYSTEM_HPP

#include "agge/graphics/dllexport.hpp"
#include "agge/core/component.hpp"
#include "agge/graphics/dimension.hpp"
#include "agge/graphics/display_system_info.hpp"
#include "agge/graphics/render_device.hpp"
#include "agge/graphics/monitor.hpp"
#include "agge/graphics/window.hpp"

namespace agge {
    namespace graphics {

        AGGE_DECLARE_REF(display_system);

        /**
         * Display system. The display system is the root context to create
         * windows.
         */
        class AGGE_GRAPHICS_EXPORT display_system : public agge::core::component
        {
        protected:
            display_system();
        public:
            virtual ~display_system();

            /**
             * Get display system info.
             * @return display system info
             */
            virtual const display_system_info& get_system_info() const;

            /**
             * Get the primary monitor.
             * @return primary monitor
             */
            virtual monitor_ref get_primary_monitor() const;

            /**
             * Creates a new window.
             * @param d window position and size
             * @param title window title
             * @param fullscreen whether window is fullscreen
             */
            virtual window_ref create_window(const dimension& d,
                                             const std::string& title,
                                             bool fullscreen);

            /**
             * Create off-screen render device.
             * @param dimension size of device
             * @return render device
             */
            virtual render_device_ref create_render_device(const dimension& d);

            /**
             * Create the configured display system.
             * @return reference to configured display system
             */
            static display_system_ref create();

            /**
             * Create specific implementation of display system.
             * @param implementation name of implementation
             * @return created display system
             */
            static display_system_ref create(const char *implementation);

        };
    }
}

#endif
