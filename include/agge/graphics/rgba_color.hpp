/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_GRAPHICS_RGBA_COLOR_HPP
#define AGGE_GRAPHICS_RGBA_COLOR_HPP

/**
 * @file   rgba_color.hpp
 * @brief  RGBA color.
 */

#include <iostream>
#include "dllexport.hpp"


namespace agge {
    namespace graphics {

        class rgb_color;

        /**
         * An RGBA color.
         */
        class AGGE_GRAPHICS_EXPORT rgba_color
        {
        public:
            /**
             * Default constructor.
             */
            inline rgba_color()
                            : r(0.0f), g(0.0f), b(0.0f), a(0.0f)
            {
            }

            /**
             * Constructor
             * @param rgbavalue RGBA value (like 0xFFAAEE77 (alpha is 0x77 here))
             */
            rgba_color(unsigned int rgbavalue);

            /**
             * Copy constructor.
             * @param c copied color value
             */
            inline rgba_color(const rgba_color& c) = default;

            rgba_color(const rgb_color& c, float alpha=1.0f);

            /**
             * Create RGBA color from color name.
             * @param name color name
             * @param alpha alpha value
             */
            rgba_color(const char *name, float alpha = 1.0f);

            /**
             * Construct (greyscale) color from single value.
             * @param value color value
             * @param alpha alpha value
             */
            inline rgba_color(const float value, const float alpha)
                            : r(value), g(value), b(value), a(alpha)
            {
            }

            /**
             * Creates a color
             * @param r_ red
             * @param g_ green
             * @param b_ blue
             * @param a_ alpha
             */
            inline rgba_color(float r_, float g_, float b_, float a_)
                            : r(r_), g(g_), b(b_), a(a_)
            {
            }

            /**
             * Initialize from array.
             * @param values color value array
             */
            inline rgba_color(const float *values)
                            : r(values[0]),
                              g(values[1]),
                              b(values[2]),
                              a(values[3])
            {
            }

            /**
             * Assignment operator.
             * @param c color to assign.
             * @return @c *this
             */
            inline rgba_color& operator =(const rgba_color& c) = default;

            void set_red(const float r_)
            {
                r = r_;
            }

            void set_green(const float g_)
            {
                g = g_;
            }

            void set_blue(const float b_)
            {
                b = b_;
            }

            void set_alpha(const float a_)
            {
                a = a_;
            }

            float get_red() const
            {
                return r;
            }

            float get_green() const
            {
                return g;
            }

            float get_blue() const
            {
                return b;
            }

            float get_alpha() const
            {
                return a;
            }

            /**
             * Compare with other color.
             * @param c color for comparison
             * @return @c true if colors equal
             */
            bool operator ==(const rgba_color& c) const;

            /**
             * Compare with other color.
             * @param c color for comparison
             * @return @c true if colors not equal
             */
            bool operator !=(const rgba_color& c) const;

            float r; //!< red
            float g; //!< green
            float b; //!< blue
            float a; //!< alpha
        };

        /**
         * Stream dump operator.
         * @param o output stream
         * @param c color to dump
         * @return @c o
         */
        AGGE_GRAPHICS_EXPORT std::ostream& operator <<(std::ostream& o,
                                                       const rgba_color& c);

    } // graphics
} // agge

#endif
