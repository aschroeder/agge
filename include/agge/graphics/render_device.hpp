/* -*-C++-*-
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_GRAPHICS_RENDER_DEVICE_HPP
#define AGGE_GRAPHICS_RENDER_DEVICE_HPP

#include "dllexport.hpp"
#include "agge/core/globals.hpp"
#include "agge/graphics/dimension.hpp"
#include "agge/graphics/rgb_color.hpp"
#include "agge/graphics/rgba_color.hpp"

namespace agge {
    namespace graphics {

        AGGE_DECLARE_REF(render_device);

        /**
         * A render device abstracts the process of sending rendering commands
         * from the target.
         */
        class AGGE_GRAPHICS_EXPORT render_device
        {
        public:
            /**
             * Destructor.
             */
            virtual ~render_device();

            /**
             * Return whether this device is the current device.
             * @return @c true if this device is the current device
             */
            bool is_current() const;

            /**
             * Makes this device the current device.
             */
            void make_current();

            /**
             * Get the dimension of this device. Some devices may have a
             * position - e.
             */
            const dimension& get_dimension() const;

            /**
             * Clears device using the clear color.
             * @param c clear color
             */
            void clear(const rgb_color& c);

            /**
             * Clears device using the clear color.
             * @param c clear color
             */
            void clear(const rgba_color& c);

            /**
             * Flushes the rendered elements to the screen.
             */
            void flush();
        protected:
            render_device(const dimension& d);

            virtual void on_make_current();
            virtual void on_flush();
            virtual void on_clear(const rgba_color& c);
        private:
            static render_device* s_current_device;
            dimension m_dimension;
        };
    }
}

#endif
