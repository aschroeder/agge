/*
 * AGGE - Another Graphics/Game Engine
 * Copyright (C) 2006-2015 by Alexander Schroeder
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef AGGE_GRAPHICS_WINDOW_HPP
#define AGGE_GRAPHICS_WINDOW_HPP

#include "dllexport.hpp"
#include "agge/core/globals.hpp"
#include "agge/graphics/render_device.hpp"
#include "agge/application/application.hpp"

#include <functional>

namespace agge {
    namespace graphics {

        /**
         *  @brief Reference to window.
         */
        AGGE_DECLARE_REF(window);

        /**
         * A window.
         */
        class AGGE_GRAPHICS_EXPORT window
        {
        protected:
            /**
             * Create a window.
             * @param dimension window position and size
             * @param title window titke
             * @param fullscreen whether the window is fullscreen
             */
            window(const dimension& dimension, const std::string& title,
                   bool fullscreen);
        public:
            typedef std::function<void (render_device&, float)> redraw_listener;
            typedef std::function<bool (void)> closing_listener;
            typedef std::function<void (void)> close_listener;

            /**
             * Destructor.
             */
            virtual ~window();

            /**
             * Get window dimension.
             * @return dimension
             */
            const dimension& get_dimension() const;

            /**
             * Get window title.
             * @return title
             */
            const std::string& get_title() const;

            /**
             * Get whether the window is a full-screen window.
             * @return @c true if fullscreen
             */
            bool is_fullscreen() const;

            /**
             * Set visibility of the window.
             * @param visible new visibility
             */
            void set_visible(bool visible);

            /**
             * Get whether the window is currently visible.
             * @return @c true if visible
             */
            bool is_visible() const;

            /**
             * Get the render device of this window.
             * @return render device of this window
             */
            virtual render_device& get_render_device() = 0;

            /**
             * Sets the redraw listener.
             * @param l redraw listener
             */
            void set_redraw_listener(const redraw_listener& l);

            /**
             * Sets window closing listener.
             * @param l window close listener
             */
            void set_closing_listener(const closing_listener& l);

            /**
             * Set window close listener.
             * @param l window close listener
             */
            void set_close_listener(const close_listener& l);
        protected:
            /**
             * Callback called if the window is shown.
             */
            virtual void on_show();

            /**
             * Callback called if the window is hidden.
             */
            virtual void on_hide();

            /**
             * Redraws the window - either calls the redraw listener or
             * paints a black screen.
             * @param interpolation percentage offset into next frame
             */
            void redraw(float interpolation);

            /**
             * Installs display listener.
             */
            void install_display_listener();

            /**
             * Remove display listener from application.
             */
            void uninstall_display_listener();

            /**
             * Method to call if the window is about to close. This method
             * will return @c true if the window close can proceed.
             * @return @c true if the window can be closed
             */
            bool on_closing();

            /**
             * Callback to call when the window is closed.
             */
            void on_close();

        private:
            void default_redraw();

            dimension m_dimension;
            std::string m_title;
            bool m_fullscreen;
            bool m_visible;
            redraw_listener m_redraw_listener;
            closing_listener m_closing_listener;
            close_listener m_close_listener;
            unsigned int m_display_listener_id;
        };

    }
}

#endif
