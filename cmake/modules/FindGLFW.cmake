# - Find GLFW
if(DEFINED GLFW_INCLUDE_DIR_HINT)
    message(STATUS "Looking for GLFW with hint ${GLFW_INCLUDE_DIR_HINT}")
    find_path(GLFW_INCLUDE_DIR 
              NAMES GLFW/glfw3.h
              HINTS ${GLFW_INCLUDE_DIR_HINT})
else()
    find_path(GLFW_INCLUDE_DIR 
              NAMES GLFW/glfw3.h)
endif()

if(DEFINED GLFW_LIBRARY_HINT)
    find_library(GLFW_LIBRARY
                 NAMES glfw3
                 PATH_SUFFIXES lib64
                 HINTS "${GLFW_LIBRARY_HINT}"
    )

else()
    find_library(GLFW_LIBRARY
                 NAMES glfw3
                 PATH_SUFFIXES lib64
    )
endif()

set(GLFW_INCLUDE_DIRS ${GLFW_INCLUDE_DIR})
set(GLFW_LIBRARIES ${GLFW_LIBRARY})    

include(${CMAKE_CURRENT_LIST_DIR}/FindPackageHandleStandardArgs.cmake)
find_package_handle_standard_args(GLFW
                                  REQUIRED_VARS GLFW_INCLUDE_DIR GLFW_LIBRARY)

mark_as_advanced(GLFW_INCLUDE_DIR GLFW_LIBRARY)
