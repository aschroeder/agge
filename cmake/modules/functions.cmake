# AGGE - Another Graphics/Game Engine
# Copyright (C) 2006-2015 by Alexander Schroeder
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License version 2 as published by the Free Software Foundation.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

FUNCTION(TARGET_ADD_DEFINE TARGET DEFINE)
    GET_TARGET_PROPERTY(tmp ${TARGET} COMPILE_FLAGS)
    IF(tmp STREQUAL "tmp-NOTFOUND")
        SET(tmp "") # set to empty string
    ELSE()
        SET(tmp "${tmp} ") # a space to cleanly separate from existing content
    ENDIF()
    SET(tmp "${tmp} -D${DEFINE}")
    SET_TARGET_PROPERTIES(${TARGET} PROPERTIES COMPILE_FLAGS "${tmp}")
ENDFUNCTION()

FUNCTION(TARGET_ADD_INCLUDE TARGET DIRECTORY)
    SET_PROPERTY(TARGET ${TARGET} APPEND PROPERTY INCLUDE_DIRECTORIES ${DIRECTORY})
ENDFUNCTION()


FUNCTION(TARGET_ADD_LIBDIR TARGET DIRECTORY)
    GET_TARGET_PROPERTY(tmp ${TARGET} LINK_FLAGS)
    IF(tmp STREQUAL "tmp-NOTFOUND")
        SET(tmp "") # set to empty string
    ELSE()
        SET(tmp "${tmp} ") # a space to cleanly separate from existing content
    ENDIF()
    IF(MSVC)
        SET(tmp "${tmp} /LIBPATH:${DIRECTORY}")
    ELSE()    
        SET(tmp "${tmp} -L${DIRECTORY}")
    ENDIF()
    SET_TARGET_PROPERTIES(${TARGET} PROPERTIES LINK_FLAGS "${tmp}")
ENDFUNCTION()

FUNCTION(TARGET_ADD_PROJECT_INCLUDES TARGET)
    TARGET_ADD_INCLUDE(${TARGET} "${CMAKE_SOURCE_DIR}/include")
    TARGET_ADD_INCLUDE(${TARGET} "${CMAKE_BINARY_DIR}/include")
ENDFUNCTION()

FUNCTION(TARGET_IS_TEST TARGET)
    ADD_TEST(NAME ${TARGET} 
             COMMAND ${CMAKE_BINARY_DIR}/${TARGET} --gtest_output=xml:${TARGET}.xml
             WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")
ENDFUNCTION()


FUNCTION(COPY_FILE FILE DESTINATION)
    FILE(MAKE_DIRECTORY ${DESTINATION})
    FILE(COPY ${FILE} DESTINATION ${DESTINATION})
ENDFUNCTION()

