# AGGE - Another Graphics/Game Engine
# Copyright (C) 2006-2015 by Alexander Schroeder
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License version 2 as published by the Free Software Foundation.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
INCLUDE(CheckIncludeFiles)

IF(CMAKE_CXX_COMPILER MATCHES ".*clang")
    SET(CMAKE_COMPILER_IS_CLANGXX 1)
ENDIF()


CHECK_INCLUDE_FILES(malloc.h HAVE_MALLOC_H)
CHECK_INCLUDE_FILES(windows.h HAVE_WINDOWS_H)
CHECK_INCLUDE_FILES("sched/yield.h" HAVE_SCHED_YIELD_H)
CHECK_INCLUDE_FILES("errno.h;unistd.h;time.h;sys/time.h" POSIX_TIME_HEADERS)

IF(POSIX_TIME_HEADERS)
    IF(NOT HAVE_WINDOWS_H)
        SET(USE_POSIX_TIME_FUNCTIONS 1)
    ENDIF()
ENDIF()

CHECK_INCLUDE_FILES("unistd.h" HAVE_UNISTD_H)

INCLUDE(TestBigEndian)
TEST_BIG_ENDIAN(AGGE_BYTEORDER_BIGENDIAN)

INCLUDE(CheckCXX11Features)

CHECK_CXX11_FEATURES()

IF(NOT "${HAVE_CXX11}")
    MESSAGE(FATAL_ERROR "AGGE requires basic C++ 11 support")
ENDIF()
