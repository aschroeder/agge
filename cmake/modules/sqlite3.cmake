# AGGE - Another Graphics/Game Engine
# Copyright (C) 2006-2015 by Alexander Schroeder
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License version 2 as published by the Free Software Foundation.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

IF(USE_SUPERBUILD)
    FIND_PACKAGE(Sqlite3)
    IF(NOT "${Sqlite3_FOUND}")
        MESSAGE("Need to build Sqlite3, not found in the system")
        EXTERNALPROJECT_ADD(sqlite3_external
                        PREFIX sqlite3_external
                        URL http://www.sqlite.org/2015/sqlite-amalgamation-3081002.zip
                        CMAKE_ARGS -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
                        INSTALL_COMMAND ""
        )
        EXTERNALPROJECT_ADD_STEP(sqlite3_external sqlite3_external_copy_cmake_file
                                 COMMAND ${CMAKE_COMMAND} -E copy ${PROJECT_SOURCE_DIR}/cmake/external/sqlite3/CMakeLists.txt ${CMAKE_CURRENT_BINARY_DIR}/sqlite3_external/src/sqlite3_external/CMakeLists.txt
                                 DEPENDEES patch
                                 DEPENDERS configure
        )
        LIST(APPEND DEPENDENCIES sqlite3_external)
    ENDIF()
ELSE()
    FIND_PACKAGE(Sqlite3)
    IF(NOT "${Sqlite3_FOUND}")
        MESSAGE("-- Retry again with superbuild location ...")
        SET(Sqlite3_INCLUDE_DIR "${PROJECT_BINARY_DIR}/sqlite3_external/src/sqlite3_external")
        SET(Sqlite3_LIBRARY_DIR "${PROJECT_BINARY_DIR}/sqlite3_external/src/sqlite3_external-build")
        FIND_PACKAGE(Sqlite3)
    ENDIF()
    ADD_LIBRARY(sqlite3 UNKNOWN IMPORTED)
    SET_PROPERTY(TARGET sqlite3
                 PROPERTY IMPORTED_LOCATION "${Sqlite3_LIBRARY_RELEASE}")
    SET_PROPERTY(TARGET sqlite3
                 PROPERTY IMPORTED_LOCATION_RELEASE "${Sqlite3_LIBRARY_RELEASE}")
    SET_PROPERTY(TARGET sqlite3
                 PROPERTY IMPORTED_LOCATION_DEBUG "${Sqlite3_LIBRARY_DEBUG}")
    SET_PROPERTY(TARGET sqlite3
                 APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${Sqlite3_INCLUDE_DIR}")
ENDIF()


