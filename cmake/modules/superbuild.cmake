# AGGE - Another Graphics/Game Engine
# Copyright (C) 2006-2015 by Alexander Schroeder
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License version 2 as published by the Free Software Foundation.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
SET(DEPENDENCIES)
INCLUDE(ExternalProject)
INCLUDE(sqlite3)
INCLUDE(gmock)
INCLUDE(glfw)
EXTERNALPROJECT_ADD(agge_superbuild
                    DEPENDS ${DEPENDENCIES}
                    SOURCE_DIR ${PROJECT_SOURCE_DIR}
                    CMAKE_ARGS -DUSE_SUPERBUILD=OFF
                    INSTALL_COMMAND ""
                    BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR})
