# AGGE - Another Graphics/Game Engine
# Copyright (C) 2006-2015 by Alexander Schroeder
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License version 2 as published by the Free Software Foundation.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
INCLUDE(ExternalProject)
IF(USE_SUPERBUILD)
    EXTERNALPROJECT_ADD(gmock_external
        URL https://github.com/aschroeder/googletest/archive/master.zip
        PREFIX gmock_external
        CMAKE_ARGS -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} -Dgtest_force_shared_crt=ON -Dgtest_disable_pthreads=ON
        INSTALL_COMMAND ""
    )
    LIST(APPEND DEPENDENCIES gmock_external)
ELSE()
    IF(MSVC) 
        SET(gmock_library_name "gmock.lib") 
        SET(gtest_library_name "gtest.lib")
    ELSE()
        SET(gmock_library_name "libgmock.a")
        SET(gtest_library_name "libgtest.a")
    ENDIF()
    SET(gmock_library_file "${PROJECT_BINARY_DIR}/gmock_external/src/gmock_external-build/googlemock/${gmock_library_name}")
    SET(gmock_include_dir "${PROJECT_BINARY_DIR}/gmock_external/src/gmock_external/googlemock/include")
    SET(gtest_library_file "${PROJECT_BINARY_DIR}/gmock_external/src/gmock_external-build/googlemock/gtest/${gtest_library_name}")
    SET(gtest_include_dir  "${PROJECT_BINARY_DIR}/gmock_external/src/gmock_external/googletest/include")

    ADD_LIBRARY(gmock STATIC IMPORTED)
    SET_PROPERTY(TARGET gmock
                 PROPERTY IMPORTED_LOCATION ${gmock_library_file})
    SET_PROPERTY(TARGET gmock 
                 APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES 
                 "${gmock_include_dir}") 

    ADD_LIBRARY(gtest STATIC IMPORTED)
    SET_PROPERTY(TARGET gtest
                 PROPERTY IMPORTED_LOCATION ${gtest_library_file})
    SET_PROPERTY(TARGET gtest
                 APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES 
                 "${gtest_include_dir}") 
    SET_PROPERTY(TARGET gmock
                 PROPERTY INTERFACE_LINK_LIBRARIES "gtest")
ENDIF()
