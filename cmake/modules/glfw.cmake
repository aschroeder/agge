# AGGE - Another Graphics/Game Engine
# Copyright (C) 2006-2015 by Alexander Schroeder
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License version 2 as published by the Free Software Foundation.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
IF(USE_SUPERBUILD)
    FIND_PACKAGE(GLFW)
    IF(NOT GLFW_FOUND) 
        MESSAGE("Need to build GLFW library, not found in the system")
        EXTERNALPROJECT_ADD(glfw_external
                            PREFIX glfw_external
                            URL https://github.com/glfw/glfw/releases/download/3.1.2/glfw-3.1.2.zip
                            CMAKE_ARGS -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
                            INSTALL_COMMAND ""
     )
        LIST(APPEND DEPENDENCIES glfw_external)
    ENDIF()
ELSE()
    FIND_PACKAGE(GLFW)
    IF(NOT GLFW_FOUND)
        MESSAGE("-- Retry again with superbuild location ...")
        SET(GLFW_INCLUDE_DIR_HINT ${CMAKE_CURRENT_BINARY_DIR}/glfw_external/src/glfw_external/include)
        SET(GLFW_LIBRARY_HINT ${CMAKE_CURRENT_BINARY_DIR}/glfw_external/src/glfw_external-build/src)
        FIND_PACKAGE(GLFW)
    ENDIF()
    
    ADD_LIBRARY(glfw UNKNOWN IMPORTED)
    SET_PROPERTY(TARGET glfw
                 PROPERTY IMPORTED_LOCATION ${GLFW_LIBRARIES})
    SET_PROPERTY(TARGET glfw
                 APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${GLFW_INCLUDE_DIR}")
    
ENDIF()